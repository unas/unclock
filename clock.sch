<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="1.27" unitdist="mm" unit="mm" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="45xx">
<description>&lt;b&gt;CMOS Logic Devices, 4500 Series &lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;http://www.ls3c.com.tw/product/1/COMOS.html
&lt;li&gt;http://www.ee.washington.edu
&lt;li&gt;http://www.onsemi.com
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL16">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16">
<description>&lt;b&gt;Small Outline package&lt;/b&gt; 150 mil</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="51"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="51"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.508" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-3.81" y="-0.762" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.905" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="4543">
<wire x1="-7.62" y1="-12.7" x2="7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<text x="-7.62" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="LD" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="IC" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="IB" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="ID" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="IA" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="PH" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="BI" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="A" x="12.7" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="B" x="12.7" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="C" x="12.7" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="D" x="12.7" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="E" x="12.7" y="0" length="middle" direction="out" rot="R180"/>
<pin name="G" x="12.7" y="2.54" length="middle" direction="out" rot="R180"/>
<pin name="F" x="12.7" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="VDD" x="12.7" y="7.62" length="middle" direction="in" rot="R180"/>
<pin name="VSS" x="-12.7" y="-10.16" length="middle" direction="out"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="4543" prefix="IC">
<description>BCD to 7-segment &lt;b&gt;LCD DECODER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="4543" x="20.32" y="0"/>
</gates>
<devices>
<device name="N" package="DIL16">
<connects>
<connect gate="A" pin="A" pad="9"/>
<connect gate="A" pin="B" pad="10"/>
<connect gate="A" pin="BI" pad="7"/>
<connect gate="A" pin="C" pad="11"/>
<connect gate="A" pin="D" pad="12"/>
<connect gate="A" pin="E" pad="13"/>
<connect gate="A" pin="F" pad="15"/>
<connect gate="A" pin="G" pad="14"/>
<connect gate="A" pin="IA" pad="5"/>
<connect gate="A" pin="IB" pad="3"/>
<connect gate="A" pin="IC" pad="2"/>
<connect gate="A" pin="ID" pad="4"/>
<connect gate="A" pin="LD" pad="1"/>
<connect gate="A" pin="PH" pad="6"/>
<connect gate="A" pin="VDD" pad="16"/>
<connect gate="A" pin="VSS" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="A" pad="9"/>
<connect gate="A" pin="B" pad="10"/>
<connect gate="A" pin="BI" pad="7"/>
<connect gate="A" pin="C" pad="11"/>
<connect gate="A" pin="D" pad="12"/>
<connect gate="A" pin="E" pad="13"/>
<connect gate="A" pin="F" pad="15"/>
<connect gate="A" pin="G" pad="14"/>
<connect gate="A" pin="IA" pad="5"/>
<connect gate="A" pin="IB" pad="3"/>
<connect gate="A" pin="IC" pad="2"/>
<connect gate="A" pin="ID" pad="4"/>
<connect gate="A" pin="LD" pad="1"/>
<connect gate="A" pin="PH" pad="6"/>
<connect gate="A" pin="VDD" pad="16"/>
<connect gate="A" pin="VSS" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="special">
<description>&lt;b&gt;Special Devices&lt;/b&gt;&lt;p&gt;
7-segment displays, switches, heatsinks, crystals, transformers, etc.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="7SEG-750">
<description>7-segment &lt;B&gt;DISPLAY&lt;/B&gt;&lt;p&gt;
750 mm(3")</description>
<wire x1="32.5" y1="-42.5" x2="32.5" y2="42.5" width="0.1524" layer="21"/>
<wire x1="-32.5" y1="42.5" x2="32.5" y2="42.5" width="0.1524" layer="21"/>
<wire x1="-32.5" y1="42.5" x2="-32.5" y2="-42.5" width="0.1524" layer="21"/>
<wire x1="32.5" y1="-42.5" x2="-32.5" y2="-42.5" width="0.1524" layer="21"/>
<wire x1="3.5306" y1="4.7498" x2="4.4196" y2="5.461" width="0.254" layer="21"/>
<wire x1="4.4196" y1="5.461" x2="4.8768" y2="5.1308" width="0.254" layer="21"/>
<wire x1="2.8448" y1="1.0414" x2="3.9878" y2="0.4064" width="0.254" layer="21"/>
<wire x1="3.9878" y1="0.4064" x2="4.8768" y2="5.1308" width="0.254" layer="21"/>
<wire x1="3.5306" y1="4.7498" x2="2.8448" y2="1.0414" width="0.254" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.683" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="5.08" x2="4.0386" y2="5.7658" width="0.254" layer="21"/>
<wire x1="4.0386" y1="5.7658" x2="3.2766" y2="6.35" width="0.254" layer="21"/>
<wire x1="2.413" y1="-0.635" x2="3.683" y2="0" width="0.254" layer="21"/>
<wire x1="2.4638" y1="-1.1938" x2="3.81" y2="-0.508" width="0.254" layer="21"/>
<wire x1="3.81" y1="-0.508" x2="2.921" y2="-5.4102" width="0.254" layer="21"/>
<wire x1="2.4638" y1="-1.1938" x2="1.8034" y2="-4.7498" width="0.254" layer="21"/>
<wire x1="1.4732" y1="-5.08" x2="2.2352" y2="-5.9944" width="0.254" layer="21"/>
<wire x1="2.2352" y1="-5.9944" x2="1.8288" y2="-6.35" width="0.254" layer="21"/>
<wire x1="1.8034" y1="-4.7498" x2="2.5908" y2="-5.6896" width="0.254" layer="21"/>
<wire x1="2.5908" y1="-5.6896" x2="2.921" y2="-5.4102" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.683" y2="0" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-5.08" x2="-4.0386" y2="-5.7658" width="0.254" layer="21"/>
<wire x1="-4.0386" y1="-5.7658" x2="-3.2766" y2="-6.35" width="0.254" layer="21"/>
<wire x1="-3.2766" y1="-6.35" x2="1.8288" y2="-6.35" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-5.08" x2="1.4732" y2="-5.08" width="0.254" layer="21"/>
<wire x1="2.413" y1="-0.635" x2="-2.54" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.413" y1="0.635" x2="-3.683" y2="0" width="0.254" layer="21"/>
<wire x1="-2.413" y1="0.635" x2="2.54" y2="0.635" width="0.254" layer="21"/>
<wire x1="-1.4732" y1="5.08" x2="-2.2352" y2="5.9944" width="0.254" layer="21"/>
<wire x1="-2.2352" y1="5.9944" x2="-1.8288" y2="6.35" width="0.254" layer="21"/>
<wire x1="-1.8288" y1="6.35" x2="3.2766" y2="6.35" width="0.254" layer="21"/>
<wire x1="-1.4732" y1="5.08" x2="3.175" y2="5.08" width="0.254" layer="21"/>
<wire x1="-3.302" y1="-5.715" x2="1.397" y2="-5.715" width="1.016" layer="21"/>
<wire x1="2.413" y1="-4.699" x2="3.048" y2="-1.524" width="1.016" layer="21"/>
<wire x1="-2.667" y1="0" x2="2.667" y2="0" width="1.016" layer="21"/>
<wire x1="2.54" y1="-5.207" x2="2.667" y2="-5.08" width="0.762" layer="21"/>
<wire x1="3.556" y1="1.397" x2="4.191" y2="4.699" width="1.016" layer="21"/>
<wire x1="4.445" y1="5.08" x2="4.572" y2="4.953" width="0.508" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.889" width="0.6604" layer="21"/>
<wire x1="3.302" y1="5.715" x2="-1.397" y2="5.715" width="1.016" layer="21"/>
<wire x1="-2.032" y1="5.842" x2="-1.651" y2="6.223" width="0.4064" layer="21"/>
<wire x1="-3.5306" y1="-4.7498" x2="-4.4196" y2="-5.461" width="0.254" layer="21"/>
<wire x1="-4.4196" y1="-5.461" x2="-4.8768" y2="-5.1308" width="0.254" layer="21"/>
<wire x1="-2.8448" y1="-1.0414" x2="-3.9878" y2="-0.4064" width="0.254" layer="21"/>
<wire x1="-3.9878" y1="-0.4064" x2="-4.8768" y2="-5.1308" width="0.254" layer="21"/>
<wire x1="-3.5306" y1="-4.7498" x2="-2.8448" y2="-1.0414" width="0.254" layer="21"/>
<wire x1="-2.4638" y1="1.1938" x2="-3.81" y2="0.508" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0.508" x2="-2.921" y2="5.4102" width="0.254" layer="21"/>
<wire x1="-2.4638" y1="1.1938" x2="-1.8034" y2="4.7498" width="0.254" layer="21"/>
<wire x1="-1.8034" y1="4.7498" x2="-2.5908" y2="5.6896" width="0.254" layer="21"/>
<wire x1="-2.5908" y1="5.6896" x2="-2.921" y2="5.4102" width="0.254" layer="21"/>
<wire x1="-2.413" y1="4.699" x2="-3.048" y2="1.524" width="1.016" layer="21"/>
<wire x1="-2.54" y1="5.207" x2="-2.667" y2="5.08" width="0.762" layer="21"/>
<wire x1="-3.556" y1="-1.397" x2="-4.191" y2="-4.699" width="1.016" layer="21"/>
<wire x1="-4.445" y1="-5.08" x2="-4.572" y2="-4.953" width="0.508" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.889" width="0.6604" layer="21"/>
<circle x="4.572" y="-5.715" radius="0.381" width="0.762" layer="21"/>
<pad name="1" x="-5.08" y="-39" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="-39" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="0" y="-39" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="-39" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="-39" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="39" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="2.54" y="39" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="0" y="39" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="-2.54" y="39" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="-5.08" y="39" drill="0.8128" shape="long" rot="R90"/>
<text x="-6.096" y="9.144" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-10.922" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.524" y1="-6.223" x2="2.159" y2="-5.715" layer="21"/>
<rectangle x1="3.175" y1="-1.27" x2="3.683" y2="-0.762" layer="21"/>
<rectangle x1="3.048" y1="-0.254" x2="3.429" y2="0.254" layer="21"/>
<rectangle x1="-3.429" y1="-0.127" x2="-3.048" y2="0.127" layer="21"/>
<rectangle x1="-3.683" y1="0.762" x2="-3.175" y2="1.27" layer="21"/>
<rectangle x1="4.318" y1="-5.842" x2="4.826" y2="-5.588" layer="21"/>
</package>
<package name="7SEG-13-MOD">
<description>7-segment &lt;B&gt;DISPLAY&lt;/B&gt;&lt;p&gt;
13 mm, pins 3/8 VDD</description>
<wire x1="6.096" y1="-8.636" x2="6.096" y2="8.636" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="8.636" x2="6.096" y2="8.636" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="8.636" x2="-6.096" y2="-8.636" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-8.636" x2="-6.096" y2="-8.636" width="0.1524" layer="21"/>
<wire x1="3.5306" y1="4.7498" x2="4.4196" y2="5.461" width="0.254" layer="21"/>
<wire x1="4.4196" y1="5.461" x2="4.8768" y2="5.1308" width="0.254" layer="21"/>
<wire x1="2.8448" y1="1.0414" x2="3.9878" y2="0.4064" width="0.254" layer="21"/>
<wire x1="3.9878" y1="0.4064" x2="4.8768" y2="5.1308" width="0.254" layer="21"/>
<wire x1="3.5306" y1="4.7498" x2="2.8448" y2="1.0414" width="0.254" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.683" y2="0" width="0.254" layer="21"/>
<wire x1="3.175" y1="5.08" x2="4.0386" y2="5.7658" width="0.254" layer="21"/>
<wire x1="4.0386" y1="5.7658" x2="3.2766" y2="6.35" width="0.254" layer="21"/>
<wire x1="2.413" y1="-0.635" x2="3.683" y2="0" width="0.254" layer="21"/>
<wire x1="2.4638" y1="-1.1938" x2="3.81" y2="-0.508" width="0.254" layer="21"/>
<wire x1="3.81" y1="-0.508" x2="2.921" y2="-5.4102" width="0.254" layer="21"/>
<wire x1="2.4638" y1="-1.1938" x2="1.8034" y2="-4.7498" width="0.254" layer="21"/>
<wire x1="1.4732" y1="-5.08" x2="2.2352" y2="-5.9944" width="0.254" layer="21"/>
<wire x1="2.2352" y1="-5.9944" x2="1.8288" y2="-6.35" width="0.254" layer="21"/>
<wire x1="1.8034" y1="-4.7498" x2="2.5908" y2="-5.6896" width="0.254" layer="21"/>
<wire x1="2.5908" y1="-5.6896" x2="2.921" y2="-5.4102" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.683" y2="0" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-5.08" x2="-4.0386" y2="-5.7658" width="0.254" layer="21"/>
<wire x1="-4.0386" y1="-5.7658" x2="-3.2766" y2="-6.35" width="0.254" layer="21"/>
<wire x1="-3.2766" y1="-6.35" x2="1.8288" y2="-6.35" width="0.254" layer="21"/>
<wire x1="-3.175" y1="-5.08" x2="1.4732" y2="-5.08" width="0.254" layer="21"/>
<wire x1="2.413" y1="-0.635" x2="-2.54" y2="-0.635" width="0.254" layer="21"/>
<wire x1="-2.413" y1="0.635" x2="-3.683" y2="0" width="0.254" layer="21"/>
<wire x1="-2.413" y1="0.635" x2="2.54" y2="0.635" width="0.254" layer="21"/>
<wire x1="-1.4732" y1="5.08" x2="-2.2352" y2="5.9944" width="0.254" layer="21"/>
<wire x1="-2.2352" y1="5.9944" x2="-1.8288" y2="6.35" width="0.254" layer="21"/>
<wire x1="-1.8288" y1="6.35" x2="3.2766" y2="6.35" width="0.254" layer="21"/>
<wire x1="-1.4732" y1="5.08" x2="3.175" y2="5.08" width="0.254" layer="21"/>
<wire x1="-3.302" y1="-5.715" x2="1.397" y2="-5.715" width="1.016" layer="21"/>
<wire x1="2.413" y1="-4.699" x2="3.048" y2="-1.524" width="1.016" layer="21"/>
<wire x1="-2.667" y1="0" x2="2.667" y2="0" width="1.016" layer="21"/>
<wire x1="2.54" y1="-5.207" x2="2.667" y2="-5.08" width="0.762" layer="21"/>
<wire x1="3.556" y1="1.397" x2="4.191" y2="4.699" width="1.016" layer="21"/>
<wire x1="4.445" y1="5.08" x2="4.572" y2="4.953" width="0.508" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.889" width="0.6604" layer="21"/>
<wire x1="3.302" y1="5.715" x2="-1.397" y2="5.715" width="1.016" layer="21"/>
<wire x1="-2.032" y1="5.842" x2="-1.651" y2="6.223" width="0.4064" layer="21"/>
<wire x1="-3.5306" y1="-4.7498" x2="-4.4196" y2="-5.461" width="0.254" layer="21"/>
<wire x1="-4.4196" y1="-5.461" x2="-4.8768" y2="-5.1308" width="0.254" layer="21"/>
<wire x1="-2.8448" y1="-1.0414" x2="-3.9878" y2="-0.4064" width="0.254" layer="21"/>
<wire x1="-3.9878" y1="-0.4064" x2="-4.8768" y2="-5.1308" width="0.254" layer="21"/>
<wire x1="-3.5306" y1="-4.7498" x2="-2.8448" y2="-1.0414" width="0.254" layer="21"/>
<wire x1="-2.4638" y1="1.1938" x2="-3.81" y2="0.508" width="0.254" layer="21"/>
<wire x1="-3.81" y1="0.508" x2="-2.921" y2="5.4102" width="0.254" layer="21"/>
<wire x1="-2.4638" y1="1.1938" x2="-1.8034" y2="4.7498" width="0.254" layer="21"/>
<wire x1="-1.8034" y1="4.7498" x2="-2.5908" y2="5.6896" width="0.254" layer="21"/>
<wire x1="-2.5908" y1="5.6896" x2="-2.921" y2="5.4102" width="0.254" layer="21"/>
<wire x1="-2.413" y1="4.699" x2="-3.048" y2="1.524" width="1.016" layer="21"/>
<wire x1="-2.54" y1="5.207" x2="-2.667" y2="5.08" width="0.762" layer="21"/>
<wire x1="-3.556" y1="-1.397" x2="-4.191" y2="-4.699" width="1.016" layer="21"/>
<wire x1="-4.445" y1="-5.08" x2="-4.572" y2="-4.953" width="0.508" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-3.81" y2="-0.889" width="0.6604" layer="21"/>
<circle x="4.572" y="-5.715" radius="0.381" width="0.762" layer="21"/>
<pad name="1" x="-5.08" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-2.54" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="0" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="2.54" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="5.08" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="5.08" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="2.54" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="0" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="-2.54" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="-5.08" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<text x="-6.096" y="9.144" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-10.922" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="1.524" y1="-6.223" x2="2.159" y2="-5.715" layer="21"/>
<rectangle x1="3.175" y1="-1.27" x2="3.683" y2="-0.762" layer="21"/>
<rectangle x1="3.048" y1="-0.254" x2="3.429" y2="0.254" layer="21"/>
<rectangle x1="-3.429" y1="-0.127" x2="-3.048" y2="0.127" layer="21"/>
<rectangle x1="-3.683" y1="0.762" x2="-3.175" y2="1.27" layer="21"/>
<rectangle x1="4.318" y1="-5.842" x2="4.826" y2="-5.588" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="7SEG-CA-MOD">
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-2.4892" y1="5.5118" x2="2.9972" y2="5.5118" width="0.8128" layer="94"/>
<wire x1="-3.5052" y1="-5.5118" x2="2.0066" y2="-5.5118" width="0.8128" layer="94"/>
<wire x1="-2.9972" y1="4.4958" x2="-3.5052" y2="0.9906" width="0.8128" layer="94"/>
<wire x1="-3.5052" y1="-0.508" x2="-3.9878" y2="-4.4958" width="0.8128" layer="94"/>
<wire x1="3.5052" y1="4.4958" x2="2.9972" y2="0.9906" width="0.8128" layer="94"/>
<wire x1="2.9972" y1="-0.508" x2="2.4892" y2="-4.4958" width="0.8128" layer="94"/>
<wire x1="-2.4892" y1="0" x2="2.0066" y2="0" width="0.8128" layer="94"/>
<circle x="3.81" y="-5.588" radius="0.1016" width="0.8128" layer="94"/>
<text x="-6.4008" y="5.3848" size="1.27" layer="94">a</text>
<text x="-6.4008" y="2.794" size="1.27" layer="94">b</text>
<text x="-6.4008" y="0.4064" size="1.27" layer="94">c</text>
<text x="-6.4008" y="-2.2098" size="1.27" layer="94">d</text>
<text x="-6.4008" y="-4.8006" size="1.27" layer="94">e</text>
<text x="5.4102" y="5.4102" size="1.27" layer="94">f</text>
<text x="5.4102" y="3.2004" size="1.27" layer="94">g</text>
<text x="5.4102" y="0.4064" size="1.27" layer="94">DP</text>
<text x="5.4102" y="-2.2098" size="1.27" layer="94">VDD1</text>
<text x="-5.08" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.4102" y="-4.8006" size="1.27" layer="94">VDD2</text>
<pin name="C" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="B" x="-7.62" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="A" x="-7.62" y="5.08" visible="off" length="short" direction="pas"/>
<pin name="D" x="-7.62" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="E" x="-7.62" y="-5.08" visible="off" length="short" direction="pas"/>
<pin name="VDD1" x="7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="DP" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="G" x="7.62" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="F" x="7.62" y="5.08" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="VDD2" x="7.62" y="-5.08" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="7SEG-CA">
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-2.4892" y1="5.5118" x2="2.9972" y2="5.5118" width="0.8128" layer="94"/>
<wire x1="-3.5052" y1="-5.5118" x2="2.0066" y2="-5.5118" width="0.8128" layer="94"/>
<wire x1="-2.9972" y1="4.4958" x2="-3.5052" y2="0.9906" width="0.8128" layer="94"/>
<wire x1="-3.5052" y1="-0.508" x2="-3.9878" y2="-4.4958" width="0.8128" layer="94"/>
<wire x1="3.5052" y1="4.4958" x2="2.9972" y2="0.9906" width="0.8128" layer="94"/>
<wire x1="2.9972" y1="-0.508" x2="2.4892" y2="-4.4958" width="0.8128" layer="94"/>
<wire x1="-2.4892" y1="0" x2="2.0066" y2="0" width="0.8128" layer="94"/>
<circle x="3.81" y="-5.588" radius="0.1016" width="0.8128" layer="94"/>
<text x="-6.4008" y="5.3848" size="1.27" layer="94">a</text>
<text x="-6.4008" y="2.794" size="1.27" layer="94">b</text>
<text x="-6.4008" y="0.4064" size="1.27" layer="94">c</text>
<text x="-6.4008" y="-2.2098" size="1.27" layer="94">d</text>
<text x="-6.4008" y="-4.8006" size="1.27" layer="94">e</text>
<text x="5.4102" y="5.4102" size="1.27" layer="94">f</text>
<text x="5.4102" y="3.2004" size="1.27" layer="94">g</text>
<text x="5.4102" y="0.4064" size="1.27" layer="94">DP</text>
<text x="5.4102" y="-2.2098" size="1.27" layer="94">VDD1</text>
<text x="-5.08" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="5.4102" y="-4.8006" size="1.27" layer="94">VDD2</text>
<pin name="C" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="B" x="-7.62" y="2.54" visible="off" length="short" direction="pas"/>
<pin name="A" x="-7.62" y="5.08" visible="off" length="short" direction="pas"/>
<pin name="D" x="-7.62" y="-2.54" visible="off" length="short" direction="pas"/>
<pin name="E" x="-7.62" y="-5.08" visible="off" length="short" direction="pas"/>
<pin name="VDD1" x="7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="DP" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="G" x="7.62" y="2.54" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="F" x="7.62" y="5.08" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="VDD2" x="7.62" y="-5.08" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="7SEG-CA-750" prefix="D" uservalue="yes">
<description>7-segment &lt;b&gt;DISPLAY&lt;/b&gt;&lt;p&gt;
single anode, 13 mm</description>
<gates>
<gate name="G$1" symbol="7SEG-CA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="7SEG-750">
<connects>
<connect gate="G$1" pin="A" pad="7"/>
<connect gate="G$1" pin="B" pad="6"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$1" pin="D" pad="3"/>
<connect gate="G$1" pin="DP" pad="8"/>
<connect gate="G$1" pin="E" pad="2"/>
<connect gate="G$1" pin="F" pad="9"/>
<connect gate="G$1" pin="G" pad="10"/>
<connect gate="G$1" pin="VDD1" pad="1"/>
<connect gate="G$1" pin="VDD2" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="7SEG-CA-MOD" prefix="D" uservalue="yes">
<description>7-segment &lt;b&gt;DISPLAY&lt;/b&gt;&lt;p&gt;
single anode, 13 mm, pins 3/8 VDD</description>
<gates>
<gate name="G$1" symbol="7SEG-CA-MOD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="7SEG-13-MOD">
<connects>
<connect gate="G$1" pin="A" pad="7"/>
<connect gate="G$1" pin="B" pad="6"/>
<connect gate="G$1" pin="C" pad="4"/>
<connect gate="G$1" pin="D" pad="2"/>
<connect gate="G$1" pin="DP" pad="5"/>
<connect gate="G$1" pin="E" pad="1"/>
<connect gate="G$1" pin="F" pad="9"/>
<connect gate="G$1" pin="G" pad="10"/>
<connect gate="G$1" pin="VDD1" pad="3"/>
<connect gate="G$1" pin="VDD2" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="uln-udn">
<description>&lt;b&gt;Driver Arrays&lt;/b&gt;&lt;p&gt;
ULN and UDN Series&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL16">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt;</description>
<wire x1="10.16" y1="2.921" x2="-10.16" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="10.16" y1="2.921" x2="10.16" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="2.921" x2="-10.16" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-2.921" x2="-10.16" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.016" x2="-10.16" y2="-1.016" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="6.35" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="8.89" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="3.81" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="1.27" y="-3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="-1.27" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="-3.81" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="-6.35" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="-8.89" y="3.81" drill="0.8128" shape="long" rot="R90"/>
<text x="-10.541" y="-2.921" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-7.493" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO16">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt;</description>
<wire x1="4.699" y1="1.9558" x2="-4.699" y2="1.9558" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.9558" x2="5.08" y2="-1.5748" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.5748" x2="-4.699" y2="1.9558" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.9558" x2="5.08" y2="1.5748" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.5748" x2="-4.699" y2="-1.9558" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="-1.9558" x2="4.699" y2="-1.9558" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.5748" x2="5.08" y2="1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.5748" x2="-5.08" y2="-1.5748" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.508" x2="-5.08" y2="-0.508" width="0.1524" layer="21" curve="-180"/>
<wire x1="-5.08" y1="-1.6002" x2="5.08" y2="-1.6002" width="0.0508" layer="21"/>
<smd name="1" x="-4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="-4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="-3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="-1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="-0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="0.635" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="0.635" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="1.905" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="3.175" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="1.905" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="3.175" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="4.445" y="-3.0734" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="4.445" y="3.0734" dx="0.6604" dy="2.032" layer="1"/>
<text x="-4.064" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-5.461" y="-1.778" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-0.889" y1="1.9558" x2="-0.381" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="-3.0988" x2="-4.191" y2="-1.9558" layer="51"/>
<rectangle x1="-3.429" y1="-3.0988" x2="-2.921" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="-3.0734" x2="-1.651" y2="-1.9304" layer="51"/>
<rectangle x1="-0.889" y1="-3.0988" x2="-0.381" y2="-1.9558" layer="51"/>
<rectangle x1="-2.159" y1="1.9558" x2="-1.651" y2="3.0988" layer="51"/>
<rectangle x1="-3.429" y1="1.9558" x2="-2.921" y2="3.0988" layer="51"/>
<rectangle x1="-4.699" y1="1.9558" x2="-4.191" y2="3.0988" layer="51"/>
<rectangle x1="0.381" y1="-3.0988" x2="0.889" y2="-1.9558" layer="51"/>
<rectangle x1="1.651" y1="-3.0988" x2="2.159" y2="-1.9558" layer="51"/>
<rectangle x1="2.921" y1="-3.0988" x2="3.429" y2="-1.9558" layer="51"/>
<rectangle x1="4.191" y1="-3.0988" x2="4.699" y2="-1.9558" layer="51"/>
<rectangle x1="0.381" y1="1.9558" x2="0.889" y2="3.0988" layer="51"/>
<rectangle x1="1.651" y1="1.9558" x2="2.159" y2="3.0988" layer="51"/>
<rectangle x1="2.921" y1="1.9558" x2="3.429" y2="3.0988" layer="51"/>
<rectangle x1="4.191" y1="1.9558" x2="4.699" y2="3.0988" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="2001A">
<wire x1="-7.62" y1="10.16" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-12.7" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-12.7" width="0.4064" layer="94"/>
<text x="-7.62" y="10.922" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-15.24" size="1.778" layer="96">&gt;VALUE</text>
<pin name="I1" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="I2" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="I3" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="I4" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="I5" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="I6" x="-12.7" y="-5.08" length="middle" direction="in"/>
<pin name="I7" x="-12.7" y="-7.62" length="middle" direction="in"/>
<pin name="O1" x="12.7" y="7.62" length="middle" direction="oc" rot="R180"/>
<pin name="O2" x="12.7" y="5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O3" x="12.7" y="2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O4" x="12.7" y="0" length="middle" direction="oc" rot="R180"/>
<pin name="O5" x="12.7" y="-2.54" length="middle" direction="oc" rot="R180"/>
<pin name="O6" x="12.7" y="-5.08" length="middle" direction="oc" rot="R180"/>
<pin name="O7" x="12.7" y="-7.62" length="middle" direction="oc" rot="R180"/>
<pin name="CD+" x="12.7" y="-10.16" length="middle" direction="pas" rot="R180"/>
<pin name="GND" x="-12.7" y="-10.16" length="middle" direction="pwr"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ULN2003A" prefix="IC">
<description>&lt;b&gt;DRIVER ARRAY&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="2001A" x="0" y="0"/>
</gates>
<devices>
<device name="N" package="DIL16">
<connects>
<connect gate="A" pin="CD+" pad="9"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="I1" pad="1"/>
<connect gate="A" pin="I2" pad="2"/>
<connect gate="A" pin="I3" pad="3"/>
<connect gate="A" pin="I4" pad="4"/>
<connect gate="A" pin="I5" pad="5"/>
<connect gate="A" pin="I6" pad="6"/>
<connect gate="A" pin="I7" pad="7"/>
<connect gate="A" pin="O1" pad="16"/>
<connect gate="A" pin="O2" pad="15"/>
<connect gate="A" pin="O3" pad="14"/>
<connect gate="A" pin="O4" pad="13"/>
<connect gate="A" pin="O5" pad="12"/>
<connect gate="A" pin="O6" pad="11"/>
<connect gate="A" pin="O7" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="SO16">
<connects>
<connect gate="A" pin="CD+" pad="9"/>
<connect gate="A" pin="GND" pad="8"/>
<connect gate="A" pin="I1" pad="1"/>
<connect gate="A" pin="I2" pad="2"/>
<connect gate="A" pin="I3" pad="3"/>
<connect gate="A" pin="I4" pad="4"/>
<connect gate="A" pin="I5" pad="5"/>
<connect gate="A" pin="I6" pad="6"/>
<connect gate="A" pin="I7" pad="7"/>
<connect gate="A" pin="O1" pad="16"/>
<connect gate="A" pin="O2" pad="15"/>
<connect gate="A" pin="O3" pad="14"/>
<connect gate="A" pin="O4" pad="13"/>
<connect gate="A" pin="O5" pad="12"/>
<connect gate="A" pin="O6" pad="11"/>
<connect gate="A" pin="O7" pad="10"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pinhead">
<description>&lt;b&gt;Pin Header Connectors&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="2X20">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="-2.54" x2="-23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="-2.54" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="-2.54" x2="-20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="-2.54" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="-2.54" x2="-18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="-2.54" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="-2.54" x2="-15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="-2.54" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="-2.54" x2="-13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="-1.905" x2="-25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="1.905" x2="-24.765" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-24.765" y1="2.54" x2="-23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-23.495" y1="2.54" x2="-22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.225" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-22.225" y1="2.54" x2="-20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-20.955" y1="2.54" x2="-20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-19.685" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-19.685" y1="2.54" x2="-18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-18.415" y1="2.54" x2="-17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.145" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-17.145" y1="2.54" x2="-15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-15.875" y1="2.54" x2="-15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-14.605" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-14.605" y1="2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="14.605" y1="2.54" x2="15.24" y2="1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="15.875" y2="2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="2.54" x2="17.78" y2="1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="18.415" y2="2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="2.54" x2="20.32" y2="1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="20.955" y2="2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="2.54" x2="22.86" y2="1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.225" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.225" y1="-2.54" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.955" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="19.685" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="19.685" y1="-2.54" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="18.415" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.145" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="17.145" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.875" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="14.605" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="13.335" y1="-2.54" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="-2.54" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-2.54" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="1.905" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="23.495" y2="2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="2.54" x2="25.4" y2="1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="24.765" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="24.765" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="23.495" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="25.4" y1="1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<pad name="1" x="-24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-24.13" y="1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="1.27" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="1.27" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="1.27" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="1.27" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="1.27" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="1.27" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="1.27" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="1.27" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="1.27" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="1.27" drill="1.016" shape="octagon"/>
<text x="-25.4" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-25.4" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-24.384" y1="-1.524" x2="-23.876" y2="-1.016" layer="51"/>
<rectangle x1="-24.384" y1="1.016" x2="-23.876" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="1.016" x2="-21.336" y2="1.524" layer="51"/>
<rectangle x1="-21.844" y1="-1.524" x2="-21.336" y2="-1.016" layer="51"/>
<rectangle x1="-19.304" y1="1.016" x2="-18.796" y2="1.524" layer="51"/>
<rectangle x1="-19.304" y1="-1.524" x2="-18.796" y2="-1.016" layer="51"/>
<rectangle x1="-16.764" y1="1.016" x2="-16.256" y2="1.524" layer="51"/>
<rectangle x1="-14.224" y1="1.016" x2="-13.716" y2="1.524" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-16.764" y1="-1.524" x2="-16.256" y2="-1.016" layer="51"/>
<rectangle x1="-14.224" y1="-1.524" x2="-13.716" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
<rectangle x1="13.716" y1="1.016" x2="14.224" y2="1.524" layer="51"/>
<rectangle x1="13.716" y1="-1.524" x2="14.224" y2="-1.016" layer="51"/>
<rectangle x1="16.256" y1="1.016" x2="16.764" y2="1.524" layer="51"/>
<rectangle x1="16.256" y1="-1.524" x2="16.764" y2="-1.016" layer="51"/>
<rectangle x1="18.796" y1="1.016" x2="19.304" y2="1.524" layer="51"/>
<rectangle x1="18.796" y1="-1.524" x2="19.304" y2="-1.016" layer="51"/>
<rectangle x1="21.336" y1="1.016" x2="21.844" y2="1.524" layer="51"/>
<rectangle x1="21.336" y1="-1.524" x2="21.844" y2="-1.016" layer="51"/>
<rectangle x1="23.876" y1="1.016" x2="24.384" y2="1.524" layer="51"/>
<rectangle x1="23.876" y1="-1.524" x2="24.384" y2="-1.016" layer="51"/>
</package>
<package name="2X20/90">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-25.4" y1="-1.905" x2="-22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-22.86" y1="0.635" x2="-25.4" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-25.4" y1="0.635" x2="-25.4" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-24.13" y1="6.985" x2="-24.13" y2="1.27" width="0.762" layer="21"/>
<wire x1="-22.86" y1="-1.905" x2="-20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-20.32" y1="0.635" x2="-22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-21.59" y1="6.985" x2="-21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="-20.32" y1="-1.905" x2="-17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-17.78" y1="0.635" x2="-20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-19.05" y1="6.985" x2="-19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="-17.78" y1="-1.905" x2="-15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-15.24" y1="0.635" x2="-17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-16.51" y1="6.985" x2="-16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="-15.24" y1="-1.905" x2="-12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="0.635" x2="-15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="0.635" x2="-12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-11.43" y1="6.985" x2="-11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="6.985" x2="-6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="6.985" x2="-3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="6.985" x2="-1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="0" y1="-1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="1.27" width="0.762" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="3.81" y1="6.985" x2="3.81" y2="1.27" width="0.762" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="6.35" y1="6.985" x2="6.35" y2="1.27" width="0.762" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="10.16" y1="0.635" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="8.89" y1="6.985" x2="8.89" y2="1.27" width="0.762" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="12.7" y1="0.635" x2="10.16" y2="0.635" width="0.1524" layer="21"/>
<wire x1="11.43" y1="6.985" x2="11.43" y2="1.27" width="0.762" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="15.24" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="15.24" y1="0.635" x2="12.7" y2="0.635" width="0.1524" layer="21"/>
<wire x1="13.97" y1="6.985" x2="13.97" y2="1.27" width="0.762" layer="21"/>
<wire x1="15.24" y1="-1.905" x2="17.78" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="17.78" y1="0.635" x2="15.24" y2="0.635" width="0.1524" layer="21"/>
<wire x1="16.51" y1="6.985" x2="16.51" y2="1.27" width="0.762" layer="21"/>
<wire x1="17.78" y1="-1.905" x2="20.32" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="20.32" y1="0.635" x2="17.78" y2="0.635" width="0.1524" layer="21"/>
<wire x1="19.05" y1="6.985" x2="19.05" y2="1.27" width="0.762" layer="21"/>
<wire x1="20.32" y1="-1.905" x2="22.86" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="22.86" y1="0.635" x2="20.32" y2="0.635" width="0.1524" layer="21"/>
<wire x1="21.59" y1="6.985" x2="21.59" y2="1.27" width="0.762" layer="21"/>
<wire x1="22.86" y1="-1.905" x2="25.4" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="25.4" y1="-1.905" x2="25.4" y2="0.635" width="0.1524" layer="21"/>
<wire x1="25.4" y1="0.635" x2="22.86" y2="0.635" width="0.1524" layer="21"/>
<wire x1="24.13" y1="6.985" x2="24.13" y2="1.27" width="0.762" layer="21"/>
<pad name="2" x="-24.13" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="4" x="-21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="6" x="-19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="8" x="-16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="10" x="-13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="12" x="-11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="14" x="-8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="16" x="-6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="18" x="-3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="20" x="-1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="22" x="1.27" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="24" x="3.81" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="26" x="6.35" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="28" x="8.89" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="30" x="11.43" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="32" x="13.97" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="34" x="16.51" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="36" x="19.05" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="38" x="21.59" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="1" x="-24.13" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="3" x="-21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="5" x="-19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="7" x="-16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="9" x="-13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="11" x="-11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="13" x="-8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="15" x="-6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="17" x="-3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="19" x="-1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="21" x="1.27" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="23" x="3.81" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="25" x="6.35" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="27" x="8.89" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="29" x="11.43" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="31" x="13.97" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="33" x="16.51" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="35" x="19.05" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="37" x="21.59" y="-6.35" drill="1.016" shape="octagon"/>
<pad name="40" x="24.13" y="-3.81" drill="1.016" shape="octagon"/>
<pad name="39" x="24.13" y="-6.35" drill="1.016" shape="octagon"/>
<text x="-26.035" y="-3.81" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="27.305" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-24.511" y1="0.635" x2="-23.749" y2="1.143" layer="21"/>
<rectangle x1="-21.971" y1="0.635" x2="-21.209" y2="1.143" layer="21"/>
<rectangle x1="-19.431" y1="0.635" x2="-18.669" y2="1.143" layer="21"/>
<rectangle x1="-16.891" y1="0.635" x2="-16.129" y2="1.143" layer="21"/>
<rectangle x1="-14.351" y1="0.635" x2="-13.589" y2="1.143" layer="21"/>
<rectangle x1="-11.811" y1="0.635" x2="-11.049" y2="1.143" layer="21"/>
<rectangle x1="-9.271" y1="0.635" x2="-8.509" y2="1.143" layer="21"/>
<rectangle x1="-6.731" y1="0.635" x2="-5.969" y2="1.143" layer="21"/>
<rectangle x1="-4.191" y1="0.635" x2="-3.429" y2="1.143" layer="21"/>
<rectangle x1="-1.651" y1="0.635" x2="-0.889" y2="1.143" layer="21"/>
<rectangle x1="0.889" y1="0.635" x2="1.651" y2="1.143" layer="21"/>
<rectangle x1="3.429" y1="0.635" x2="4.191" y2="1.143" layer="21"/>
<rectangle x1="5.969" y1="0.635" x2="6.731" y2="1.143" layer="21"/>
<rectangle x1="8.509" y1="0.635" x2="9.271" y2="1.143" layer="21"/>
<rectangle x1="11.049" y1="0.635" x2="11.811" y2="1.143" layer="21"/>
<rectangle x1="13.589" y1="0.635" x2="14.351" y2="1.143" layer="21"/>
<rectangle x1="16.129" y1="0.635" x2="16.891" y2="1.143" layer="21"/>
<rectangle x1="18.669" y1="0.635" x2="19.431" y2="1.143" layer="21"/>
<rectangle x1="21.209" y1="0.635" x2="21.971" y2="1.143" layer="21"/>
<rectangle x1="23.749" y1="0.635" x2="24.511" y2="1.143" layer="21"/>
<rectangle x1="-24.511" y1="-2.921" x2="-23.749" y2="-1.905" layer="21"/>
<rectangle x1="-21.971" y1="-2.921" x2="-21.209" y2="-1.905" layer="21"/>
<rectangle x1="-24.511" y1="-5.461" x2="-23.749" y2="-4.699" layer="21"/>
<rectangle x1="-24.511" y1="-4.699" x2="-23.749" y2="-2.921" layer="51"/>
<rectangle x1="-21.971" y1="-4.699" x2="-21.209" y2="-2.921" layer="51"/>
<rectangle x1="-21.971" y1="-5.461" x2="-21.209" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-2.921" x2="-18.669" y2="-1.905" layer="21"/>
<rectangle x1="-16.891" y1="-2.921" x2="-16.129" y2="-1.905" layer="21"/>
<rectangle x1="-19.431" y1="-5.461" x2="-18.669" y2="-4.699" layer="21"/>
<rectangle x1="-19.431" y1="-4.699" x2="-18.669" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-4.699" x2="-16.129" y2="-2.921" layer="51"/>
<rectangle x1="-16.891" y1="-5.461" x2="-16.129" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-2.921" x2="-13.589" y2="-1.905" layer="21"/>
<rectangle x1="-14.351" y1="-5.461" x2="-13.589" y2="-4.699" layer="21"/>
<rectangle x1="-14.351" y1="-4.699" x2="-13.589" y2="-2.921" layer="51"/>
<rectangle x1="-11.811" y1="-2.921" x2="-11.049" y2="-1.905" layer="21"/>
<rectangle x1="-9.271" y1="-2.921" x2="-8.509" y2="-1.905" layer="21"/>
<rectangle x1="-11.811" y1="-5.461" x2="-11.049" y2="-4.699" layer="21"/>
<rectangle x1="-11.811" y1="-4.699" x2="-11.049" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-4.699" x2="-8.509" y2="-2.921" layer="51"/>
<rectangle x1="-9.271" y1="-5.461" x2="-8.509" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-2.921" x2="-5.969" y2="-1.905" layer="21"/>
<rectangle x1="-4.191" y1="-2.921" x2="-3.429" y2="-1.905" layer="21"/>
<rectangle x1="-6.731" y1="-5.461" x2="-5.969" y2="-4.699" layer="21"/>
<rectangle x1="-6.731" y1="-4.699" x2="-5.969" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-4.699" x2="-3.429" y2="-2.921" layer="51"/>
<rectangle x1="-4.191" y1="-5.461" x2="-3.429" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-2.921" x2="-0.889" y2="-1.905" layer="21"/>
<rectangle x1="-1.651" y1="-5.461" x2="-0.889" y2="-4.699" layer="21"/>
<rectangle x1="-1.651" y1="-4.699" x2="-0.889" y2="-2.921" layer="51"/>
<rectangle x1="0.889" y1="-2.921" x2="1.651" y2="-1.905" layer="21"/>
<rectangle x1="3.429" y1="-2.921" x2="4.191" y2="-1.905" layer="21"/>
<rectangle x1="0.889" y1="-5.461" x2="1.651" y2="-4.699" layer="21"/>
<rectangle x1="0.889" y1="-4.699" x2="1.651" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-4.699" x2="4.191" y2="-2.921" layer="51"/>
<rectangle x1="3.429" y1="-5.461" x2="4.191" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-2.921" x2="6.731" y2="-1.905" layer="21"/>
<rectangle x1="8.509" y1="-2.921" x2="9.271" y2="-1.905" layer="21"/>
<rectangle x1="5.969" y1="-5.461" x2="6.731" y2="-4.699" layer="21"/>
<rectangle x1="5.969" y1="-4.699" x2="6.731" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-4.699" x2="9.271" y2="-2.921" layer="51"/>
<rectangle x1="8.509" y1="-5.461" x2="9.271" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-2.921" x2="11.811" y2="-1.905" layer="21"/>
<rectangle x1="11.049" y1="-5.461" x2="11.811" y2="-4.699" layer="21"/>
<rectangle x1="11.049" y1="-4.699" x2="11.811" y2="-2.921" layer="51"/>
<rectangle x1="13.589" y1="-2.921" x2="14.351" y2="-1.905" layer="21"/>
<rectangle x1="16.129" y1="-2.921" x2="16.891" y2="-1.905" layer="21"/>
<rectangle x1="13.589" y1="-5.461" x2="14.351" y2="-4.699" layer="21"/>
<rectangle x1="13.589" y1="-4.699" x2="14.351" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-4.699" x2="16.891" y2="-2.921" layer="51"/>
<rectangle x1="16.129" y1="-5.461" x2="16.891" y2="-4.699" layer="21"/>
<rectangle x1="18.669" y1="-2.921" x2="19.431" y2="-1.905" layer="21"/>
<rectangle x1="21.209" y1="-2.921" x2="21.971" y2="-1.905" layer="21"/>
<rectangle x1="18.669" y1="-5.461" x2="19.431" y2="-4.699" layer="21"/>
<rectangle x1="18.669" y1="-4.699" x2="19.431" y2="-2.921" layer="51"/>
<rectangle x1="21.209" y1="-4.699" x2="21.971" y2="-2.921" layer="51"/>
<rectangle x1="21.209" y1="-5.461" x2="21.971" y2="-4.699" layer="21"/>
<rectangle x1="23.749" y1="-2.921" x2="24.511" y2="-1.905" layer="21"/>
<rectangle x1="23.749" y1="-5.461" x2="24.511" y2="-4.699" layer="21"/>
<rectangle x1="23.749" y1="-4.699" x2="24.511" y2="-2.921" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="PINH2X20">
<wire x1="-6.35" y1="-27.94" x2="8.89" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-27.94" x2="8.89" y2="25.4" width="0.4064" layer="94"/>
<wire x1="8.89" y1="25.4" x2="-6.35" y2="25.4" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="25.4" x2="-6.35" y2="-27.94" width="0.4064" layer="94"/>
<text x="-6.35" y="26.035" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="21" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="22" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="23" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="24" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="25" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="26" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="27" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="28" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="29" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="30" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="31" x="-2.54" y="-15.24" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="32" x="5.08" y="-15.24" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="33" x="-2.54" y="-17.78" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="34" x="5.08" y="-17.78" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="35" x="-2.54" y="-20.32" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="36" x="5.08" y="-20.32" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="37" x="-2.54" y="-22.86" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="38" x="5.08" y="-22.86" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="39" x="-2.54" y="-25.4" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="40" x="5.08" y="-25.4" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X20" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="PINH2X20" x="0" y="0"/>
</gates>
<devices>
<device name="" package="2X20">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="/90" package="2X20/90">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="21" pad="21"/>
<connect gate="A" pin="22" pad="22"/>
<connect gate="A" pin="23" pad="23"/>
<connect gate="A" pin="24" pad="24"/>
<connect gate="A" pin="25" pad="25"/>
<connect gate="A" pin="26" pad="26"/>
<connect gate="A" pin="27" pad="27"/>
<connect gate="A" pin="28" pad="28"/>
<connect gate="A" pin="29" pad="29"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="30" pad="30"/>
<connect gate="A" pin="31" pad="31"/>
<connect gate="A" pin="32" pad="32"/>
<connect gate="A" pin="33" pad="33"/>
<connect gate="A" pin="34" pad="34"/>
<connect gate="A" pin="35" pad="35"/>
<connect gate="A" pin="36" pad="36"/>
<connect gate="A" pin="37" pad="37"/>
<connect gate="A" pin="38" pad="38"/>
<connect gate="A" pin="39" pad="39"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="40" pad="40"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="PRL1632">
<description>&lt;b&gt;PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)&lt;/b&gt;&lt;p&gt;
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<wire x1="0.7275" y1="-1.5228" x2="-0.7277" y2="-1.5228" width="0.1524" layer="51"/>
<wire x1="0.7275" y1="1.5228" x2="-0.7152" y2="1.5228" width="0.1524" layer="51"/>
<smd name="2" x="0.822" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="1" x="-0.822" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-1.4" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.4" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.4" y2="1.6" layer="51"/>
<rectangle x1="0.4" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-EU-1">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU-1" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PRL1632" package="PRL1632">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
Please keep in mind, that these devices are necessary for the
automatic wiring of the supply signals.&lt;p&gt;
The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="VDD">
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-1.905" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VDD" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="VDD" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VDD" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="40xx">
<description>&lt;b&gt;CMOS Logic Devices, 4000 Series&lt;/b&gt;&lt;p&gt;
Based on the following sources:
&lt;ul&gt;
&lt;li&gt;Motorola &lt;i&gt;CMOS LOGIC DATA&lt;/i&gt;; book, 02/88, DL131 REV 1
&lt;li&gt;http://www.elexp.com
&lt;li&gt;http://www.intersil.com
&lt;li&gt;http://www.ls3c.com.tw/product/1/COMOS.html
&lt;/ul&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DIL24-6">
<description>&lt;b&gt;Dual In Line Package&lt;/b&gt; 0.6 inch</description>
<wire x1="-15.113" y1="-1.27" x2="-15.113" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="1.27" x2="-15.113" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<wire x1="15.113" y1="-6.604" x2="15.113" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="6.604" x2="-15.113" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="6.604" x2="15.113" y2="6.604" width="0.1524" layer="21"/>
<wire x1="-15.113" y1="-6.604" x2="15.113" y2="-6.604" width="0.1524" layer="21"/>
<pad name="1" x="-13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="-8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="-6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="-3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="-1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="1.27" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="3.81" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="9" x="6.35" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="10" x="8.89" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="11" x="11.43" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="12" x="13.97" y="-7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="13" x="13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="14" x="11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="15" x="8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="16" x="6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="17" x="3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="18" x="1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="19" x="-1.27" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="20" x="-3.81" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="21" x="-6.35" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="22" x="-8.89" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="23" x="-11.43" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<pad name="24" x="-13.97" y="7.62" drill="0.8128" shape="long" rot="R90"/>
<text x="-15.621" y="-6.35" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-12.065" y="-0.889" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SO24W">
<description>&lt;b&gt;Wide Small Outline package&lt;/b&gt; 300 mil</description>
<wire x1="7.366" y1="3.7338" x2="-7.366" y2="3.7338" width="0.1524" layer="21"/>
<wire x1="7.366" y1="-3.7338" x2="7.747" y2="-3.3528" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.747" y1="3.3528" x2="-7.366" y2="3.7338" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.366" y1="3.7338" x2="7.747" y2="3.3528" width="0.1524" layer="21" curve="-90"/>
<wire x1="-7.747" y1="-3.3528" x2="-7.366" y2="-3.7338" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.366" y1="-3.7338" x2="7.366" y2="-3.7338" width="0.1524" layer="21"/>
<wire x1="7.747" y1="-3.3528" x2="7.747" y2="3.3528" width="0.1524" layer="21"/>
<wire x1="-7.747" y1="3.3528" x2="-7.747" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-7.747" y1="1.27" x2="-7.747" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-7.747" y1="-1.27" x2="-7.747" y2="-3.3528" width="0.1524" layer="21"/>
<wire x1="-7.747" y1="-3.3782" x2="7.747" y2="-3.3782" width="0.0508" layer="21"/>
<wire x1="-7.747" y1="1.27" x2="-7.747" y2="-1.27" width="0.1524" layer="21" curve="-180"/>
<smd name="1" x="-6.985" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="2" x="-5.715" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="3" x="-4.445" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="4" x="-3.175" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="5" x="-1.905" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="6" x="-0.635" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="7" x="0.635" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="8" x="1.905" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="17" x="1.905" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="18" x="0.635" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="19" x="-0.635" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="20" x="-1.905" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="21" x="-3.175" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="22" x="-4.445" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="23" x="-5.715" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="24" x="-6.985" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="16" x="3.175" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="15" x="4.445" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="14" x="5.715" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="13" x="6.985" y="5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="9" x="3.175" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="10" x="4.445" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="11" x="5.715" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<smd name="12" x="6.985" y="-5.0292" dx="0.6604" dy="2.032" layer="1"/>
<text x="-5.588" y="-0.508" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-8.128" y="-3.302" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<rectangle x1="-7.239" y1="-3.8608" x2="-6.731" y2="-3.7338" layer="21"/>
<rectangle x1="-7.239" y1="-5.334" x2="-6.731" y2="-3.8608" layer="51"/>
<rectangle x1="-5.969" y1="-3.8608" x2="-5.461" y2="-3.7338" layer="21"/>
<rectangle x1="-5.969" y1="-5.334" x2="-5.461" y2="-3.8608" layer="51"/>
<rectangle x1="-4.699" y1="-3.8608" x2="-4.191" y2="-3.7338" layer="21"/>
<rectangle x1="-4.699" y1="-5.334" x2="-4.191" y2="-3.8608" layer="51"/>
<rectangle x1="-3.429" y1="-3.8608" x2="-2.921" y2="-3.7338" layer="21"/>
<rectangle x1="-3.429" y1="-5.334" x2="-2.921" y2="-3.8608" layer="51"/>
<rectangle x1="-2.159" y1="-5.334" x2="-1.651" y2="-3.8608" layer="51"/>
<rectangle x1="-2.159" y1="-3.8608" x2="-1.651" y2="-3.7338" layer="21"/>
<rectangle x1="-0.889" y1="-3.8608" x2="-0.381" y2="-3.7338" layer="21"/>
<rectangle x1="-0.889" y1="-5.334" x2="-0.381" y2="-3.8608" layer="51"/>
<rectangle x1="0.381" y1="-3.8608" x2="0.889" y2="-3.7338" layer="21"/>
<rectangle x1="0.381" y1="-5.334" x2="0.889" y2="-3.8608" layer="51"/>
<rectangle x1="1.651" y1="-3.8608" x2="2.159" y2="-3.7338" layer="21"/>
<rectangle x1="1.651" y1="-5.334" x2="2.159" y2="-3.8608" layer="51"/>
<rectangle x1="-7.239" y1="3.8608" x2="-6.731" y2="5.334" layer="51"/>
<rectangle x1="-7.239" y1="3.7338" x2="-6.731" y2="3.8608" layer="21"/>
<rectangle x1="-5.969" y1="3.7338" x2="-5.461" y2="3.8608" layer="21"/>
<rectangle x1="-5.969" y1="3.8608" x2="-5.461" y2="5.334" layer="51"/>
<rectangle x1="-4.699" y1="3.7338" x2="-4.191" y2="3.8608" layer="21"/>
<rectangle x1="-4.699" y1="3.8608" x2="-4.191" y2="5.334" layer="51"/>
<rectangle x1="-3.429" y1="3.7338" x2="-2.921" y2="3.8608" layer="21"/>
<rectangle x1="-3.429" y1="3.8608" x2="-2.921" y2="5.334" layer="51"/>
<rectangle x1="-2.159" y1="3.7338" x2="-1.651" y2="3.8608" layer="21"/>
<rectangle x1="-2.159" y1="3.8608" x2="-1.651" y2="5.334" layer="51"/>
<rectangle x1="-0.889" y1="3.7338" x2="-0.381" y2="3.8608" layer="21"/>
<rectangle x1="-0.889" y1="3.8608" x2="-0.381" y2="5.334" layer="51"/>
<rectangle x1="0.381" y1="3.7338" x2="0.889" y2="3.8608" layer="21"/>
<rectangle x1="0.381" y1="3.8608" x2="0.889" y2="5.334" layer="51"/>
<rectangle x1="1.651" y1="3.7338" x2="2.159" y2="3.8608" layer="21"/>
<rectangle x1="1.651" y1="3.8608" x2="2.159" y2="5.334" layer="51"/>
<rectangle x1="2.921" y1="3.7338" x2="3.429" y2="3.8608" layer="21"/>
<rectangle x1="4.191" y1="3.7338" x2="4.699" y2="3.8608" layer="21"/>
<rectangle x1="5.461" y1="3.7338" x2="5.969" y2="3.8608" layer="21"/>
<rectangle x1="6.731" y1="3.7338" x2="7.239" y2="3.8608" layer="21"/>
<rectangle x1="2.921" y1="3.8608" x2="3.429" y2="5.334" layer="51"/>
<rectangle x1="4.191" y1="3.8608" x2="4.699" y2="5.334" layer="51"/>
<rectangle x1="5.461" y1="3.8608" x2="5.969" y2="5.334" layer="51"/>
<rectangle x1="6.731" y1="3.8608" x2="7.239" y2="5.334" layer="51"/>
<rectangle x1="2.921" y1="-3.8608" x2="3.429" y2="-3.7338" layer="21"/>
<rectangle x1="4.191" y1="-3.8608" x2="4.699" y2="-3.7338" layer="21"/>
<rectangle x1="5.461" y1="-3.8608" x2="5.969" y2="-3.7338" layer="21"/>
<rectangle x1="6.731" y1="-3.8608" x2="7.239" y2="-3.7338" layer="21"/>
<rectangle x1="2.921" y1="-5.334" x2="3.429" y2="-3.8608" layer="51"/>
<rectangle x1="4.191" y1="-5.334" x2="4.699" y2="-3.8608" layer="51"/>
<rectangle x1="5.461" y1="-5.334" x2="5.969" y2="-3.8608" layer="51"/>
<rectangle x1="6.731" y1="-5.334" x2="7.239" y2="-3.8608" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="4067">
<wire x1="-7.62" y1="27.94" x2="-7.62" y2="-5.48" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="-5.48" x2="7.62" y2="-5.48" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.48" x2="7.62" y2="27.94" width="0.4064" layer="94"/>
<wire x1="7.62" y1="27.94" x2="-7.62" y2="27.94" width="0.4064" layer="94"/>
<text x="-7.62" y="28.575" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-8.02" size="1.778" layer="96">&gt;VALUE</text>
<pin name="O/I" x="-12.7" y="25.4" length="middle" direction="out"/>
<pin name="X7" x="-12.7" y="22.86" length="middle" direction="in"/>
<pin name="X6" x="-12.7" y="20.32" length="middle" direction="in"/>
<pin name="X5" x="-12.7" y="17.78" length="middle" direction="in"/>
<pin name="X4" x="-12.7" y="15.24" length="middle" direction="in"/>
<pin name="X3" x="-12.7" y="12.7" length="middle" direction="in"/>
<pin name="X2" x="-12.7" y="10.16" length="middle" direction="in"/>
<pin name="X1" x="-12.7" y="7.62" length="middle" direction="in"/>
<pin name="X0" x="-12.7" y="5.08" length="middle" direction="in"/>
<pin name="A" x="-12.7" y="2.54" length="middle" direction="in"/>
<pin name="B" x="-12.7" y="0" length="middle" direction="in"/>
<pin name="VSS" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="VDD" x="12.7" y="25.4" length="middle" direction="in" rot="R180"/>
<pin name="X15" x="12.7" y="22.86" length="middle" direction="in" rot="R180"/>
<pin name="X14" x="12.7" y="20.32" length="middle" direction="in" rot="R180"/>
<pin name="X13" x="12.7" y="17.78" length="middle" direction="in" rot="R180"/>
<pin name="X12" x="12.7" y="15.24" length="middle" direction="in" rot="R180"/>
<pin name="X11" x="12.7" y="12.7" length="middle" direction="in" rot="R180"/>
<pin name="X10" x="12.7" y="10.16" length="middle" direction="in" rot="R180"/>
<pin name="X9" x="12.7" y="7.62" length="middle" direction="in" rot="R180"/>
<pin name="X8" x="12.7" y="5.08" length="middle" direction="in" rot="R180"/>
<pin name="INH" x="12.7" y="2.54" length="middle" direction="in" rot="R180"/>
<pin name="C" x="12.7" y="0" length="middle" direction="in" rot="R180"/>
<pin name="D" x="12.7" y="-2.54" length="middle" direction="in" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="4067" prefix="IC">
<description>16-channel &lt;b&gt;ANALOG MULTIPLEXER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="4067" x="17.78" y="-2.54"/>
</gates>
<devices>
<device name="N" package="DIL24-6">
<connects>
<connect gate="A" pin="A" pad="10"/>
<connect gate="A" pin="B" pad="11"/>
<connect gate="A" pin="C" pad="14"/>
<connect gate="A" pin="D" pad="13"/>
<connect gate="A" pin="INH" pad="15"/>
<connect gate="A" pin="O/I" pad="1"/>
<connect gate="A" pin="VDD" pad="24"/>
<connect gate="A" pin="VSS" pad="12"/>
<connect gate="A" pin="X0" pad="9"/>
<connect gate="A" pin="X1" pad="8"/>
<connect gate="A" pin="X10" pad="21"/>
<connect gate="A" pin="X11" pad="20"/>
<connect gate="A" pin="X12" pad="19"/>
<connect gate="A" pin="X13" pad="18"/>
<connect gate="A" pin="X14" pad="17"/>
<connect gate="A" pin="X15" pad="16"/>
<connect gate="A" pin="X2" pad="7"/>
<connect gate="A" pin="X3" pad="6"/>
<connect gate="A" pin="X4" pad="5"/>
<connect gate="A" pin="X5" pad="4"/>
<connect gate="A" pin="X6" pad="3"/>
<connect gate="A" pin="X7" pad="2"/>
<connect gate="A" pin="X8" pad="23"/>
<connect gate="A" pin="X9" pad="22"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="D" package="SO24W">
<connects>
<connect gate="A" pin="A" pad="10"/>
<connect gate="A" pin="B" pad="11"/>
<connect gate="A" pin="C" pad="14"/>
<connect gate="A" pin="D" pad="13"/>
<connect gate="A" pin="INH" pad="15"/>
<connect gate="A" pin="O/I" pad="1"/>
<connect gate="A" pin="VDD" pad="24"/>
<connect gate="A" pin="VSS" pad="12"/>
<connect gate="A" pin="X0" pad="9"/>
<connect gate="A" pin="X1" pad="8"/>
<connect gate="A" pin="X10" pad="21"/>
<connect gate="A" pin="X11" pad="20"/>
<connect gate="A" pin="X12" pad="19"/>
<connect gate="A" pin="X13" pad="18"/>
<connect gate="A" pin="X14" pad="17"/>
<connect gate="A" pin="X15" pad="16"/>
<connect gate="A" pin="X2" pad="7"/>
<connect gate="A" pin="X3" pad="6"/>
<connect gate="A" pin="X4" pad="5"/>
<connect gate="A" pin="X5" pad="4"/>
<connect gate="A" pin="X6" pad="3"/>
<connect gate="A" pin="X7" pad="2"/>
<connect gate="A" pin="X8" pad="23"/>
<connect gate="A" pin="X9" pad="22"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit">
<packages>
<package name="1206">
<description>&lt;b&gt;CHICAGO MINIATURE LAMP, INC.&lt;/b&gt;&lt;p&gt;
7022X Series SMT LEDs 1206 Package Size</description>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.5" x2="0.55" y2="-0.5" width="0.1016" layer="21" curve="95.452622" cap="flat"/>
<wire x1="-0.55" y1="-0.5" x2="-0.55" y2="0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<wire x1="-0.55" y1="0.5" x2="0.55" y2="0.5" width="0.1016" layer="21" curve="-95.452622" cap="flat"/>
<wire x1="0.55" y1="0.5" x2="0.55" y2="-0.5" width="0.1016" layer="51" curve="-84.547378" cap="flat"/>
<smd name="A" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="C" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.1" y1="-0.1" x2="0.1" y2="0.1" layer="21"/>
<rectangle x1="0.45" y1="-0.7" x2="0.8" y2="-0.45" layer="51"/>
<rectangle x1="0.8" y1="-0.7" x2="0.9" y2="0.5" layer="51"/>
<rectangle x1="0.8" y1="0.55" x2="0.9" y2="0.7" layer="51"/>
<rectangle x1="-0.9" y1="-0.7" x2="-0.8" y2="0.5" layer="51"/>
<rectangle x1="-0.9" y1="0.55" x2="-0.8" y2="0.7" layer="51"/>
<rectangle x1="0.45" y1="-0.7" x2="0.6" y2="-0.45" layer="21"/>
</package>
<package name="LD260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, square, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="-1.27" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="0" y1="1.27" x2="0.9917" y2="0.7934" width="0.1524" layer="21" curve="-51.33923" cap="flat"/>
<wire x1="-0.9917" y1="0.7934" x2="0" y2="1.27" width="0.1524" layer="21" curve="-51.33923" cap="flat"/>
<wire x1="0" y1="-1.27" x2="0.9917" y2="-0.7934" width="0.1524" layer="21" curve="51.33923" cap="flat"/>
<wire x1="-0.9917" y1="-0.7934" x2="0" y2="-1.27" width="0.1524" layer="21" curve="51.33923" cap="flat"/>
<wire x1="0.9558" y1="-0.8363" x2="1.27" y2="0" width="0.1524" layer="51" curve="41.185419" cap="flat"/>
<wire x1="0.9756" y1="0.813" x2="1.2699" y2="0" width="0.1524" layer="51" curve="-39.806332" cap="flat"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="-0.8265" width="0.1524" layer="51" curve="40.600331" cap="flat"/>
<wire x1="-1.27" y1="0" x2="-0.9643" y2="0.8265" width="0.1524" layer="51" curve="-40.600331" cap="flat"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.635" x2="2.032" y2="0.635" layer="51"/>
<rectangle x1="1.905" y1="-0.635" x2="2.032" y2="0.635" layer="21"/>
</package>
<package name="LED2X5">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
2 x 5 mm, rectangle</description>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="2.54" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="0" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="-0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0.381" x2="0.508" y2="0" width="0.1524" layer="51"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-0.254" x2="1.143" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.762" x2="0.9398" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="0.9398" y1="-0.6096" x2="1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.651" y2="-0.762" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-0.762" x2="1.4478" y2="-0.6096" width="0.1524" layer="51"/>
<wire x1="1.4478" y1="-0.6096" x2="1.651" y2="-0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-2.54" y="-2.413" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="2.159" y1="-1.27" x2="2.413" y2="1.27" layer="21"/>
</package>
<package name="LED3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="1.5748" y1="-1.27" x2="1.5748" y2="1.27" width="0.254" layer="51"/>
<wire x1="-1.524" y1="0" x2="-1.1708" y2="0.9756" width="0.1524" layer="51" curve="-39.80361" cap="flat"/>
<wire x1="-1.524" y1="0" x2="-1.1391" y2="-1.0125" width="0.1524" layer="51" curve="41.633208" cap="flat"/>
<wire x1="1.1571" y1="0.9918" x2="1.524" y2="0" width="0.1524" layer="51" curve="-40.601165" cap="flat"/>
<wire x1="1.1708" y1="-0.9756" x2="1.524" y2="0" width="0.1524" layer="51" curve="39.80361" cap="flat"/>
<wire x1="0" y1="1.524" x2="1.2401" y2="0.8858" width="0.1524" layer="21" curve="-54.461337" cap="flat"/>
<wire x1="-1.2192" y1="0.9144" x2="0" y2="1.524" width="0.1524" layer="21" curve="-53.130102" cap="flat"/>
<wire x1="0" y1="-1.524" x2="1.203" y2="-0.9356" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-1.203" y1="-0.9356" x2="0" y2="-1.524" width="0.1524" layer="21" curve="52.126876" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.254" layer="21" curve="-50.193108" cap="flat"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.254" layer="21" curve="-61.926949" cap="flat"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.254" layer="21" curve="49.763022" cap="flat"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.254" layer="21" curve="60.255215" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7891" y2="0.9634" width="0.254" layer="51" curve="-28.301701" cap="flat"/>
<wire x1="-2.032" y1="0" x2="-1.7306" y2="-1.065" width="0.254" layer="51" curve="31.60822" cap="flat"/>
<wire x1="-2.5908" y1="1.7272" x2="-1.8542" y2="1.7272" width="0.127" layer="21"/>
<wire x1="-2.2352" y1="1.3208" x2="-2.2352" y2="2.1082" width="0.127" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="1.905" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.905" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LED5MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
5 mm, round</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.254" layer="21" curve="-286.260205" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" diameter="1.6764" shape="octagon"/>
<text x="3.175" y="0.5334" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.2004" y="-1.8034" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="LSU260">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
1 mm, round, Siemens</description>
<wire x1="0" y1="-0.508" x2="-1.143" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.508" x2="-1.143" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.508" x2="0" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="-0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="-0.254" x2="-1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.397" y1="0.254" x2="-1.143" y2="0.254" width="0.1524" layer="51"/>
<wire x1="-1.143" y1="0.254" x2="-1.143" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0.508" y1="-0.254" x2="1.397" y2="-0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="-0.254" x2="1.397" y2="0.254" width="0.1524" layer="51"/>
<wire x1="1.397" y1="0.254" x2="0.508" y2="0.254" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.381" x2="0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.508" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="-0.381" x2="-0.254" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0.381" x2="0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.508" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0.381" x2="-0.254" y2="0.508" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.254" x2="0.254" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="-0.254" y1="0" x2="0" y2="0.254" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="0.381" y1="-0.381" x2="0.381" y2="0.381" width="0.1524" layer="21" curve="90"/>
<circle x="0" y="0" radius="0.508" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="0.8382" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-1.8542" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="-1.397" y1="-0.254" x2="-1.143" y2="0.254" layer="51"/>
<rectangle x1="0.508" y1="-0.254" x2="1.397" y2="0.254" layer="51"/>
</package>
<package name="LZR181">
<description>&lt;B&gt;LED BLOCK&lt;/B&gt;&lt;p&gt;
1 LED, Siemens</description>
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.889" x2="1.27" y2="-0.889" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.889" x2="-1.27" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0" x2="0" y2="0.889" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-0.508" y1="0" x2="0" y2="0.508" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.508" x2="0.508" y2="0" width="0.1524" layer="21" curve="90" cap="flat"/>
<wire x1="0" y1="-0.889" x2="0.889" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-0.8678" y1="0.7439" x2="0" y2="1.143" width="0.1524" layer="21" curve="-49.396139" cap="flat"/>
<wire x1="0" y1="1.143" x2="0.8678" y2="0.7439" width="0.1524" layer="21" curve="-49.396139" cap="flat"/>
<wire x1="-0.8678" y1="-0.7439" x2="0" y2="-1.143" width="0.1524" layer="21" curve="49.396139" cap="flat"/>
<wire x1="0" y1="-1.143" x2="0.8678" y2="-0.7439" width="0.1524" layer="21" curve="49.396139" cap="flat"/>
<wire x1="0.8678" y1="0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="-40.604135" cap="flat"/>
<wire x1="0.8678" y1="-0.7439" x2="1.143" y2="0" width="0.1524" layer="51" curve="40.604135" cap="flat"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="0.7439" width="0.1524" layer="51" curve="-40.604135" cap="flat"/>
<wire x1="-1.143" y1="0" x2="-0.8678" y2="-0.7439" width="0.1524" layer="51" curve="40.604135" cap="flat"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.1524" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.2954" y="1.4732" size="1.016" layer="25" ratio="14">&gt;NAME</text>
<text x="-1.27" y="-2.4892" size="1.016" layer="27" ratio="14">&gt;VALUE</text>
<rectangle x1="1.27" y1="-0.889" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.27" y2="0.254" layer="51"/>
</package>
<package name="Q62902-B152">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.9718" y1="-1.8542" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="-1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-1.8542" x2="-2.54" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.54" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.8542" x2="-2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.8542" x2="-2.9718" y2="1.8542" width="0.1524" layer="21"/>
<wire x1="-2.9718" y1="0.254" x2="-2.9718" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-1.1486" y1="0.8814" x2="0" y2="1.4478" width="0.1524" layer="21" curve="-52.498642" cap="flat"/>
<wire x1="0" y1="1.4478" x2="1.1476" y2="0.8827" width="0.1524" layer="21" curve="-52.433716" cap="flat"/>
<wire x1="-1.1351" y1="-0.8987" x2="0" y2="-1.4478" width="0.1524" layer="21" curve="51.629985" cap="flat"/>
<wire x1="0" y1="-1.4478" x2="1.1305" y2="-0.9044" width="0.1524" layer="21" curve="51.339172" cap="flat"/>
<wire x1="1.1281" y1="-0.9074" x2="1.4478" y2="0" width="0.1524" layer="51" curve="38.811177" cap="flat"/>
<wire x1="1.1401" y1="0.8923" x2="1.4478" y2="0" width="0.1524" layer="51" curve="-38.048073" cap="flat"/>
<wire x1="-1.4478" y1="0" x2="-1.1305" y2="-0.9044" width="0.1524" layer="51" curve="38.659064" cap="flat"/>
<wire x1="-1.4478" y1="0" x2="-1.1456" y2="0.8853" width="0.1524" layer="51" curve="-37.696376" cap="flat"/>
<wire x1="0" y1="1.7018" x2="1.4674" y2="0.8618" width="0.1524" layer="21" curve="-59.573488" cap="flat"/>
<wire x1="-1.4618" y1="0.8714" x2="0" y2="1.7018" width="0.1524" layer="21" curve="-59.200638" cap="flat"/>
<wire x1="0" y1="-1.7018" x2="1.4571" y2="-0.8793" width="0.1524" layer="21" curve="58.891781" cap="flat"/>
<wire x1="-1.4571" y1="-0.8793" x2="0" y2="-1.7018" width="0.1524" layer="21" curve="58.891781" cap="flat"/>
<wire x1="-1.7018" y1="0" x2="-1.4447" y2="0.8995" width="0.1524" layer="51" curve="-31.907626" cap="flat"/>
<wire x1="-1.7018" y1="0" x2="-1.4502" y2="-0.8905" width="0.1524" layer="51" curve="31.551992" cap="flat"/>
<wire x1="1.4521" y1="0.8874" x2="1.7018" y2="0" width="0.1524" layer="51" curve="-31.429586" cap="flat"/>
<wire x1="1.4459" y1="-0.8975" x2="1.7018" y2="0" width="0.1524" layer="51" curve="31.828757" cap="flat"/>
<wire x1="-2.1082" y1="1.8542" x2="-2.1082" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="0.4826" x2="-2.1082" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="-2.1082" y1="-0.4826" x2="-2.1082" y2="-1.8542" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="21" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.9718" y1="1.8542" x2="2.9718" y2="0.4826" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="0.4826" x2="2.9718" y2="-0.4826" width="0.1524" layer="21"/>
<wire x1="2.9718" y1="-0.4826" x2="2.9718" y2="-1.8542" width="0.1524" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.905" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-3.556" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B153">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="5.5118" y1="-3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="-3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="-3.5052" x2="-5.08" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-4.6482" y1="3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.08" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-3.5052" x2="-5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="3.5052" x2="-5.5118" y2="3.5052" width="0.1524" layer="21"/>
<wire x1="-5.5118" y1="0.254" x2="-5.5118" y2="-0.254" width="0.1524" layer="21" curve="180"/>
<wire x1="-4.6482" y1="3.5052" x2="-4.6482" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="5.5118" y1="3.5052" x2="5.5118" y2="-3.5052" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="21"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-4.191" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="Q62902-B155">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="-1.27" y1="-3.048" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="10.033" y1="3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="2.921" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="2.921" y2="3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-3.048" x2="10.033" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="2.921" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-5.207" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.54" x2="-1.27" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.54" x2="-5.207" y2="-2.54" width="0.1524" layer="21" curve="180"/>
<wire x1="-6.985" y1="0.635" x2="-6.985" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="1.397" x2="-6.096" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="1.905" x2="-5.207" y2="-1.905" width="0.1524" layer="21"/>
<pad name="K" x="7.62" y="1.27" drill="0.8128" shape="long"/>
<pad name="A" x="7.62" y="-1.27" drill="0.8128" shape="long"/>
<text x="3.302" y="-2.794" size="1.016" layer="21" ratio="14">A+</text>
<text x="3.302" y="1.778" size="1.016" layer="21" ratio="14">K-</text>
<text x="11.684" y="-2.794" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="0.635" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.921" y1="1.016" x2="6.731" y2="1.524" layer="21"/>
<rectangle x1="2.921" y1="-1.524" x2="6.731" y2="-1.016" layer="21"/>
<hole x="0" y="0" drill="0.8128"/>
</package>
<package name="Q62902-B156">
<description>&lt;b&gt;LED HOLDER&lt;/b&gt;&lt;p&gt;
Siemens</description>
<wire x1="2.54" y1="-1.905" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0" x2="0" y2="1.143" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.143" x2="1.143" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-1.651" y1="0" x2="0" y2="1.651" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-1.651" x2="1.651" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="-2.159" y1="0" x2="0" y2="2.159" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-2.159" x2="2.159" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="2.54" y1="3.81" x2="3.81" y2="2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-3.81" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-3.81" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-3.302" x2="-2.54" y2="-3.302" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.54" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="3.175" width="0.254" layer="21"/>
<pad name="A" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-3.81" y="4.0894" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.7846" y="-5.3594" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-3.556" y="-3.302" size="1.016" layer="21" ratio="14">+</text>
<text x="2.794" y="-3.302" size="1.016" layer="21" ratio="14">-</text>
</package>
<package name="SFH480">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487" cap="flat"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="SFH482">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
infrared emitting diode, Infineon
TO-18, lead spacing 2.54 mm, cathode marking&lt;p&gt;
Inifineon</description>
<wire x1="-2.159" y1="1.524" x2="-2.794" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-2.794" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.159" x2="-2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="0" y1="1.778" x2="1.5358" y2="0.8959" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="0.8959" x2="0" y2="1.778" width="0.1524" layer="21" curve="-59.743278" cap="flat"/>
<wire x1="-1.5358" y1="-0.8959" x2="0" y2="-1.778" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="0" y1="-1.778" x2="1.5358" y2="-0.8959" width="0.1524" layer="21" curve="59.743278" cap="flat"/>
<wire x1="1.5142" y1="0.9318" x2="1.778" y2="0" width="0.1524" layer="51" curve="-31.606487" cap="flat"/>
<wire x1="1.5" y1="-0.9546" x2="1.778" y2="0" width="0.1524" layer="51" curve="32.472615" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5142" y2="-0.9318" width="0.1524" layer="51" curve="31.606487" cap="flat"/>
<wire x1="-1.778" y1="0" x2="-1.5" y2="0.9546" width="0.1524" layer="51" curve="-32.472615" cap="flat"/>
<wire x1="-0.635" y1="0" x2="0" y2="0.635" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="-1.016" y1="0" x2="0" y2="1.016" width="0.1524" layer="51" curve="-90" cap="flat"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<wire x1="0" y1="-1.016" x2="1.016" y2="0" width="0.1524" layer="51" curve="90" cap="flat"/>
<circle x="0" y="0" radius="2.667" width="0.1524" layer="21"/>
<circle x="0" y="0" radius="2.413" width="0.254" layer="21"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="-1.27" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-4.318" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="U57X32">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
rectangle, 5.7 x 3.2 mm</description>
<wire x1="-3.175" y1="1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="3.175" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.905" x2="-3.175" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="2.667" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.667" y1="-1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="1.397" x2="-2.667" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="2.54" y2="1.016" width="0.1524" layer="51"/>
<wire x1="2.286" y1="1.27" x2="2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.508" x2="2.54" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-0.508" x2="2.54" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-1.016" x2="2.54" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="1.27" x2="-1.778" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="1.27" x2="-0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.27" x2="-0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.254" y1="1.27" x2="0.254" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="0.762" y1="1.27" x2="0.762" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="1.778" y1="1.27" x2="1.778" y2="-1.27" width="0.1524" layer="51"/>
<pad name="A" x="-1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<pad name="K" x="1.27" y="0" drill="0.8128" shape="long" rot="R90"/>
<text x="3.683" y="0.254" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.683" y="-1.524" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="IRL80A">
<description>&lt;B&gt;IR LED&lt;/B&gt;&lt;p&gt;
IR transmitter Siemens</description>
<wire x1="0.889" y1="2.286" x2="0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.778" x2="0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="0.889" y1="0.762" x2="0.889" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-0.635" x2="0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="0.889" y1="-1.778" x2="0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="0.889" y1="-2.286" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="-0.889" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.778" x2="-0.889" y2="0.762" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="0.762" x2="-0.889" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-0.889" y1="-1.778" x2="-0.889" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="2.286" x2="0.889" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="-0.762" x2="-0.889" y2="0.762" width="0.1524" layer="21" curve="-180"/>
<wire x1="-1.397" y1="0.254" x2="-1.397" y2="-0.254" width="0.0508" layer="21"/>
<wire x1="-1.143" y1="0.508" x2="-1.143" y2="-0.508" width="0.0508" layer="21"/>
<pad name="K" x="0" y="1.27" drill="0.8128" shape="octagon"/>
<pad name="A" x="0" y="-1.27" drill="0.8128" shape="octagon"/>
<text x="1.27" y="0.381" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="1.27" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="P-LCC-2">
<description>&lt;b&gt;TOPLED® High-optical Power LED (HOP)&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... ls_t675.pdf</description>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.8" x2="1.1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-1.8" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="C" x="0" y="-2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.75" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="2.54" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-2.25" x2="1.3" y2="-0.75" layer="31"/>
<rectangle x1="-1.3" y1="0.75" x2="1.3" y2="2.25" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.4" y1="0.65" x2="1.4" y2="2.35" layer="29"/>
<rectangle x1="-1.4" y1="-2.35" x2="1.4" y2="-0.65" layer="29"/>
</package>
<package name="OSRAM-MINI-TOP-LED">
<description>&lt;b&gt;BLUE LINETM Hyper Mini TOPLED® Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LB M676.pdf</description>
<wire x1="-0.6" y1="0.9" x2="-0.6" y2="-0.7" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.4" y1="-0.9" x2="0.6" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="-0.9" x2="0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="0.6" y1="0.9" x2="-0.6" y2="0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="0.95" x2="-0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="1.1" x2="0.45" y2="1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="1.1" x2="0.45" y2="0.95" width="0.1016" layer="51"/>
<wire x1="-0.6" y1="-0.7" x2="-0.4" y2="-0.9" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-0.9" x2="-0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.45" y1="-1.1" x2="0.45" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.45" y1="-1.1" x2="0.45" y2="-0.95" width="0.1016" layer="51"/>
<smd name="A" x="0" y="2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-2.6" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="1.905" size="1.27" layer="21">A</text>
<text x="-0.635" y="-3.175" size="1.27" layer="21">C</text>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.5" y1="0.6" x2="0.5" y2="1.4" layer="31"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-0.6" layer="31"/>
<rectangle x1="-0.15" y1="-0.6" x2="0.15" y2="-0.3" layer="51"/>
<rectangle x1="-0.45" y1="0.65" x2="0.45" y2="1.35" layer="31"/>
<rectangle x1="-0.45" y1="-1.35" x2="0.45" y2="-0.65" layer="31"/>
</package>
<package name="OSRAM-SIDELED">
<description>&lt;b&gt;Super SIDELED® High-Current LED&lt;/b&gt;&lt;p&gt;
LG A672, LP A672 &lt;br&gt;
Source: http://www.osram.convergy.de/ ... LG_LP_A672.pdf (2004.05.13)</description>
<wire x1="-1.85" y1="-2.05" x2="-1.85" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="-0.75" x2="-1.7" y2="-0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="-0.75" x2="-1.7" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.7" y1="0.75" x2="-1.85" y2="0.75" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="0.75" x2="-1.85" y2="2.05" width="0.1016" layer="51"/>
<wire x1="-1.85" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="2.05" x2="0.9" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="-1.85" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="0.9" y1="-2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="-2.05" x2="1.85" y2="-1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="-1.85" x2="1.85" y2="1.85" width="0.1016" layer="51"/>
<wire x1="1.85" y1="1.85" x2="1.05" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="0.9" y2="2.05" width="0.1016" layer="51"/>
<wire x1="1.05" y1="2.05" x2="1.05" y2="-2.05" width="0.1016" layer="51"/>
<wire x1="-0.55" y1="-0.9" x2="-0.55" y2="0.9" width="0.1016" layer="51" curve="-167.319617"/>
<wire x1="-0.55" y1="-0.9" x2="0.85" y2="-1.2" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.55" y1="0.9" x2="0.85" y2="1.2" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="-2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="0.635" y="-3.175" size="1.27" layer="21" rot="R90">C</text>
<text x="0.635" y="2.54" size="1.27" layer="21" rot="R90">A</text>
<text x="-2.54" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.1" y1="-2.2" x2="2.1" y2="-0.4" layer="29"/>
<rectangle x1="-2.1" y1="0.4" x2="2.1" y2="2.2" layer="29"/>
<rectangle x1="-1.9" y1="-2.1" x2="1.9" y2="-0.6" layer="31"/>
<rectangle x1="-1.9" y1="0.6" x2="1.9" y2="2.1" layer="31"/>
<rectangle x1="-1.85" y1="-2.05" x2="-0.7" y2="-1" layer="51"/>
</package>
<package name="SMART-LED">
<description>&lt;b&gt;SmartLEDTM Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY L896.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="A" x="0" y="0.725" dx="0.35" dy="0.35" layer="1"/>
<smd name="B" x="0" y="-0.725" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-0.635" size="1.016" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.016" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
</package>
<package name="P-LCC-2-TOPLED-RG">
<description>&lt;b&gt;Hyper TOPLED® RG Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY T776.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1.1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1.1" y1="1.6" x2="-1.1" y2="2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="2.45" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1.1" y1="-1.6" x2="-1.1" y2="-2.45" width="0.1016" layer="51"/>
<wire x1="1.1" y1="-2.45" x2="1.1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="21"/>
<smd name="C" x="0" y="-3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="3.5" dx="4" dy="4" layer="1" stop="no" cream="no"/>
<text x="-2.54" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.81" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-0.635" y="3.29" size="1.27" layer="21">A</text>
<text x="-0.635" y="-4.56" size="1.27" layer="21">C</text>
<rectangle x1="-1.3" y1="-3" x2="1.3" y2="-1.5" layer="31"/>
<rectangle x1="-1.3" y1="1.5" x2="1.3" y2="3" layer="31"/>
<rectangle x1="-0.25" y1="-0.25" x2="0.25" y2="0.25" layer="21"/>
<rectangle x1="-1.15" y1="2.4" x2="1.15" y2="2.7" layer="51"/>
<rectangle x1="-1.15" y1="-2.7" x2="1.15" y2="-2.4" layer="51"/>
<rectangle x1="-1.5" y1="1.5" x2="1.5" y2="3.2" layer="29"/>
<rectangle x1="-1.5" y1="-3.2" x2="1.5" y2="-1.5" layer="29"/>
<hole x="0" y="0" drill="2.8"/>
</package>
<package name="MICRO-SIDELED">
<description>&lt;b&gt;Hyper Micro SIDELED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LS_LY Y876.pdf</description>
<wire x1="0.65" y1="1.1" x2="-0.1" y2="1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="1.1" x2="-0.35" y2="1" width="0.1016" layer="51"/>
<wire x1="-0.35" y1="1" x2="-0.35" y2="-0.9" width="0.1016" layer="21"/>
<wire x1="-0.35" y1="-0.9" x2="-0.1" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="-0.1" y1="-1.1" x2="0.65" y2="-1.1" width="0.1016" layer="51"/>
<wire x1="0.65" y1="-1.1" x2="0.65" y2="1.1" width="0.1016" layer="21"/>
<wire x1="0.6" y1="0.9" x2="0.25" y2="0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="0.7" x2="0.25" y2="-0.7" width="0.0508" layer="21"/>
<wire x1="0.25" y1="-0.7" x2="0.6" y2="-0.9" width="0.0508" layer="21"/>
<smd name="A" x="0" y="1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="C" x="0" y="-1.95" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.4" y1="1.1" x2="0.4" y2="1.8" layer="29"/>
<rectangle x1="-0.4" y1="-1.8" x2="0.4" y2="-1.1" layer="29"/>
<rectangle x1="-0.35" y1="-1.75" x2="0.35" y2="-1.15" layer="31"/>
<rectangle x1="-0.35" y1="1.15" x2="0.35" y2="1.75" layer="31"/>
<rectangle x1="-0.125" y1="1.125" x2="0.125" y2="1.75" layer="51"/>
<rectangle x1="-0.125" y1="-1.75" x2="0.125" y2="-1.125" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-0.65" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<smd name="A" x="-2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@3" x="2" y="3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@4" x="2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="-2" y="-3.15" dx="3.3" dy="4.8" layer="1" stop="no" cream="no"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="21">C</text>
<text x="-1.905" y="2.54" size="1.27" layer="21">A</text>
<text x="1.27" y="2.54" size="1.27" layer="21">C</text>
<text x="1.27" y="-3.81" size="1.27" layer="21">C</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
</package>
<package name="CHIP-LED0603">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB Q993&lt;br&gt;
Source: http://www.osram.convergy.de/ ... Lb_q993.pdf</description>
<wire x1="-0.4" y1="0.45" x2="-0.4" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.45" x2="0.4" y2="-0.45" width="0.1016" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-0.635" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-0.635" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.45" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="0.45" y2="-0.45" layer="51"/>
<rectangle x1="-0.45" y1="0" x2="-0.3" y2="0.3" layer="21"/>
<rectangle x1="0.3" y1="0" x2="0.45" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
</package>
<package name="CHIP-LED0805">
<description>&lt;b&gt;Hyper CHIPLED Hyper-Bright LED&lt;/b&gt;&lt;p&gt;
LB R99A&lt;br&gt;
Source: http://www.osram.convergy.de/ ... lb_r99a.pdf</description>
<wire x1="-0.625" y1="0.45" x2="-0.625" y2="-0.45" width="0.1016" layer="51"/>
<wire x1="0.625" y1="0.45" x2="0.625" y2="-0.475" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.27" y="-2.54" size="1.016" layer="21" font="vector">+</text>
<rectangle x1="-0.675" y1="0" x2="-0.525" y2="0.3" layer="21"/>
<rectangle x1="0.525" y1="0" x2="0.675" y2="0.3" layer="21"/>
<rectangle x1="-0.15" y1="0" x2="0.15" y2="0.3" layer="21"/>
<rectangle x1="-0.675" y1="0.45" x2="0.675" y2="1.05" layer="51"/>
<rectangle x1="-0.675" y1="-1.05" x2="0.675" y2="-0.45" layer="51"/>
</package>
<package name="MINI-TOPLED-SANTANA">
<description>&lt;b&gt;Mini TOPLED Santana®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG M470.pdf</description>
<wire x1="0.7" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.35" y1="-1" x2="-0.45" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-1" x2="-0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-1" x2="-0.7" y2="-0.75" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="-0.75" x2="-0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="-0.7" y1="1" x2="0.7" y2="1" width="0.1016" layer="21"/>
<wire x1="0.7" y1="1" x2="0.7" y2="-0.65" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.7" y2="-1" width="0.1016" layer="21"/>
<wire x1="0.45" y1="-0.7" x2="-0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="-0.7" x2="-0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="-0.45" y1="0.7" x2="0.45" y2="0.7" width="0.1016" layer="21"/>
<wire x1="0.45" y1="0.7" x2="0.45" y2="-0.7" width="0.1016" layer="21"/>
<wire x1="0.7" y1="-0.65" x2="0.35" y2="-1" width="0.1016" layer="21"/>
<smd name="C" x="0" y="-2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="2.2" dx="1.6" dy="1.6" layer="1" stop="no" cream="no"/>
<text x="-1.27" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.55" y1="1.5" x2="0.55" y2="2.1" layer="29"/>
<rectangle x1="-0.55" y1="-2.1" x2="0.55" y2="-1.5" layer="29"/>
<rectangle x1="-0.5" y1="-2.05" x2="0.5" y2="-1.55" layer="31"/>
<rectangle x1="-0.5" y1="1.55" x2="0.5" y2="2.05" layer="31"/>
<rectangle x1="-0.2" y1="-0.4" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.5" y1="-2.1" x2="0.5" y2="-1.4" layer="51"/>
<rectangle x1="-0.5" y1="1.4" x2="0.5" y2="2.05" layer="51"/>
<rectangle x1="-0.5" y1="1" x2="0.5" y2="1.4" layer="21"/>
<rectangle x1="-0.5" y1="-1.4" x2="0.5" y2="-1.05" layer="21"/>
<hole x="0" y="0" drill="2.7"/>
</package>
<package name="CHIPLED_0805">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_R971.pdf</description>
<wire x1="-0.35" y1="1" x2="0.35" y2="1" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.35" y1="-1" x2="0.35" y2="-1" width="0.1016" layer="51" curve="-180" cap="flat"/>
<wire x1="0.575" y1="0.525" x2="0.575" y2="-0.525" width="0.1016" layer="51"/>
<wire x1="-0.575" y1="-0.5" x2="-0.575" y2="0.925" width="0.1016" layer="51"/>
<circle x="-0.45" y="0.85" radius="0.103" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.05" dx="1.2" dy="1.2" layer="1"/>
<smd name="A" x="0" y="-1.05" dx="1.2" dy="1.2" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3" y1="0.5" x2="0.625" y2="1" layer="51"/>
<rectangle x1="-0.325" y1="0.5" x2="-0.175" y2="0.75" layer="51"/>
<rectangle x1="0.175" y1="0.5" x2="0.325" y2="0.75" layer="51"/>
<rectangle x1="-0.2" y1="0.5" x2="0.2" y2="0.675" layer="51"/>
<rectangle x1="0.3" y1="-1" x2="0.625" y2="-0.5" layer="51"/>
<rectangle x1="-0.625" y1="-1" x2="-0.3" y2="-0.5" layer="51"/>
<rectangle x1="0.175" y1="-0.75" x2="0.325" y2="-0.5" layer="51"/>
<rectangle x1="-0.325" y1="-0.75" x2="-0.175" y2="-0.5" layer="51"/>
<rectangle x1="-0.2" y1="-0.675" x2="0.2" y2="-0.5" layer="51"/>
<rectangle x1="-0.1" y1="0" x2="0.1" y2="0.2" layer="21"/>
<rectangle x1="-0.6" y1="0.5" x2="-0.3" y2="0.8" layer="51"/>
<rectangle x1="-0.625" y1="0.925" x2="-0.4" y2="1" layer="51"/>
</package>
<package name="CHIPLED_1206">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY N971.pdf</description>
<wire x1="-0.4" y1="1.625" x2="0.4" y2="1.625" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.8" y1="-0.95" x2="-0.8" y2="0.95" width="0.1016" layer="51"/>
<wire x1="0.8" y1="0.95" x2="0.8" y2="-0.95" width="0.1016" layer="51"/>
<circle x="-0.55" y="1.425" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="0" y="1.75" dx="1.5" dy="1.5" layer="1"/>
<smd name="A" x="0" y="-1.75" dx="1.5" dy="1.5" layer="1"/>
<text x="-1.27" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="2.54" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.85" y1="1.525" x2="-0.35" y2="1.65" layer="51"/>
<rectangle x1="-0.85" y1="1.225" x2="-0.625" y2="1.55" layer="51"/>
<rectangle x1="-0.45" y1="1.225" x2="-0.325" y2="1.45" layer="51"/>
<rectangle x1="-0.65" y1="1.225" x2="-0.225" y2="1.35" layer="51"/>
<rectangle x1="0.35" y1="1.3" x2="0.85" y2="1.65" layer="51"/>
<rectangle x1="0.25" y1="1.225" x2="0.85" y2="1.35" layer="51"/>
<rectangle x1="-0.85" y1="0.95" x2="0.85" y2="1.25" layer="51"/>
<rectangle x1="-0.85" y1="-1.65" x2="0.85" y2="-0.95" layer="51"/>
<rectangle x1="-0.85" y1="0.35" x2="-0.525" y2="0.775" layer="21"/>
<rectangle x1="0.525" y1="0.35" x2="0.85" y2="0.775" layer="21"/>
<rectangle x1="-0.175" y1="0" x2="0.175" y2="0.35" layer="21"/>
</package>
<package name="CHIPLED_0603">
<description>&lt;b&gt;CHIPLED&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LG_LY Q971.pdf</description>
<wire x1="-0.3" y1="0.825" x2="0.3" y2="0.825" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.75" dx="0.8" dy="0.8" layer="1"/>
<smd name="A" x="0" y="-0.75" dx="0.8" dy="0.8" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
</package>
<package name="CHIPLED-0603-TTW">
<description>&lt;b&gt;CHIPLED-0603&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.3" y1="0.825" x2="0.3" y2="0.825" width="0.1016" layer="51" curve="180" cap="flat"/>
<wire x1="-0.275" y1="-0.825" x2="0.275" y2="-0.825" width="0.0508" layer="51" curve="-180" cap="flat"/>
<wire x1="-0.4" y1="0.375" x2="-0.4" y2="-0.35" width="0.1016" layer="51"/>
<wire x1="0.4" y1="0.35" x2="0.4" y2="-0.35" width="0.1016" layer="51"/>
<circle x="-0.35" y="0.625" radius="0.075" width="0.0508" layer="51"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.45" y1="0.7" x2="-0.25" y2="0.85" layer="51"/>
<rectangle x1="-0.275" y1="0.55" x2="-0.225" y2="0.6" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="-0.4" y2="0.725" layer="51"/>
<rectangle x1="0.25" y1="0.55" x2="0.45" y2="0.85" layer="51"/>
<rectangle x1="-0.45" y1="0.35" x2="0.45" y2="0.575" layer="51"/>
<rectangle x1="-0.45" y1="-0.85" x2="-0.25" y2="-0.35" layer="51"/>
<rectangle x1="0.25" y1="-0.85" x2="0.45" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.575" x2="0.275" y2="-0.35" layer="51"/>
<rectangle x1="-0.275" y1="-0.65" x2="-0.175" y2="-0.55" layer="51"/>
<rectangle x1="0.175" y1="-0.65" x2="0.275" y2="-0.55" layer="51"/>
<rectangle x1="-0.125" y1="0" x2="0.125" y2="0.25" layer="21"/>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.4" y1="0.625" x2="0.4" y2="1.125" layer="29"/>
<rectangle x1="-0.4" y1="-1.125" x2="0.4" y2="-0.625" layer="29"/>
<rectangle x1="-0.175" y1="-0.675" x2="0.175" y2="-0.325" layer="29"/>
</package>
<package name="SMARTLED-TTW">
<description>&lt;b&gt;SmartLED TTW&lt;/b&gt;&lt;p&gt;
Recommended Solder Pad useable for SmartLEDTM and Chipled - Package 0603&lt;br&gt;
Package able to withstand TTW-soldering heat&lt;br&gt;
Package suitable for TTW-soldering&lt;br&gt;
Source: http://www.osram.convergy.de/ ... LO_LS_LY L89K.pdf</description>
<wire x1="-0.35" y1="0.6" x2="0.35" y2="0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="0.35" y1="0.6" x2="0.35" y2="-0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.6" x2="-0.35" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<wire x1="-0.35" y1="-0.6" x2="-0.35" y2="0.6" width="0.1016" layer="21" style="shortdash"/>
<wire x1="0.35" y1="-0.4" x2="0.15" y2="-0.6" width="0.1016" layer="51" style="shortdash"/>
<smd name="C" x="0" y="0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A" x="0" y="-0.875" dx="0.8" dy="0.5" layer="1" stop="no" cream="no"/>
<smd name="A@1" x="0" y="-0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<smd name="C@1" x="0" y="0.5" dx="0.35" dy="0.35" layer="1" stop="no" cream="no"/>
<text x="-0.635" y="-1.27" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="1.905" y="-1.27" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="0.325" x2="0.175" y2="0.7" layer="29"/>
<rectangle x1="-0.15" y1="-0.35" x2="0.15" y2="-0.05" layer="21"/>
<rectangle x1="-0.15" y1="0.6" x2="0.15" y2="0.85" layer="51"/>
<rectangle x1="-0.15" y1="-0.85" x2="0.15" y2="-0.6" layer="51"/>
<rectangle x1="-0.225" y1="0.3" x2="0.225" y2="0.975" layer="31"/>
<rectangle x1="-0.175" y1="-0.7" x2="0.175" y2="-0.325" layer="29" rot="R180"/>
<rectangle x1="-0.225" y1="-0.975" x2="0.225" y2="-0.3" layer="31" rot="R180"/>
</package>
<package name="SMLK34">
<wire x1="-2" y1="1" x2="1.7" y2="1" width="0.127" layer="21"/>
<wire x1="1.7" y1="1" x2="2" y2="0.7" width="0.127" layer="21"/>
<wire x1="2" y1="0.7" x2="2" y2="-1" width="0.127" layer="21"/>
<wire x1="2" y1="-1" x2="-2" y2="-1" width="0.127" layer="21"/>
<wire x1="-2" y1="-1" x2="-2" y2="1" width="0.127" layer="21"/>
<wire x1="-1.8" y1="0.8" x2="-1.8" y2="-0.8" width="0.127" layer="25"/>
<wire x1="-1.8" y1="-0.8" x2="1.8" y2="-0.8" width="0.127" layer="25"/>
<wire x1="1.8" y1="-0.8" x2="1.8" y2="0.6" width="0.127" layer="25"/>
<wire x1="1.8" y1="0.6" x2="1.6" y2="0.8" width="0.127" layer="25"/>
<wire x1="1.6" y1="0.8" x2="-1.8" y2="0.8" width="0.127" layer="25"/>
<smd name="A" x="0.9" y="0" dx="3.65" dy="1.74" layer="1" rot="R180"/>
<smd name="K" x="-2.1" y="0" dx="1.35" dy="1.74" layer="1" rot="R180"/>
<text x="-2.8" y="-3.97" size="1.27" layer="25" font="vector">&gt;NAME</text>
<text x="-2.8" y="-5.37" size="1.27" layer="27" font="vector">&gt;VALUE</text>
<text x="1.73" y="-2.178" size="1.016" layer="21" font="vector">A</text>
<text x="-2.938" y="-2.178" size="1.016" layer="21" font="vector">K</text>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;b&gt;LED&lt;/b&gt;&lt;p&gt;
&lt;u&gt;OSRAM&lt;/u&gt;:&lt;br&gt;

- &lt;u&gt;CHIPLED&lt;/u&gt;&lt;br&gt;
LG R971, LG N971, LY N971, LG Q971, LY Q971, LO R971, LY R971
LH N974, LH R974&lt;br&gt;
LS Q976, LO Q976, LY Q976&lt;br&gt;
LO Q996&lt;br&gt;


- &lt;u&gt;Hyper CHIPLED&lt;/u&gt;&lt;br&gt;
LW Q18S&lt;br&gt;
LB Q993, LB Q99A, LB R99A&lt;br&gt;

- &lt;u&gt;SideLED&lt;/u&gt;&lt;br&gt;
LS A670, LO A670, LY A670, LG A670, LP A670&lt;br&gt;
LB A673, LV A673, LT A673, LW A673&lt;br&gt;
LH A674&lt;br&gt;
LY A675&lt;br&gt;
LS A676, LA A676, LO A676, LY A676, LW A676&lt;br&gt;
LS A679, LY A679, LG A679&lt;br&gt;

-  &lt;u&gt;Hyper Micro SIDELED®&lt;/u&gt;&lt;br&gt;
LS Y876, LA Y876, LO Y876, LY Y876&lt;br&gt;
LT Y87S&lt;br&gt;

- &lt;u&gt;SmartLED&lt;/u&gt;&lt;br&gt;
LW L88C, LW L88S&lt;br&gt;
LB L89C, LB L89S, LG L890&lt;br&gt;
LS L89K, LO L89K, LY L89K&lt;br&gt;
LS L896, LA L896, LO L896, LY L896&lt;br&gt;

- &lt;u&gt;TOPLED&lt;/u&gt;&lt;br&gt;
LS T670, LO T670, LY T670, LG T670, LP T670&lt;br&gt;
LSG T670, LSP T670, LSY T670, LOP T670, LYG T670&lt;br&gt;
LG T671, LOG T671, LSG T671&lt;br&gt;
LB T673, LV T673, LT T673, LW T673&lt;br&gt;
LH T674&lt;br&gt;
LS T676, LA T676, LO T676, LY T676, LB T676, LH T676, LSB T676, LW T676&lt;br&gt;
LB T67C, LV T67C, LT T67C, LS T67K, LO T67K, LY T67K, LW E67C&lt;br&gt;
LS E67B, LA E67B, LO E67B, LY E67B, LB E67C, LV E67C, LT E67C&lt;br&gt;
LW T67C&lt;br&gt;
LS T679, LY T679, LG T679&lt;br&gt;
LS T770, LO T770, LY T770, LG T770, LP T770&lt;br&gt;
LB T773, LV T773, LT T773, LW T773&lt;br&gt;
LH T774&lt;br&gt;
LS E675, LA E675, LY E675, LS T675&lt;br&gt;
LS T776, LA T776, LO T776, LY T776, LB T776&lt;br&gt;
LHGB T686&lt;br&gt;
LT T68C, LB T68C&lt;br&gt;

- &lt;u&gt;Hyper Mini TOPLED®&lt;/u&gt;&lt;br&gt;
LB M676&lt;br&gt;

- &lt;u&gt;Mini TOPLED Santana®&lt;/u&gt;&lt;br&gt;
LG M470&lt;br&gt;
LS M47K, LO M47K, LY M47K&lt;br&gt;

&lt;p&gt;
Source: http://www.osram.convergy.de/</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="SMT1206" package="1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LD260" package="LD260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR2X5" package="LED2X5">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5MM" package="LED5MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LSU260" package="LSU260">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LZR181" package="LZR181">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B152" package="Q62902-B152">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B153" package="Q62902-B153">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B155" package="Q62902-B155">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="B156" package="Q62902-B156">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH480" package="SFH480">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SFH482" package="SFH482">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SQR5.7X3.2" package="U57X32">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="IRL80A" package="IRL80A">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2" package="P-LCC-2">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MINI-TOP" package="OSRAM-MINI-TOP-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SIDELED" package="OSRAM-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMART-LED" package="SMART-LED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="B"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-2-BACK" package="P-LCC-2-TOPLED-RG">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MICRO-SIDELED" package="MICRO-SIDELED">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="P-LCC-4" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C@4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0603" package="CHIP-LED0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIP-LED0805" package="CHIP-LED0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="TOPLED-SANTANA" package="MINI-TOPLED-SANTANA">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0805" package="CHIPLED_0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_1206" package="CHIPLED_1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED_0603" package="CHIPLED_0603">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CHIPLED-0603-TTW" package="CHIPLED-0603-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="" package="SMARTLED-TTW">
<connects>
<connect gate="G$1" pin="A" pad="A@1"/>
<connect gate="G$1" pin="C" pad="C@1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SMLK34" package="SMLK34">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-jack">
<description>&lt;b&gt;Jacks&lt;/b&gt;&lt;p&gt;
Power Connectors&lt;br&gt;
Phone Connectors&lt;br&gt;
Cinch Connectors&lt;br&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DCJ0303">
<description>&lt;b&gt;DC POWER JACK&lt;/b&gt; Pad shape changed to LONG 2007.07.26&lt;p&gt;
Source: DCJ0303.pdf</description>
<wire x1="1.65" y1="2.6" x2="-1.65" y2="2.6" width="0" layer="46"/>
<wire x1="-1.65" y1="2.6" x2="-1.65" y2="3.6" width="0" layer="46"/>
<wire x1="-1.65" y1="3.6" x2="1.65" y2="3.6" width="0" layer="46"/>
<wire x1="1.65" y1="3.6" x2="1.65" y2="2.6" width="0" layer="46"/>
<wire x1="5.3" y1="1.4" x2="4.3" y2="1.4" width="0" layer="46"/>
<wire x1="4.3" y1="1.4" x2="4.3" y2="-1.4" width="0" layer="46"/>
<wire x1="4.3" y1="-1.4" x2="5.3" y2="-1.4" width="0" layer="46"/>
<wire x1="5.3" y1="-1.4" x2="5.3" y2="1.4" width="0" layer="46"/>
<wire x1="1.4" y1="-3.5" x2="-1.4" y2="-3.5" width="0" layer="46"/>
<wire x1="-1.4" y1="-3.5" x2="-1.4" y2="-2.5" width="0" layer="46"/>
<wire x1="-1.4" y1="-2.5" x2="1.4" y2="-2.5" width="0" layer="46"/>
<wire x1="1.4" y1="-2.5" x2="1.4" y2="-3.5" width="0" layer="46"/>
<wire x1="-4.3" y1="-10.4" x2="4.3" y2="-10.4" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-10.4" x2="4.3" y2="3.9" width="0.2032" layer="51"/>
<wire x1="4.3" y1="3.9" x2="-4.3" y2="3.9" width="0.2032" layer="51"/>
<wire x1="-4.3" y1="3.9" x2="-4.3" y2="-10.4" width="0.2032" layer="21"/>
<wire x1="-2.7" y1="3.9" x2="-4.3" y2="3.9" width="0.2032" layer="21"/>
<wire x1="4.3" y1="3.9" x2="2.7" y2="3.9" width="0.2032" layer="21"/>
<wire x1="-3" y1="-10.275" x2="-3" y2="-3" width="0.2032" layer="51" style="shortdash"/>
<wire x1="3" y1="-10.3" x2="3" y2="-3" width="0.2032" layer="51" style="shortdash"/>
<wire x1="3" y1="-3" x2="-3" y2="-3" width="0.2032" layer="51" style="shortdash"/>
<wire x1="-0.9" y1="-9" x2="-0.9" y2="-4.5" width="0.2032" layer="51" style="shortdash"/>
<wire x1="0.9" y1="-9" x2="0.9" y2="-4.5" width="0.2032" layer="51" style="shortdash"/>
<wire x1="-0.9" y1="-9" x2="0.9" y2="-9" width="0.2032" layer="51" curve="166.57811"/>
<wire x1="4.3" y1="-10.4" x2="4.3" y2="-2.45" width="0.2032" layer="21"/>
<wire x1="4.3" y1="2.3" x2="4.3" y2="3.9" width="0.2032" layer="21"/>
<pad name="1" x="0" y="3.1" drill="1" diameter="2" shape="long" rot="R180"/>
<pad name="3" x="0" y="-3" drill="1" diameter="2" shape="long" rot="R180"/>
<pad name="2" x="4.8" y="0" drill="1" diameter="2" shape="long" rot="R90"/>
<text x="6.35" y="-10.16" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="DC-JACK-SWITCH">
<wire x1="5.08" y1="2.54" x2="-2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0.762" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0.762" y1="-2.54" x2="0" y2="-1.016" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.016" x2="-0.762" y2="-2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="3.81" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-3.302" y1="-2.54" x2="-1.778" y2="1.27" layer="94"/>
<pin name="1" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="2" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<polygon width="0.1524" layer="94">
<vertex x="2.54" y="-2.54"/>
<vertex x="2.032" y="-1.27"/>
<vertex x="3.048" y="-1.27"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="DCJ0303" prefix="J" uservalue="yes">
<description>&lt;b&gt;DC POWER JACK&lt;/b&gt;&lt;p&gt;
Source: DCJ0303.pdf</description>
<gates>
<gate name="G$1" symbol="DC-JACK-SWITCH" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DCJ0303">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="RASPBERRYPI" library="pinhead" deviceset="PINHD-2X20" device=""/>
<part name="LCD_HOUR_A" library="special" deviceset="7SEG-CA-750" device=""/>
<part name="4543_HOUR_A" library="45xx" deviceset="4543" device="N"/>
<part name="2003_HOUR_A" library="uln-udn" deviceset="ULN2003A" device="N"/>
<part name="SUPPLY1" library="supply2" deviceset="VDD" device="" value="12V"/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="LCD_HOUR_B" library="special" deviceset="7SEG-CA-750" device=""/>
<part name="4543_HOUR_B" library="45xx" deviceset="4543" device="N"/>
<part name="2003_HOUR_B" library="uln-udn" deviceset="ULN2003A" device="N"/>
<part name="LCD_MIN_A" library="special" deviceset="7SEG-CA-750" device=""/>
<part name="4543_MIN_A" library="45xx" deviceset="4543" device="N"/>
<part name="2003_MIN_A" library="uln-udn" deviceset="ULN2003A" device="N"/>
<part name="LCD_MIN_B" library="special" deviceset="7SEG-CA-750" device=""/>
<part name="4543_MIN_B" library="45xx" deviceset="4543" device="N"/>
<part name="2003_MIN_B" library="uln-udn" deviceset="ULN2003A" device="N"/>
<part name="LCD_SEC_A" library="special" deviceset="7SEG-CA-750" device=""/>
<part name="4543_SEC_A" library="45xx" deviceset="4543" device="N"/>
<part name="2003_SEC_A" library="uln-udn" deviceset="ULN2003A" device="N"/>
<part name="LCD_SEC_B" library="special" deviceset="7SEG-CA-750" device=""/>
<part name="4543_SEC_B" library="45xx" deviceset="4543" device="N"/>
<part name="2003_SEC_B" library="uln-udn" deviceset="ULN2003A" device="N"/>
<part name="LCD_DAY_A" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_DAY_A" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_DAY_B" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_DAY_B" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_MONTH_A" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_MONTH_A" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_MONTH_B" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_MONTH_B" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_YEAR_A" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_YEAR_A" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_YEAR_B" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_YEAR_B" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_YEAR_C" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_YEAR_C" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_YEAR_D" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_YEAR_D" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_TEMP_A" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_TEMP_A" library="45xx" deviceset="4543" device="N"/>
<part name="LCD_TEMP_B" library="special" deviceset="7SEG-CA-MOD" device=""/>
<part name="4543_TEMP_B" library="45xx" deviceset="4543" device="N"/>
<part name="IC1" library="40xx" deviceset="4067" device="N"/>
<part name="MON" library="adafruit" deviceset="LED" device="5MM"/>
<part name="TUE" library="adafruit" deviceset="LED" device="5MM"/>
<part name="WED" library="adafruit" deviceset="LED" device="5MM"/>
<part name="THU" library="adafruit" deviceset="LED" device="5MM"/>
<part name="FRI" library="adafruit" deviceset="LED" device="5MM"/>
<part name="SAT" library="adafruit" deviceset="LED" device="5MM"/>
<part name="SUN" library="adafruit" deviceset="LED" device="5MM"/>
<part name="1K-1" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-2" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-3" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-4" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-5" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-1" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-2" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-3" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-4" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-5" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-6" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-7" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-1" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-2" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-3" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-4" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-5" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-6" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-7" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-6" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-7" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-8" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-9" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-10" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-8" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-9" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-10" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-11" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-12" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-13" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-14" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-8" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-9" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-10" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-11" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-12" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-13" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-14" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-11" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-12" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-13" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-14" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-15" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-15" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-16" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-17" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-18" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-19" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-20" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-21" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-15" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-16" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-17" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-18" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-19" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-20" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-21" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-16" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-17" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-18" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-19" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-20" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-22" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-23" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-24" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-25" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-26" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-27" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-28" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-22" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-23" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-24" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-25" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-26" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-27" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-28" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-21" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-22" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-23" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-24" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-25" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-29" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-30" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-31" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-32" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-33" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-34" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-35" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-30" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-31" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-32" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-33" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-34" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-35" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-29" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-26" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-27" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-28" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-29" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-30" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-36" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-37" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-38" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-39" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-40" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-41" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="10K-42" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-36" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-37" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-38" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-39" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-40" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-41" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-42" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-91" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-1" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-31" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-32" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-33" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-34" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-35" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-2" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-3" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-4" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-5" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-6" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-7" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-8" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-36" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-37" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-38" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-39" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-40" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-41" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-9" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-10" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-11" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-12" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-13" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-14" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-42" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-43" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-44" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-45" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-46" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-47" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-15" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-16" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-17" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-18" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-19" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-20" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-21" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-48" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-22" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-49" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-50" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-51" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-52" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-53" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-54" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-23" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-24" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-25" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-26" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-27" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-28" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-29" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-55" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-56" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-57" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-58" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-59" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-30" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-31" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-32" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-33" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-34" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-35" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-60" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-36" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-61" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-62" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-63" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-64" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-65" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-66" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-37" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-38" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-39" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-40" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-41" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-42" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-67" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-68" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-69" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-70" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-71" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-72" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-43" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-44" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-45" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-46" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-47" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-48" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-73" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-74" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-75" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-76" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-77" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-49" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-50" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-51" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-52" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-53" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-54" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-55" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-78" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-56" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-79" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-80" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-81" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-82" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-83" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-57" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-58" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-59" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-60" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-61" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-62" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-84" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-63" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-64" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-65" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-66" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-67" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-68" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-69" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-85" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-86" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-87" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-88" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="1K-89" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-70" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-71" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-72" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-73" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-74" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-75" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-76" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="100-77" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="J1" library="con-jack" deviceset="DCJ0303" device=""/>
<part name="COLON_A" library="adafruit" deviceset="LED" device="5MM"/>
<part name="COLON_B" library="adafruit" deviceset="LED" device="5MM"/>
<part name="COLON_C" library="adafruit" deviceset="LED" device="5MM"/>
<part name="COLON_D" library="adafruit" deviceset="LED" device="5MM"/>
<part name="470-43" library="resistor" deviceset="R-EU_" device="0204/5"/>
<part name="470-44" library="resistor" deviceset="R-EU_" device="0204/5"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="RASPBERRYPI" gate="A" x="826.77" y="97.79"/>
<instance part="LCD_HOUR_A" gate="G$1" x="157.48" y="226.06"/>
<instance part="4543_HOUR_A" gate="A" x="158.75" y="194.31"/>
<instance part="2003_HOUR_A" gate="A" x="194.31" y="191.77"/>
<instance part="SUPPLY1" gate="G$1" x="120.65" y="233.68"/>
<instance part="GND1" gate="1" x="142.24" y="173.99"/>
<instance part="LCD_HOUR_B" gate="G$1" x="278.13" y="224.79"/>
<instance part="4543_HOUR_B" gate="A" x="279.4" y="193.04"/>
<instance part="2003_HOUR_B" gate="A" x="314.96" y="190.5"/>
<instance part="LCD_MIN_A" gate="G$1" x="397.51" y="224.79"/>
<instance part="4543_MIN_A" gate="A" x="398.78" y="193.04"/>
<instance part="2003_MIN_A" gate="A" x="434.34" y="190.5"/>
<instance part="LCD_MIN_B" gate="G$1" x="518.16" y="224.79"/>
<instance part="4543_MIN_B" gate="A" x="519.43" y="193.04"/>
<instance part="2003_MIN_B" gate="A" x="554.99" y="190.5"/>
<instance part="LCD_SEC_A" gate="G$1" x="641.35" y="224.79"/>
<instance part="4543_SEC_A" gate="A" x="642.62" y="193.04"/>
<instance part="2003_SEC_A" gate="A" x="678.18" y="190.5"/>
<instance part="LCD_SEC_B" gate="G$1" x="763.27" y="224.79"/>
<instance part="4543_SEC_B" gate="A" x="764.54" y="193.04"/>
<instance part="2003_SEC_B" gate="A" x="800.1" y="190.5"/>
<instance part="LCD_DAY_A" gate="G$1" x="142.24" y="132.08"/>
<instance part="4543_DAY_A" gate="A" x="140.97" y="96.52"/>
<instance part="LCD_DAY_B" gate="G$1" x="217.17" y="132.08"/>
<instance part="4543_DAY_B" gate="A" x="215.9" y="96.52"/>
<instance part="LCD_MONTH_A" gate="G$1" x="293.37" y="132.08"/>
<instance part="4543_MONTH_A" gate="A" x="292.1" y="96.52"/>
<instance part="LCD_MONTH_B" gate="G$1" x="373.38" y="130.81"/>
<instance part="4543_MONTH_B" gate="A" x="372.11" y="95.25"/>
<instance part="LCD_YEAR_A" gate="G$1" x="453.39" y="130.81"/>
<instance part="4543_YEAR_A" gate="A" x="452.12" y="95.25"/>
<instance part="LCD_YEAR_B" gate="G$1" x="530.86" y="130.81"/>
<instance part="4543_YEAR_B" gate="A" x="529.59" y="95.25"/>
<instance part="LCD_YEAR_C" gate="G$1" x="612.14" y="130.81"/>
<instance part="4543_YEAR_C" gate="A" x="610.87" y="95.25"/>
<instance part="LCD_YEAR_D" gate="G$1" x="689.61" y="130.81"/>
<instance part="4543_YEAR_D" gate="A" x="688.34" y="95.25"/>
<instance part="LCD_TEMP_A" gate="G$1" x="367.03" y="30.48"/>
<instance part="4543_TEMP_A" gate="A" x="365.76" y="-5.08"/>
<instance part="LCD_TEMP_B" gate="G$1" x="445.77" y="30.48"/>
<instance part="4543_TEMP_B" gate="A" x="444.5" y="-5.08"/>
<instance part="IC1" gate="A" x="584.2" y="15.24"/>
<instance part="MON" gate="G$1" x="508" y="5.08"/>
<instance part="TUE" gate="G$1" x="515.62" y="5.08"/>
<instance part="WED" gate="G$1" x="523.24" y="5.08"/>
<instance part="THU" gate="G$1" x="530.86" y="5.08"/>
<instance part="FRI" gate="G$1" x="538.48" y="5.08"/>
<instance part="SAT" gate="G$1" x="546.1" y="5.08"/>
<instance part="SUN" gate="G$1" x="553.72" y="5.08"/>
<instance part="1K-1" gate="G$1" x="137.16" y="201.93" smashed="yes">
<attribute name="NAME" x="134.62" y="200.8886" size="1.778" layer="95"/>
<attribute name="VALUE" x="133.35" y="198.628" size="1.778" layer="96"/>
</instance>
<instance part="1K-2" gate="G$1" x="137.16" y="199.39" smashed="yes">
<attribute name="NAME" x="134.62" y="198.3486" size="1.778" layer="95"/>
<attribute name="VALUE" x="133.35" y="196.088" size="1.778" layer="96"/>
</instance>
<instance part="1K-3" gate="G$1" x="137.16" y="196.85" smashed="yes">
<attribute name="NAME" x="134.62" y="195.8086" size="1.778" layer="95"/>
<attribute name="VALUE" x="133.35" y="193.548" size="1.778" layer="96"/>
</instance>
<instance part="1K-4" gate="G$1" x="137.16" y="194.31" smashed="yes">
<attribute name="NAME" x="134.62" y="193.2686" size="1.778" layer="95"/>
<attribute name="VALUE" x="133.35" y="191.008" size="1.778" layer="96"/>
</instance>
<instance part="1K-5" gate="G$1" x="137.16" y="191.77" smashed="yes">
<attribute name="NAME" x="134.62" y="190.7286" size="1.778" layer="95"/>
<attribute name="VALUE" x="133.35" y="188.468" size="1.778" layer="96"/>
</instance>
<instance part="10K-1" gate="G$1" x="176.53" y="199.39" smashed="yes">
<attribute name="NAME" x="173.99" y="198.3486" size="1.778" layer="95"/>
<attribute name="VALUE" x="172.72" y="196.088" size="1.778" layer="96"/>
</instance>
<instance part="10K-2" gate="G$1" x="176.53" y="196.85" smashed="yes">
<attribute name="NAME" x="173.99" y="195.8086" size="1.778" layer="95"/>
<attribute name="VALUE" x="172.72" y="193.548" size="1.778" layer="96"/>
</instance>
<instance part="10K-3" gate="G$1" x="176.53" y="194.31" smashed="yes">
<attribute name="NAME" x="173.99" y="193.2686" size="1.778" layer="95"/>
<attribute name="VALUE" x="172.72" y="191.008" size="1.778" layer="96"/>
</instance>
<instance part="10K-4" gate="G$1" x="176.53" y="191.77" smashed="yes">
<attribute name="NAME" x="173.99" y="190.7286" size="1.778" layer="95"/>
<attribute name="VALUE" x="172.72" y="188.468" size="1.778" layer="96"/>
</instance>
<instance part="10K-5" gate="G$1" x="176.53" y="189.23" smashed="yes">
<attribute name="NAME" x="173.99" y="188.1886" size="1.778" layer="95"/>
<attribute name="VALUE" x="172.72" y="185.928" size="1.778" layer="96"/>
</instance>
<instance part="10K-6" gate="G$1" x="176.53" y="186.69" smashed="yes">
<attribute name="NAME" x="173.99" y="185.6486" size="1.778" layer="95"/>
<attribute name="VALUE" x="172.72" y="183.388" size="1.778" layer="96"/>
</instance>
<instance part="10K-7" gate="G$1" x="176.53" y="184.15" smashed="yes">
<attribute name="NAME" x="173.99" y="183.1086" size="1.778" layer="95"/>
<attribute name="VALUE" x="172.72" y="180.848" size="1.778" layer="96"/>
</instance>
<instance part="470-1" gate="G$1" x="212.09" y="199.39" smashed="yes">
<attribute name="NAME" x="209.55" y="198.3486" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="196.088" size="1.778" layer="96"/>
</instance>
<instance part="470-2" gate="G$1" x="212.09" y="196.85" smashed="yes">
<attribute name="NAME" x="209.55" y="195.8086" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="193.548" size="1.778" layer="96"/>
</instance>
<instance part="470-3" gate="G$1" x="212.09" y="194.31" smashed="yes">
<attribute name="NAME" x="209.55" y="193.2686" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="191.008" size="1.778" layer="96"/>
</instance>
<instance part="470-4" gate="G$1" x="212.09" y="191.77" smashed="yes">
<attribute name="NAME" x="209.55" y="190.7286" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="188.468" size="1.778" layer="96"/>
</instance>
<instance part="470-5" gate="G$1" x="212.09" y="189.23" smashed="yes">
<attribute name="NAME" x="209.55" y="188.1886" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="185.928" size="1.778" layer="96"/>
</instance>
<instance part="470-6" gate="G$1" x="212.09" y="186.69" smashed="yes">
<attribute name="NAME" x="209.55" y="185.6486" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="183.388" size="1.778" layer="96"/>
</instance>
<instance part="470-7" gate="G$1" x="212.09" y="184.15" smashed="yes">
<attribute name="NAME" x="209.55" y="183.1086" size="1.778" layer="95"/>
<attribute name="VALUE" x="208.28" y="180.848" size="1.778" layer="96"/>
</instance>
<instance part="1K-6" gate="G$1" x="257.81" y="200.66" smashed="yes">
<attribute name="NAME" x="255.27" y="199.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="254" y="197.358" size="1.778" layer="96"/>
</instance>
<instance part="1K-7" gate="G$1" x="257.81" y="198.12" smashed="yes">
<attribute name="NAME" x="255.27" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="254" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="1K-8" gate="G$1" x="257.81" y="195.58" smashed="yes">
<attribute name="NAME" x="255.27" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="254" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="1K-9" gate="G$1" x="257.81" y="193.04" smashed="yes">
<attribute name="NAME" x="255.27" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="254" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="1K-10" gate="G$1" x="257.81" y="190.5" smashed="yes">
<attribute name="NAME" x="255.27" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="254" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-8" gate="G$1" x="297.18" y="198.12" smashed="yes">
<attribute name="NAME" x="294.64" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="293.37" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="10K-9" gate="G$1" x="297.18" y="195.58" smashed="yes">
<attribute name="NAME" x="294.64" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="293.37" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="10K-10" gate="G$1" x="297.18" y="193.04" smashed="yes">
<attribute name="NAME" x="294.64" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="293.37" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="10K-11" gate="G$1" x="297.18" y="190.5" smashed="yes">
<attribute name="NAME" x="294.64" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="293.37" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-12" gate="G$1" x="297.18" y="182.88" smashed="yes">
<attribute name="NAME" x="294.64" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="293.37" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="10K-13" gate="G$1" x="297.18" y="185.42" smashed="yes">
<attribute name="NAME" x="294.64" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="293.37" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="10K-14" gate="G$1" x="297.18" y="187.96" smashed="yes">
<attribute name="NAME" x="294.64" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="293.37" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="470-8" gate="G$1" x="332.74" y="198.12" smashed="yes">
<attribute name="NAME" x="330.2" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="328.93" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="470-9" gate="G$1" x="332.74" y="195.58" smashed="yes">
<attribute name="NAME" x="330.2" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="328.93" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="470-10" gate="G$1" x="332.74" y="193.04" smashed="yes">
<attribute name="NAME" x="330.2" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="328.93" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="470-11" gate="G$1" x="332.74" y="190.5" smashed="yes">
<attribute name="NAME" x="330.2" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="328.93" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="470-12" gate="G$1" x="332.74" y="187.96" smashed="yes">
<attribute name="NAME" x="330.2" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="328.93" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="470-13" gate="G$1" x="332.74" y="185.42" smashed="yes">
<attribute name="NAME" x="330.2" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="328.93" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="470-14" gate="G$1" x="332.74" y="182.88" smashed="yes">
<attribute name="NAME" x="330.2" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="328.93" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="1K-11" gate="G$1" x="377.19" y="200.66" smashed="yes">
<attribute name="NAME" x="374.65" y="199.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="373.38" y="197.358" size="1.778" layer="96"/>
</instance>
<instance part="1K-12" gate="G$1" x="377.19" y="198.12" smashed="yes">
<attribute name="NAME" x="374.65" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="373.38" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="1K-13" gate="G$1" x="377.19" y="195.58" smashed="yes">
<attribute name="NAME" x="374.65" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="373.38" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="1K-14" gate="G$1" x="377.19" y="193.04" smashed="yes">
<attribute name="NAME" x="374.65" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="373.38" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="1K-15" gate="G$1" x="377.19" y="190.5" smashed="yes">
<attribute name="NAME" x="374.65" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="373.38" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-15" gate="G$1" x="416.56" y="198.12" smashed="yes">
<attribute name="NAME" x="414.02" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="412.75" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="10K-16" gate="G$1" x="416.56" y="195.58" smashed="yes">
<attribute name="NAME" x="414.02" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="412.75" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="10K-17" gate="G$1" x="416.56" y="193.04" smashed="yes">
<attribute name="NAME" x="414.02" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="412.75" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="10K-18" gate="G$1" x="416.56" y="190.5" smashed="yes">
<attribute name="NAME" x="414.02" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="412.75" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-19" gate="G$1" x="416.56" y="187.96" smashed="yes">
<attribute name="NAME" x="414.02" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="412.75" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="10K-20" gate="G$1" x="416.56" y="185.42" smashed="yes">
<attribute name="NAME" x="414.02" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="412.75" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="10K-21" gate="G$1" x="416.56" y="182.88" smashed="yes">
<attribute name="NAME" x="414.02" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="412.75" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="470-15" gate="G$1" x="452.12" y="198.12" smashed="yes">
<attribute name="NAME" x="449.58" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="448.31" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="470-16" gate="G$1" x="452.12" y="195.58" smashed="yes">
<attribute name="NAME" x="449.58" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="448.31" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="470-17" gate="G$1" x="452.12" y="193.04" smashed="yes">
<attribute name="NAME" x="449.58" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="448.31" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="470-18" gate="G$1" x="452.12" y="190.5" smashed="yes">
<attribute name="NAME" x="449.58" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="448.31" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="470-19" gate="G$1" x="452.12" y="187.96" smashed="yes">
<attribute name="NAME" x="449.58" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="448.31" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="470-20" gate="G$1" x="452.12" y="185.42" smashed="yes">
<attribute name="NAME" x="449.58" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="448.31" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="470-21" gate="G$1" x="452.12" y="182.88" smashed="yes">
<attribute name="NAME" x="449.58" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="448.31" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="1K-16" gate="G$1" x="497.84" y="200.66" smashed="yes">
<attribute name="NAME" x="495.3" y="199.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="494.03" y="197.358" size="1.778" layer="96"/>
</instance>
<instance part="1K-17" gate="G$1" x="497.84" y="198.12" smashed="yes">
<attribute name="NAME" x="495.3" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="494.03" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="1K-18" gate="G$1" x="497.84" y="195.58" smashed="yes">
<attribute name="NAME" x="495.3" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="494.03" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="1K-19" gate="G$1" x="497.84" y="193.04" smashed="yes">
<attribute name="NAME" x="495.3" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="494.03" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="1K-20" gate="G$1" x="497.84" y="190.5" smashed="yes">
<attribute name="NAME" x="495.3" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="494.03" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-22" gate="G$1" x="537.21" y="198.12" smashed="yes">
<attribute name="NAME" x="534.67" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="533.4" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="10K-23" gate="G$1" x="537.21" y="195.58" smashed="yes">
<attribute name="NAME" x="534.67" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="533.4" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="10K-24" gate="G$1" x="537.21" y="193.04" smashed="yes">
<attribute name="NAME" x="534.67" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="533.4" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="10K-25" gate="G$1" x="537.21" y="190.5" smashed="yes">
<attribute name="NAME" x="534.67" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="533.4" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-26" gate="G$1" x="537.21" y="187.96" smashed="yes">
<attribute name="NAME" x="534.67" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="533.4" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="10K-27" gate="G$1" x="537.21" y="185.42" smashed="yes">
<attribute name="NAME" x="534.67" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="533.4" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="10K-28" gate="G$1" x="537.21" y="182.88" smashed="yes">
<attribute name="NAME" x="534.67" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="533.4" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="470-22" gate="G$1" x="572.77" y="198.12" smashed="yes">
<attribute name="NAME" x="570.23" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="568.96" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="470-23" gate="G$1" x="572.77" y="195.58" smashed="yes">
<attribute name="NAME" x="570.23" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="568.96" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="470-24" gate="G$1" x="572.77" y="193.04" smashed="yes">
<attribute name="NAME" x="570.23" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="568.96" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="470-25" gate="G$1" x="572.77" y="190.5" smashed="yes">
<attribute name="NAME" x="570.23" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="568.96" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="470-26" gate="G$1" x="572.77" y="187.96" smashed="yes">
<attribute name="NAME" x="570.23" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="568.96" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="470-27" gate="G$1" x="572.77" y="185.42" smashed="yes">
<attribute name="NAME" x="570.23" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="568.96" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="470-28" gate="G$1" x="572.77" y="182.88" smashed="yes">
<attribute name="NAME" x="570.23" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="568.96" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="1K-21" gate="G$1" x="621.03" y="200.66" smashed="yes">
<attribute name="NAME" x="618.49" y="199.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="617.22" y="197.358" size="1.778" layer="96"/>
</instance>
<instance part="1K-22" gate="G$1" x="621.03" y="198.12" smashed="yes">
<attribute name="NAME" x="618.49" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="617.22" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="1K-23" gate="G$1" x="621.03" y="195.58" smashed="yes">
<attribute name="NAME" x="618.49" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="617.22" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="1K-24" gate="G$1" x="621.03" y="193.04" smashed="yes">
<attribute name="NAME" x="618.49" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="617.22" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="1K-25" gate="G$1" x="621.03" y="190.5" smashed="yes">
<attribute name="NAME" x="618.49" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="617.22" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-29" gate="G$1" x="660.4" y="198.12" smashed="yes">
<attribute name="NAME" x="657.86" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="656.59" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="10K-30" gate="G$1" x="660.4" y="195.58" smashed="yes">
<attribute name="NAME" x="657.86" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="656.59" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="10K-31" gate="G$1" x="660.4" y="193.04" smashed="yes">
<attribute name="NAME" x="657.86" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="656.59" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="10K-32" gate="G$1" x="660.4" y="190.5" smashed="yes">
<attribute name="NAME" x="657.86" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="656.59" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-33" gate="G$1" x="660.4" y="187.96" smashed="yes">
<attribute name="NAME" x="657.86" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="656.59" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="10K-34" gate="G$1" x="660.4" y="185.42" smashed="yes">
<attribute name="NAME" x="657.86" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="656.59" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="10K-35" gate="G$1" x="660.4" y="182.88" smashed="yes">
<attribute name="NAME" x="657.86" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="656.59" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="470-30" gate="G$1" x="695.96" y="195.58" smashed="yes">
<attribute name="NAME" x="693.42" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="692.15" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="470-31" gate="G$1" x="695.96" y="193.04" smashed="yes">
<attribute name="NAME" x="693.42" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="692.15" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="470-32" gate="G$1" x="695.96" y="190.5" smashed="yes">
<attribute name="NAME" x="693.42" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="692.15" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="470-33" gate="G$1" x="695.96" y="187.96" smashed="yes">
<attribute name="NAME" x="693.42" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="692.15" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="470-34" gate="G$1" x="695.96" y="185.42" smashed="yes">
<attribute name="NAME" x="693.42" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="692.15" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="470-35" gate="G$1" x="695.96" y="182.88" smashed="yes">
<attribute name="NAME" x="693.42" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="692.15" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="470-29" gate="G$1" x="695.96" y="198.12" smashed="yes">
<attribute name="NAME" x="693.42" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="692.15" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="1K-26" gate="G$1" x="742.95" y="200.66" smashed="yes">
<attribute name="NAME" x="740.41" y="199.6186" size="1.778" layer="95"/>
<attribute name="VALUE" x="739.14" y="197.358" size="1.778" layer="96"/>
</instance>
<instance part="1K-27" gate="G$1" x="742.95" y="198.12" smashed="yes">
<attribute name="NAME" x="740.41" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="739.14" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="1K-28" gate="G$1" x="742.95" y="195.58" smashed="yes">
<attribute name="NAME" x="740.41" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="739.14" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="1K-29" gate="G$1" x="742.95" y="193.04" smashed="yes">
<attribute name="NAME" x="740.41" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="739.14" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="1K-30" gate="G$1" x="742.95" y="190.5" smashed="yes">
<attribute name="NAME" x="740.41" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="739.14" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-36" gate="G$1" x="782.32" y="198.12" smashed="yes">
<attribute name="NAME" x="779.78" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="778.51" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="10K-37" gate="G$1" x="782.32" y="195.58" smashed="yes">
<attribute name="NAME" x="779.78" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="778.51" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="10K-38" gate="G$1" x="782.32" y="193.04" smashed="yes">
<attribute name="NAME" x="779.78" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="778.51" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="10K-39" gate="G$1" x="782.32" y="190.5" smashed="yes">
<attribute name="NAME" x="779.78" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="778.51" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="10K-40" gate="G$1" x="782.32" y="187.96" smashed="yes">
<attribute name="NAME" x="779.78" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="778.51" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="10K-41" gate="G$1" x="782.32" y="185.42" smashed="yes">
<attribute name="NAME" x="779.78" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="778.51" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="10K-42" gate="G$1" x="782.32" y="182.88" smashed="yes">
<attribute name="NAME" x="779.78" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="778.51" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="470-36" gate="G$1" x="817.88" y="198.12" smashed="yes">
<attribute name="NAME" x="815.34" y="197.0786" size="1.778" layer="95"/>
<attribute name="VALUE" x="814.07" y="194.818" size="1.778" layer="96"/>
</instance>
<instance part="470-37" gate="G$1" x="817.88" y="195.58" smashed="yes">
<attribute name="NAME" x="815.34" y="194.5386" size="1.778" layer="95"/>
<attribute name="VALUE" x="814.07" y="192.278" size="1.778" layer="96"/>
</instance>
<instance part="470-38" gate="G$1" x="817.88" y="193.04" smashed="yes">
<attribute name="NAME" x="815.34" y="191.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="814.07" y="189.738" size="1.778" layer="96"/>
</instance>
<instance part="470-39" gate="G$1" x="817.88" y="190.5" smashed="yes">
<attribute name="NAME" x="815.34" y="189.4586" size="1.778" layer="95"/>
<attribute name="VALUE" x="814.07" y="187.198" size="1.778" layer="96"/>
</instance>
<instance part="470-40" gate="G$1" x="817.88" y="187.96" smashed="yes">
<attribute name="NAME" x="815.34" y="186.9186" size="1.778" layer="95"/>
<attribute name="VALUE" x="814.07" y="184.658" size="1.778" layer="96"/>
</instance>
<instance part="470-41" gate="G$1" x="817.88" y="185.42" smashed="yes">
<attribute name="NAME" x="815.34" y="184.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="814.07" y="182.118" size="1.778" layer="96"/>
</instance>
<instance part="470-42" gate="G$1" x="817.88" y="182.88" smashed="yes">
<attribute name="NAME" x="815.34" y="181.8386" size="1.778" layer="95"/>
<attribute name="VALUE" x="814.07" y="179.578" size="1.778" layer="96"/>
</instance>
<instance part="1K-91" gate="G$1" x="123.19" y="104.14" smashed="yes">
<attribute name="NAME" x="120.65" y="103.0986" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="100.838" size="1.778" layer="96"/>
</instance>
<instance part="100-1" gate="G$1" x="158.75" y="101.6" smashed="yes">
<attribute name="NAME" x="156.21" y="100.5586" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="98.298" size="1.778" layer="96"/>
</instance>
<instance part="1K-31" gate="G$1" x="123.19" y="101.6" smashed="yes">
<attribute name="NAME" x="120.65" y="100.5586" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="98.298" size="1.778" layer="96"/>
</instance>
<instance part="1K-32" gate="G$1" x="123.19" y="99.06" smashed="yes">
<attribute name="NAME" x="120.65" y="98.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="95.758" size="1.778" layer="96"/>
</instance>
<instance part="1K-33" gate="G$1" x="123.19" y="96.52" smashed="yes">
<attribute name="NAME" x="120.65" y="95.4786" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="1K-34" gate="G$1" x="123.19" y="93.98" smashed="yes">
<attribute name="NAME" x="120.65" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="90.678" size="1.778" layer="96"/>
</instance>
<instance part="1K-35" gate="G$1" x="123.19" y="91.44" smashed="yes">
<attribute name="NAME" x="120.65" y="90.3986" size="1.778" layer="95"/>
<attribute name="VALUE" x="119.38" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="100-2" gate="G$1" x="158.75" y="99.06" smashed="yes">
<attribute name="NAME" x="156.21" y="98.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="95.758" size="1.778" layer="96"/>
</instance>
<instance part="100-3" gate="G$1" x="158.75" y="96.52" smashed="yes">
<attribute name="NAME" x="156.21" y="95.4786" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="100-4" gate="G$1" x="158.75" y="93.98" smashed="yes">
<attribute name="NAME" x="156.21" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="90.678" size="1.778" layer="96"/>
</instance>
<instance part="100-5" gate="G$1" x="158.75" y="91.44" smashed="yes">
<attribute name="NAME" x="156.21" y="90.3986" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="100-6" gate="G$1" x="158.75" y="88.9" smashed="yes">
<attribute name="NAME" x="156.21" y="87.8586" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="85.598" size="1.778" layer="96"/>
</instance>
<instance part="100-7" gate="G$1" x="158.75" y="86.36" smashed="yes">
<attribute name="NAME" x="156.21" y="85.3186" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="83.058" size="1.778" layer="96"/>
</instance>
<instance part="100-8" gate="G$1" x="233.68" y="101.6" smashed="yes">
<attribute name="NAME" x="231.14" y="100.5586" size="1.778" layer="95"/>
<attribute name="VALUE" x="229.87" y="98.298" size="1.778" layer="96"/>
</instance>
<instance part="1K-36" gate="G$1" x="198.12" y="104.14" smashed="yes">
<attribute name="NAME" x="195.58" y="103.0986" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.31" y="100.838" size="1.778" layer="96"/>
</instance>
<instance part="1K-37" gate="G$1" x="198.12" y="101.6" smashed="yes">
<attribute name="NAME" x="195.58" y="100.5586" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.31" y="98.298" size="1.778" layer="96"/>
</instance>
<instance part="1K-38" gate="G$1" x="198.12" y="99.06" smashed="yes">
<attribute name="NAME" x="195.58" y="98.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.31" y="95.758" size="1.778" layer="96"/>
</instance>
<instance part="1K-39" gate="G$1" x="198.12" y="96.52" smashed="yes">
<attribute name="NAME" x="195.58" y="95.4786" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.31" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="1K-40" gate="G$1" x="198.12" y="93.98" smashed="yes">
<attribute name="NAME" x="195.58" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.31" y="90.678" size="1.778" layer="96"/>
</instance>
<instance part="1K-41" gate="G$1" x="198.12" y="91.44" smashed="yes">
<attribute name="NAME" x="195.58" y="90.3986" size="1.778" layer="95"/>
<attribute name="VALUE" x="194.31" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="100-9" gate="G$1" x="233.68" y="99.06" smashed="yes">
<attribute name="NAME" x="231.14" y="98.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="229.87" y="95.758" size="1.778" layer="96"/>
</instance>
<instance part="100-10" gate="G$1" x="233.68" y="96.52" smashed="yes">
<attribute name="NAME" x="231.14" y="95.4786" size="1.778" layer="95"/>
<attribute name="VALUE" x="229.87" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="100-11" gate="G$1" x="233.68" y="93.98" smashed="yes">
<attribute name="NAME" x="231.14" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="229.87" y="90.678" size="1.778" layer="96"/>
</instance>
<instance part="100-12" gate="G$1" x="233.68" y="91.44" smashed="yes">
<attribute name="NAME" x="231.14" y="90.3986" size="1.778" layer="95"/>
<attribute name="VALUE" x="229.87" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="100-13" gate="G$1" x="233.68" y="88.9" smashed="yes">
<attribute name="NAME" x="231.14" y="87.8586" size="1.778" layer="95"/>
<attribute name="VALUE" x="229.87" y="85.598" size="1.778" layer="96"/>
</instance>
<instance part="100-14" gate="G$1" x="233.68" y="86.36" smashed="yes">
<attribute name="NAME" x="231.14" y="85.3186" size="1.778" layer="95"/>
<attribute name="VALUE" x="229.87" y="83.058" size="1.778" layer="96"/>
</instance>
<instance part="1K-42" gate="G$1" x="274.32" y="104.14" smashed="yes">
<attribute name="NAME" x="271.78" y="103.0986" size="1.778" layer="95"/>
<attribute name="VALUE" x="270.51" y="100.838" size="1.778" layer="96"/>
</instance>
<instance part="1K-43" gate="G$1" x="274.32" y="101.6" smashed="yes">
<attribute name="NAME" x="271.78" y="100.5586" size="1.778" layer="95"/>
<attribute name="VALUE" x="270.51" y="98.298" size="1.778" layer="96"/>
</instance>
<instance part="1K-44" gate="G$1" x="274.32" y="99.06" smashed="yes">
<attribute name="NAME" x="271.78" y="98.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="270.51" y="95.758" size="1.778" layer="96"/>
</instance>
<instance part="1K-45" gate="G$1" x="274.32" y="96.52" smashed="yes">
<attribute name="NAME" x="271.78" y="95.4786" size="1.778" layer="95"/>
<attribute name="VALUE" x="270.51" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="1K-46" gate="G$1" x="274.32" y="93.98" smashed="yes">
<attribute name="NAME" x="271.78" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="270.51" y="90.678" size="1.778" layer="96"/>
</instance>
<instance part="1K-47" gate="G$1" x="274.32" y="91.44" smashed="yes">
<attribute name="NAME" x="271.78" y="90.3986" size="1.778" layer="95"/>
<attribute name="VALUE" x="270.51" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="100-15" gate="G$1" x="309.88" y="101.6" smashed="yes">
<attribute name="NAME" x="307.34" y="100.5586" size="1.778" layer="95"/>
<attribute name="VALUE" x="306.07" y="98.298" size="1.778" layer="96"/>
</instance>
<instance part="100-16" gate="G$1" x="309.88" y="99.06" smashed="yes">
<attribute name="NAME" x="307.34" y="98.0186" size="1.778" layer="95"/>
<attribute name="VALUE" x="306.07" y="95.758" size="1.778" layer="96"/>
</instance>
<instance part="100-17" gate="G$1" x="309.88" y="96.52" smashed="yes">
<attribute name="NAME" x="307.34" y="95.4786" size="1.778" layer="95"/>
<attribute name="VALUE" x="306.07" y="93.218" size="1.778" layer="96"/>
</instance>
<instance part="100-18" gate="G$1" x="309.88" y="93.98" smashed="yes">
<attribute name="NAME" x="307.34" y="92.9386" size="1.778" layer="95"/>
<attribute name="VALUE" x="306.07" y="90.678" size="1.778" layer="96"/>
</instance>
<instance part="100-19" gate="G$1" x="309.88" y="91.44" smashed="yes">
<attribute name="NAME" x="307.34" y="90.3986" size="1.778" layer="95"/>
<attribute name="VALUE" x="306.07" y="88.138" size="1.778" layer="96"/>
</instance>
<instance part="100-20" gate="G$1" x="309.88" y="88.9" smashed="yes">
<attribute name="NAME" x="307.34" y="87.8586" size="1.778" layer="95"/>
<attribute name="VALUE" x="306.07" y="85.598" size="1.778" layer="96"/>
</instance>
<instance part="100-21" gate="G$1" x="309.88" y="86.36" smashed="yes">
<attribute name="NAME" x="307.34" y="85.3186" size="1.778" layer="95"/>
<attribute name="VALUE" x="306.07" y="83.058" size="1.778" layer="96"/>
</instance>
<instance part="1K-48" gate="G$1" x="354.33" y="102.87" smashed="yes">
<attribute name="NAME" x="351.79" y="101.8286" size="1.778" layer="95"/>
<attribute name="VALUE" x="350.52" y="99.568" size="1.778" layer="96"/>
</instance>
<instance part="100-22" gate="G$1" x="389.89" y="100.33" smashed="yes">
<attribute name="NAME" x="387.35" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="386.08" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-49" gate="G$1" x="354.33" y="100.33" smashed="yes">
<attribute name="NAME" x="351.79" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="350.52" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-50" gate="G$1" x="354.33" y="97.79" smashed="yes">
<attribute name="NAME" x="351.79" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="350.52" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="1K-51" gate="G$1" x="354.33" y="95.25" smashed="yes">
<attribute name="NAME" x="351.79" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="350.52" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="1K-52" gate="G$1" x="354.33" y="92.71" smashed="yes">
<attribute name="NAME" x="351.79" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="350.52" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="1K-53" gate="G$1" x="354.33" y="90.17" smashed="yes">
<attribute name="NAME" x="351.79" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="350.52" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="1K-54" gate="G$1" x="434.34" y="102.87" smashed="yes">
<attribute name="NAME" x="431.8" y="101.8286" size="1.778" layer="95"/>
<attribute name="VALUE" x="430.53" y="99.568" size="1.778" layer="96"/>
</instance>
<instance part="100-23" gate="G$1" x="389.89" y="97.79" smashed="yes">
<attribute name="NAME" x="387.35" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="386.08" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="100-24" gate="G$1" x="389.89" y="95.25" smashed="yes">
<attribute name="NAME" x="387.35" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="386.08" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="100-25" gate="G$1" x="389.89" y="92.71" smashed="yes">
<attribute name="NAME" x="387.35" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="386.08" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="100-26" gate="G$1" x="389.89" y="90.17" smashed="yes">
<attribute name="NAME" x="387.35" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="386.08" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="100-27" gate="G$1" x="389.89" y="87.63" smashed="yes">
<attribute name="NAME" x="387.35" y="86.5886" size="1.778" layer="95"/>
<attribute name="VALUE" x="386.08" y="84.328" size="1.778" layer="96"/>
</instance>
<instance part="100-28" gate="G$1" x="389.89" y="85.09" smashed="yes">
<attribute name="NAME" x="387.35" y="84.0486" size="1.778" layer="95"/>
<attribute name="VALUE" x="386.08" y="81.788" size="1.778" layer="96"/>
</instance>
<instance part="100-29" gate="G$1" x="469.9" y="100.33" smashed="yes">
<attribute name="NAME" x="467.36" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="466.09" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-55" gate="G$1" x="434.34" y="100.33" smashed="yes">
<attribute name="NAME" x="431.8" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="430.53" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-56" gate="G$1" x="434.34" y="97.79" smashed="yes">
<attribute name="NAME" x="431.8" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="430.53" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="1K-57" gate="G$1" x="434.34" y="95.25" smashed="yes">
<attribute name="NAME" x="431.8" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="430.53" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="1K-58" gate="G$1" x="434.34" y="92.71" smashed="yes">
<attribute name="NAME" x="431.8" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="430.53" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="1K-59" gate="G$1" x="434.34" y="90.17" smashed="yes">
<attribute name="NAME" x="431.8" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="430.53" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="100-30" gate="G$1" x="469.9" y="97.79" smashed="yes">
<attribute name="NAME" x="467.36" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="466.09" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="100-31" gate="G$1" x="469.9" y="95.25" smashed="yes">
<attribute name="NAME" x="467.36" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="466.09" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="100-32" gate="G$1" x="469.9" y="92.71" smashed="yes">
<attribute name="NAME" x="467.36" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="466.09" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="100-33" gate="G$1" x="469.9" y="90.17" smashed="yes">
<attribute name="NAME" x="467.36" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="466.09" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="100-34" gate="G$1" x="469.9" y="87.63" smashed="yes">
<attribute name="NAME" x="467.36" y="86.5886" size="1.778" layer="95"/>
<attribute name="VALUE" x="466.09" y="84.328" size="1.778" layer="96"/>
</instance>
<instance part="100-35" gate="G$1" x="469.9" y="85.09" smashed="yes">
<attribute name="NAME" x="467.36" y="84.0486" size="1.778" layer="95"/>
<attribute name="VALUE" x="466.09" y="81.788" size="1.778" layer="96"/>
</instance>
<instance part="1K-60" gate="G$1" x="511.81" y="102.87" smashed="yes">
<attribute name="NAME" x="509.27" y="101.8286" size="1.778" layer="95"/>
<attribute name="VALUE" x="508" y="99.568" size="1.778" layer="96"/>
</instance>
<instance part="100-36" gate="G$1" x="547.37" y="100.33" smashed="yes">
<attribute name="NAME" x="544.83" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="543.56" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-61" gate="G$1" x="511.81" y="100.33" smashed="yes">
<attribute name="NAME" x="509.27" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="508" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-62" gate="G$1" x="511.81" y="97.79" smashed="yes">
<attribute name="NAME" x="509.27" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="508" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="1K-63" gate="G$1" x="511.81" y="95.25" smashed="yes">
<attribute name="NAME" x="509.27" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="508" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="1K-64" gate="G$1" x="511.81" y="92.71" smashed="yes">
<attribute name="NAME" x="509.27" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="508" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="1K-65" gate="G$1" x="511.81" y="90.17" smashed="yes">
<attribute name="NAME" x="509.27" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="508" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="1K-66" gate="G$1" x="593.09" y="102.87" smashed="yes">
<attribute name="NAME" x="590.55" y="101.8286" size="1.778" layer="95"/>
<attribute name="VALUE" x="589.28" y="99.568" size="1.778" layer="96"/>
</instance>
<instance part="100-37" gate="G$1" x="547.37" y="97.79" smashed="yes">
<attribute name="NAME" x="544.83" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="543.56" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="100-38" gate="G$1" x="547.37" y="95.25" smashed="yes">
<attribute name="NAME" x="544.83" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="543.56" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="100-39" gate="G$1" x="547.37" y="92.71" smashed="yes">
<attribute name="NAME" x="544.83" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="543.56" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="100-40" gate="G$1" x="547.37" y="90.17" smashed="yes">
<attribute name="NAME" x="544.83" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="543.56" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="100-41" gate="G$1" x="547.37" y="87.63" smashed="yes">
<attribute name="NAME" x="544.83" y="86.5886" size="1.778" layer="95"/>
<attribute name="VALUE" x="543.56" y="84.328" size="1.778" layer="96"/>
</instance>
<instance part="100-42" gate="G$1" x="628.65" y="100.33" smashed="yes">
<attribute name="NAME" x="626.11" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="624.84" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-67" gate="G$1" x="593.09" y="100.33" smashed="yes">
<attribute name="NAME" x="590.55" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="589.28" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-68" gate="G$1" x="593.09" y="97.79" smashed="yes">
<attribute name="NAME" x="590.55" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="589.28" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="1K-69" gate="G$1" x="593.09" y="95.25" smashed="yes">
<attribute name="NAME" x="590.55" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="589.28" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="1K-70" gate="G$1" x="593.09" y="92.71" smashed="yes">
<attribute name="NAME" x="590.55" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="589.28" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="1K-71" gate="G$1" x="593.09" y="90.17" smashed="yes">
<attribute name="NAME" x="590.55" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="589.28" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="1K-72" gate="G$1" x="670.56" y="102.87" smashed="yes">
<attribute name="NAME" x="668.02" y="101.8286" size="1.778" layer="95"/>
<attribute name="VALUE" x="666.75" y="99.568" size="1.778" layer="96"/>
</instance>
<instance part="100-43" gate="G$1" x="628.65" y="97.79" smashed="yes">
<attribute name="NAME" x="626.11" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="624.84" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="100-44" gate="G$1" x="628.65" y="95.25" smashed="yes">
<attribute name="NAME" x="626.11" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="624.84" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="100-45" gate="G$1" x="628.65" y="92.71" smashed="yes">
<attribute name="NAME" x="626.11" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="624.84" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="100-46" gate="G$1" x="628.65" y="90.17" smashed="yes">
<attribute name="NAME" x="626.11" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="624.84" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="100-47" gate="G$1" x="628.65" y="87.63" smashed="yes">
<attribute name="NAME" x="626.11" y="86.5886" size="1.778" layer="95"/>
<attribute name="VALUE" x="624.84" y="84.328" size="1.778" layer="96"/>
</instance>
<instance part="100-48" gate="G$1" x="628.65" y="85.09" smashed="yes">
<attribute name="NAME" x="626.11" y="84.0486" size="1.778" layer="95"/>
<attribute name="VALUE" x="624.84" y="81.788" size="1.778" layer="96"/>
</instance>
<instance part="1K-73" gate="G$1" x="670.56" y="100.33" smashed="yes">
<attribute name="NAME" x="668.02" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="666.75" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="1K-74" gate="G$1" x="670.56" y="97.79" smashed="yes">
<attribute name="NAME" x="668.02" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="666.75" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="1K-75" gate="G$1" x="670.56" y="95.25" smashed="yes">
<attribute name="NAME" x="668.02" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="666.75" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="1K-76" gate="G$1" x="670.56" y="92.71" smashed="yes">
<attribute name="NAME" x="668.02" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="666.75" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="1K-77" gate="G$1" x="670.56" y="90.17" smashed="yes">
<attribute name="NAME" x="668.02" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="666.75" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="100-49" gate="G$1" x="706.12" y="100.33" smashed="yes">
<attribute name="NAME" x="703.58" y="99.2886" size="1.778" layer="95"/>
<attribute name="VALUE" x="702.31" y="97.028" size="1.778" layer="96"/>
</instance>
<instance part="100-50" gate="G$1" x="706.12" y="97.79" smashed="yes">
<attribute name="NAME" x="703.58" y="96.7486" size="1.778" layer="95"/>
<attribute name="VALUE" x="702.31" y="94.488" size="1.778" layer="96"/>
</instance>
<instance part="100-51" gate="G$1" x="706.12" y="95.25" smashed="yes">
<attribute name="NAME" x="703.58" y="94.2086" size="1.778" layer="95"/>
<attribute name="VALUE" x="702.31" y="91.948" size="1.778" layer="96"/>
</instance>
<instance part="100-52" gate="G$1" x="706.12" y="92.71" smashed="yes">
<attribute name="NAME" x="703.58" y="91.6686" size="1.778" layer="95"/>
<attribute name="VALUE" x="702.31" y="89.408" size="1.778" layer="96"/>
</instance>
<instance part="100-53" gate="G$1" x="706.12" y="90.17" smashed="yes">
<attribute name="NAME" x="703.58" y="89.1286" size="1.778" layer="95"/>
<attribute name="VALUE" x="702.31" y="86.868" size="1.778" layer="96"/>
</instance>
<instance part="100-54" gate="G$1" x="706.12" y="87.63" smashed="yes">
<attribute name="NAME" x="703.58" y="86.5886" size="1.778" layer="95"/>
<attribute name="VALUE" x="702.31" y="84.328" size="1.778" layer="96"/>
</instance>
<instance part="100-55" gate="G$1" x="706.12" y="85.09" smashed="yes">
<attribute name="NAME" x="703.58" y="84.0486" size="1.778" layer="95"/>
<attribute name="VALUE" x="702.31" y="81.788" size="1.778" layer="96"/>
</instance>
<instance part="1K-78" gate="G$1" x="347.98" y="2.54" smashed="yes">
<attribute name="NAME" x="345.44" y="1.4986" size="1.778" layer="95"/>
<attribute name="VALUE" x="344.17" y="-0.762" size="1.778" layer="96"/>
</instance>
<instance part="100-56" gate="G$1" x="383.54" y="0" smashed="yes">
<attribute name="NAME" x="381" y="-1.0414" size="1.778" layer="95"/>
<attribute name="VALUE" x="379.73" y="-3.302" size="1.778" layer="96"/>
</instance>
<instance part="1K-79" gate="G$1" x="347.98" y="0" smashed="yes">
<attribute name="NAME" x="345.44" y="-1.0414" size="1.778" layer="95"/>
<attribute name="VALUE" x="344.17" y="-3.302" size="1.778" layer="96"/>
</instance>
<instance part="1K-80" gate="G$1" x="347.98" y="-2.54" smashed="yes">
<attribute name="NAME" x="345.44" y="-3.5814" size="1.778" layer="95"/>
<attribute name="VALUE" x="344.17" y="-5.842" size="1.778" layer="96"/>
</instance>
<instance part="1K-81" gate="G$1" x="347.98" y="-5.08" smashed="yes">
<attribute name="NAME" x="345.44" y="-6.1214" size="1.778" layer="95"/>
<attribute name="VALUE" x="344.17" y="-8.382" size="1.778" layer="96"/>
</instance>
<instance part="1K-82" gate="G$1" x="347.98" y="-7.62" smashed="yes">
<attribute name="NAME" x="345.44" y="-8.6614" size="1.778" layer="95"/>
<attribute name="VALUE" x="344.17" y="-10.922" size="1.778" layer="96"/>
</instance>
<instance part="1K-83" gate="G$1" x="347.98" y="-10.16" smashed="yes">
<attribute name="NAME" x="345.44" y="-11.2014" size="1.778" layer="95"/>
<attribute name="VALUE" x="344.17" y="-13.462" size="1.778" layer="96"/>
</instance>
<instance part="100-57" gate="G$1" x="383.54" y="-2.54" smashed="yes">
<attribute name="NAME" x="381" y="-3.5814" size="1.778" layer="95"/>
<attribute name="VALUE" x="379.73" y="-5.842" size="1.778" layer="96"/>
</instance>
<instance part="100-58" gate="G$1" x="383.54" y="-5.08" smashed="yes">
<attribute name="NAME" x="381" y="-6.1214" size="1.778" layer="95"/>
<attribute name="VALUE" x="379.73" y="-8.382" size="1.778" layer="96"/>
</instance>
<instance part="100-59" gate="G$1" x="383.54" y="-7.62" smashed="yes">
<attribute name="NAME" x="381" y="-8.6614" size="1.778" layer="95"/>
<attribute name="VALUE" x="379.73" y="-10.922" size="1.778" layer="96"/>
</instance>
<instance part="100-60" gate="G$1" x="383.54" y="-10.16" smashed="yes">
<attribute name="NAME" x="381" y="-11.2014" size="1.778" layer="95"/>
<attribute name="VALUE" x="379.73" y="-13.462" size="1.778" layer="96"/>
</instance>
<instance part="100-61" gate="G$1" x="383.54" y="-12.7" smashed="yes">
<attribute name="NAME" x="381" y="-13.7414" size="1.778" layer="95"/>
<attribute name="VALUE" x="379.73" y="-16.002" size="1.778" layer="96"/>
</instance>
<instance part="100-62" gate="G$1" x="383.54" y="-15.24" smashed="yes">
<attribute name="NAME" x="381" y="-16.2814" size="1.778" layer="95"/>
<attribute name="VALUE" x="379.73" y="-18.542" size="1.778" layer="96"/>
</instance>
<instance part="1K-84" gate="G$1" x="426.72" y="2.54" smashed="yes">
<attribute name="NAME" x="424.18" y="1.4986" size="1.778" layer="95"/>
<attribute name="VALUE" x="422.91" y="-0.762" size="1.778" layer="96"/>
</instance>
<instance part="100-63" gate="G$1" x="462.28" y="0" smashed="yes">
<attribute name="NAME" x="459.74" y="-1.0414" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="-3.302" size="1.778" layer="96"/>
</instance>
<instance part="100-64" gate="G$1" x="462.28" y="-2.54" smashed="yes">
<attribute name="NAME" x="459.74" y="-3.5814" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="-5.842" size="1.778" layer="96"/>
</instance>
<instance part="100-65" gate="G$1" x="462.28" y="-5.08" smashed="yes">
<attribute name="NAME" x="459.74" y="-6.1214" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="-8.382" size="1.778" layer="96"/>
</instance>
<instance part="100-66" gate="G$1" x="462.28" y="-7.62" smashed="yes">
<attribute name="NAME" x="459.74" y="-8.6614" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="-10.922" size="1.778" layer="96"/>
</instance>
<instance part="100-67" gate="G$1" x="462.28" y="-10.16" smashed="yes">
<attribute name="NAME" x="459.74" y="-11.2014" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="-13.462" size="1.778" layer="96"/>
</instance>
<instance part="100-68" gate="G$1" x="462.28" y="-12.7" smashed="yes">
<attribute name="NAME" x="459.74" y="-13.7414" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="-16.002" size="1.778" layer="96"/>
</instance>
<instance part="100-69" gate="G$1" x="462.28" y="-15.24" smashed="yes">
<attribute name="NAME" x="459.74" y="-16.2814" size="1.778" layer="95"/>
<attribute name="VALUE" x="458.47" y="-18.542" size="1.778" layer="96"/>
</instance>
<instance part="1K-85" gate="G$1" x="426.72" y="0" smashed="yes">
<attribute name="NAME" x="424.18" y="-1.0414" size="1.778" layer="95"/>
<attribute name="VALUE" x="422.91" y="-3.302" size="1.778" layer="96"/>
</instance>
<instance part="1K-86" gate="G$1" x="426.72" y="-2.54" smashed="yes">
<attribute name="NAME" x="424.18" y="-3.5814" size="1.778" layer="95"/>
<attribute name="VALUE" x="422.91" y="-5.842" size="1.778" layer="96"/>
</instance>
<instance part="1K-87" gate="G$1" x="426.72" y="-5.08" smashed="yes">
<attribute name="NAME" x="424.18" y="-6.1214" size="1.778" layer="95"/>
<attribute name="VALUE" x="422.91" y="-8.382" size="1.778" layer="96"/>
</instance>
<instance part="1K-88" gate="G$1" x="426.72" y="-7.62" smashed="yes">
<attribute name="NAME" x="424.18" y="-8.6614" size="1.778" layer="95"/>
<attribute name="VALUE" x="422.91" y="-10.922" size="1.778" layer="96"/>
</instance>
<instance part="1K-89" gate="G$1" x="426.72" y="-10.16" smashed="yes">
<attribute name="NAME" x="424.18" y="-11.2014" size="1.778" layer="95"/>
<attribute name="VALUE" x="422.91" y="-13.462" size="1.778" layer="96"/>
</instance>
<instance part="100-70" gate="G$1" x="566.42" y="38.1" smashed="yes">
<attribute name="NAME" x="563.88" y="37.0586" size="1.778" layer="95"/>
<attribute name="VALUE" x="562.61" y="34.798" size="1.778" layer="96"/>
</instance>
<instance part="100-71" gate="G$1" x="566.42" y="35.56" smashed="yes">
<attribute name="NAME" x="563.88" y="34.5186" size="1.778" layer="95"/>
<attribute name="VALUE" x="562.61" y="32.258" size="1.778" layer="96"/>
</instance>
<instance part="100-72" gate="G$1" x="566.42" y="33.02" smashed="yes">
<attribute name="NAME" x="563.88" y="31.9786" size="1.778" layer="95"/>
<attribute name="VALUE" x="562.61" y="29.718" size="1.778" layer="96"/>
</instance>
<instance part="100-73" gate="G$1" x="566.42" y="30.48" smashed="yes">
<attribute name="NAME" x="563.88" y="29.4386" size="1.778" layer="95"/>
<attribute name="VALUE" x="562.61" y="27.178" size="1.778" layer="96"/>
</instance>
<instance part="100-74" gate="G$1" x="566.42" y="27.94" smashed="yes">
<attribute name="NAME" x="563.88" y="26.8986" size="1.778" layer="95"/>
<attribute name="VALUE" x="562.61" y="24.638" size="1.778" layer="96"/>
</instance>
<instance part="100-75" gate="G$1" x="566.42" y="25.4" smashed="yes">
<attribute name="NAME" x="563.88" y="24.3586" size="1.778" layer="95"/>
<attribute name="VALUE" x="562.61" y="22.098" size="1.778" layer="96"/>
</instance>
<instance part="100-76" gate="G$1" x="566.42" y="22.86" smashed="yes">
<attribute name="NAME" x="563.88" y="21.8186" size="1.778" layer="95"/>
<attribute name="VALUE" x="562.61" y="19.558" size="1.778" layer="96"/>
</instance>
<instance part="100-77" gate="G$1" x="547.37" y="85.09" smashed="yes">
<attribute name="NAME" x="544.83" y="84.0486" size="1.778" layer="95"/>
<attribute name="VALUE" x="543.56" y="81.788" size="1.778" layer="96"/>
</instance>
<instance part="J1" gate="G$1" x="845.82" y="149.86"/>
<instance part="COLON_A" gate="G$1" x="368.3" y="292.1"/>
<instance part="COLON_B" gate="G$1" x="363.22" y="269.24"/>
<instance part="COLON_C" gate="G$1" x="441.96" y="292.1"/>
<instance part="COLON_D" gate="G$1" x="434.34" y="266.7"/>
<instance part="470-43" gate="G$1" x="368.3" y="306.07" smashed="yes" rot="R90">
<attribute name="NAME" x="369.3414" y="303.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="371.602" y="302.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="470-44" gate="G$1" x="441.96" y="306.07" smashed="yes" rot="R90">
<attribute name="NAME" x="443.0014" y="303.53" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="445.262" y="302.26" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="A" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="7"/>
<wire x1="824.23" y1="113.03" x2="814.07" y2="113.03" width="0.1524" layer="91"/>
<label x="814.07" y="113.03" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="132.08" y1="191.77" x2="129.54" y2="191.77" width="0.1524" layer="91"/>
<label x="129.54" y="191.77" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-5" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="252.73" y1="190.5" x2="250.19" y2="190.5" width="0.1524" layer="91"/>
<label x="250.19" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-10" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="372.11" y1="190.5" x2="369.57" y2="190.5" width="0.1524" layer="91"/>
<label x="369.57" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-15" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="492.76" y1="190.5" x2="490.22" y2="190.5" width="0.1524" layer="91"/>
<label x="490.22" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-20" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="737.87" y1="190.5" x2="735.33" y2="190.5" width="0.1524" layer="91"/>
<label x="735.33" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-30" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="118.11" y1="93.98" x2="114.3" y2="93.98" width="0.1524" layer="91"/>
<label x="114.3" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-34" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="193.04" y1="93.98" x2="189.23" y2="93.98" width="0.1524" layer="91"/>
<label x="189.23" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-40" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="269.24" y1="93.98" x2="265.43" y2="93.98" width="0.1524" layer="91"/>
<label x="265.43" y="93.98" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-46" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="349.25" y1="92.71" x2="345.44" y2="92.71" width="0.1524" layer="91"/>
<label x="345.44" y="92.71" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-52" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="429.26" y1="92.71" x2="425.45" y2="92.71" width="0.1524" layer="91"/>
<label x="425.45" y="92.71" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-58" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="506.73" y1="92.71" x2="502.92" y2="92.71" width="0.1524" layer="91"/>
<label x="502.92" y="92.71" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-64" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="588.01" y1="92.71" x2="584.2" y2="92.71" width="0.1524" layer="91"/>
<label x="584.2" y="92.71" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-70" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="665.48" y1="92.71" x2="661.67" y2="92.71" width="0.1524" layer="91"/>
<pinref part="1K-76" gate="G$1" pin="1"/>
<label x="661.67" y="92.71" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="342.9" y1="-7.62" x2="339.09" y2="-7.62" width="0.1524" layer="91"/>
<label x="339.09" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-82" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="421.64" y1="-7.62" x2="417.83" y2="-7.62" width="0.1524" layer="91"/>
<label x="417.83" y="-7.62" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-88" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="1K-25" gate="G$1" pin="1"/>
<wire x1="615.95" y1="190.5" x2="613.41" y2="190.5" width="0.1524" layer="91"/>
<label x="613.41" y="190.5" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="D" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="15"/>
<wire x1="824.23" y1="102.87" x2="814.07" y2="102.87" width="0.1524" layer="91"/>
<label x="814.07" y="102.87" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="132.08" y1="194.31" x2="129.54" y2="194.31" width="0.1524" layer="91"/>
<label x="129.54" y="194.31" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-4" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="252.73" y1="193.04" x2="250.19" y2="193.04" width="0.1524" layer="91"/>
<label x="250.19" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-9" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="372.11" y1="193.04" x2="369.57" y2="193.04" width="0.1524" layer="91"/>
<label x="369.57" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-14" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="492.76" y1="193.04" x2="490.22" y2="193.04" width="0.1524" layer="91"/>
<label x="490.22" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-19" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="737.87" y1="193.04" x2="735.33" y2="193.04" width="0.1524" layer="91"/>
<label x="735.33" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-29" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="118.11" y1="96.52" x2="114.3" y2="96.52" width="0.1524" layer="91"/>
<label x="114.3" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-33" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="193.04" y1="96.52" x2="189.23" y2="96.52" width="0.1524" layer="91"/>
<label x="189.23" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-39" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="269.24" y1="96.52" x2="265.43" y2="96.52" width="0.1524" layer="91"/>
<label x="265.43" y="96.52" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-45" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="349.25" y1="95.25" x2="345.44" y2="95.25" width="0.1524" layer="91"/>
<label x="345.44" y="95.25" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-51" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="429.26" y1="95.25" x2="425.45" y2="95.25" width="0.1524" layer="91"/>
<label x="425.45" y="95.25" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-57" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="506.73" y1="95.25" x2="502.92" y2="95.25" width="0.1524" layer="91"/>
<label x="502.92" y="95.25" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-63" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="588.01" y1="95.25" x2="584.2" y2="95.25" width="0.1524" layer="91"/>
<label x="584.2" y="95.25" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-69" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="665.48" y1="95.25" x2="661.67" y2="95.25" width="0.1524" layer="91"/>
<pinref part="1K-75" gate="G$1" pin="1"/>
<label x="661.67" y="95.25" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="342.9" y1="-5.08" x2="339.09" y2="-5.08" width="0.1524" layer="91"/>
<label x="339.09" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-81" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="421.64" y1="-5.08" x2="417.83" y2="-5.08" width="0.1524" layer="91"/>
<label x="417.83" y="-5.08" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-87" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="1K-24" gate="G$1" pin="1"/>
<wire x1="615.95" y1="193.04" x2="613.41" y2="193.04" width="0.1524" layer="91"/>
<label x="613.41" y="193.04" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="C" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="13"/>
<wire x1="824.23" y1="105.41" x2="814.07" y2="105.41" width="0.1524" layer="91"/>
<label x="814.07" y="105.41" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="132.08" y1="196.85" x2="129.54" y2="196.85" width="0.1524" layer="91"/>
<label x="129.54" y="196.85" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-3" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="252.73" y1="195.58" x2="250.19" y2="195.58" width="0.1524" layer="91"/>
<label x="250.19" y="195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-8" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="372.11" y1="195.58" x2="369.57" y2="195.58" width="0.1524" layer="91"/>
<label x="369.57" y="195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-13" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="492.76" y1="195.58" x2="490.22" y2="195.58" width="0.1524" layer="91"/>
<label x="490.22" y="195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-18" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="737.87" y1="195.58" x2="735.33" y2="195.58" width="0.1524" layer="91"/>
<label x="735.33" y="195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-28" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="118.11" y1="99.06" x2="114.3" y2="99.06" width="0.1524" layer="91"/>
<label x="114.3" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-32" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="193.04" y1="99.06" x2="189.23" y2="99.06" width="0.1524" layer="91"/>
<label x="189.23" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-38" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="269.24" y1="99.06" x2="265.43" y2="99.06" width="0.1524" layer="91"/>
<label x="265.43" y="99.06" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-44" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="349.25" y1="97.79" x2="345.44" y2="97.79" width="0.1524" layer="91"/>
<label x="345.44" y="97.79" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-50" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="429.26" y1="97.79" x2="425.45" y2="97.79" width="0.1524" layer="91"/>
<label x="425.45" y="97.79" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-56" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="506.73" y1="97.79" x2="502.92" y2="97.79" width="0.1524" layer="91"/>
<label x="502.92" y="97.79" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-62" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="588.01" y1="97.79" x2="584.2" y2="97.79" width="0.1524" layer="91"/>
<label x="584.2" y="97.79" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-68" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="665.48" y1="97.79" x2="661.67" y2="97.79" width="0.1524" layer="91"/>
<pinref part="1K-74" gate="G$1" pin="1"/>
<label x="661.67" y="97.79" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="342.9" y1="-2.54" x2="339.09" y2="-2.54" width="0.1524" layer="91"/>
<label x="339.09" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-80" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="421.64" y1="-2.54" x2="417.83" y2="-2.54" width="0.1524" layer="91"/>
<label x="417.83" y="-2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-86" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="1K-23" gate="G$1" pin="1"/>
<wire x1="615.95" y1="195.58" x2="613.41" y2="195.58" width="0.1524" layer="91"/>
<label x="613.41" y="195.58" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="B" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="11"/>
<wire x1="824.23" y1="107.95" x2="814.07" y2="107.95" width="0.1524" layer="91"/>
<label x="814.07" y="107.95" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="252.73" y1="198.12" x2="250.19" y2="198.12" width="0.1524" layer="91"/>
<label x="250.19" y="198.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-7" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="372.11" y1="198.12" x2="369.57" y2="198.12" width="0.1524" layer="91"/>
<label x="369.57" y="198.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-12" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="492.76" y1="198.12" x2="490.22" y2="198.12" width="0.1524" layer="91"/>
<label x="490.22" y="198.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-17" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="737.87" y1="198.12" x2="735.33" y2="198.12" width="0.1524" layer="91"/>
<label x="735.33" y="198.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-27" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="118.11" y1="101.6" x2="114.3" y2="101.6" width="0.1524" layer="91"/>
<label x="114.3" y="101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-31" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="193.04" y1="101.6" x2="189.23" y2="101.6" width="0.1524" layer="91"/>
<label x="189.23" y="101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-37" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="269.24" y1="101.6" x2="265.43" y2="101.6" width="0.1524" layer="91"/>
<label x="265.43" y="101.6" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-43" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="349.25" y1="100.33" x2="345.44" y2="100.33" width="0.1524" layer="91"/>
<label x="345.44" y="100.33" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-49" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="429.26" y1="100.33" x2="425.45" y2="100.33" width="0.1524" layer="91"/>
<label x="425.45" y="100.33" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-55" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="506.73" y1="100.33" x2="502.92" y2="100.33" width="0.1524" layer="91"/>
<label x="502.92" y="100.33" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-61" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="588.01" y1="100.33" x2="584.2" y2="100.33" width="0.1524" layer="91"/>
<label x="584.2" y="100.33" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-67" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="665.48" y1="100.33" x2="661.67" y2="100.33" width="0.1524" layer="91"/>
<pinref part="1K-73" gate="G$1" pin="1"/>
<label x="661.67" y="100.33" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="342.9" y1="0" x2="339.09" y2="0" width="0.1524" layer="91"/>
<label x="339.09" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-79" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="421.64" y1="0" x2="417.83" y2="0" width="0.1524" layer="91"/>
<label x="417.83" y="0" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-85" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="1K-2" gate="G$1" pin="1"/>
<wire x1="132.08" y1="199.39" x2="129.54" y2="199.39" width="0.1524" layer="91"/>
<label x="129.54" y="199.39" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="1K-22" gate="G$1" pin="1"/>
<wire x1="615.95" y1="198.12" x2="613.41" y2="198.12" width="0.1524" layer="91"/>
<label x="613.41" y="198.12" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DAY_A_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="19"/>
<wire x1="824.23" y1="97.79" x2="814.07" y2="97.79" width="0.1524" layer="91"/>
<label x="814.07" y="97.79" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="118.11" y1="104.14" x2="114.3" y2="104.14" width="0.1524" layer="91"/>
<label x="114.3" y="104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-91" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="9"/>
<wire x1="824.23" y1="110.49" x2="814.07" y2="110.49" width="0.1524" layer="91"/>
<label x="814.07" y="110.49" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="25"/>
<wire x1="824.23" y1="90.17" x2="814.07" y2="90.17" width="0.1524" layer="91"/>
<label x="814.07" y="90.17" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="39"/>
<wire x1="824.23" y1="72.39" x2="814.07" y2="72.39" width="0.1524" layer="91"/>
<label x="814.07" y="72.39" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="34"/>
<wire x1="831.85" y1="80.01" x2="842.01" y2="80.01" width="0.1524" layer="91"/>
<label x="842.01" y="80.01" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="30"/>
<wire x1="831.85" y1="85.09" x2="842.01" y2="85.09" width="0.1524" layer="91"/>
<label x="842.01" y="85.09" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="20"/>
<wire x1="831.85" y1="97.79" x2="842.01" y2="97.79" width="0.1524" layer="91"/>
<label x="842.01" y="97.79" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="14"/>
<wire x1="831.85" y1="105.41" x2="842.01" y2="105.41" width="0.1524" layer="91"/>
<label x="842.01" y="105.41" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="6"/>
<wire x1="831.85" y1="115.57" x2="842.01" y2="115.57" width="0.1524" layer="91"/>
<label x="842.01" y="115.57" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="GND"/>
<wire x1="181.61" y1="181.61" x2="177.8" y2="181.61" width="0.1524" layer="91"/>
<wire x1="177.8" y1="181.61" x2="177.8" y2="177.8" width="0.1524" layer="91"/>
<wire x1="177.8" y1="177.8" x2="142.24" y2="177.8" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="142.24" y1="177.8" x2="142.24" y2="176.53" width="0.1524" layer="91"/>
<pinref part="4543_HOUR_A" gate="A" pin="VSS"/>
<wire x1="146.05" y1="184.15" x2="142.24" y2="184.15" width="0.1524" layer="91"/>
<wire x1="142.24" y1="184.15" x2="142.24" y2="177.8" width="0.1524" layer="91"/>
<junction x="142.24" y="177.8"/>
<pinref part="4543_HOUR_A" gate="A" pin="BI"/>
<wire x1="146.05" y1="186.69" x2="142.24" y2="186.69" width="0.1524" layer="91"/>
<wire x1="142.24" y1="186.69" x2="142.24" y2="184.15" width="0.1524" layer="91"/>
<junction x="142.24" y="184.15"/>
<pinref part="4543_HOUR_A" gate="A" pin="PH"/>
<wire x1="146.05" y1="189.23" x2="142.24" y2="189.23" width="0.1524" layer="91"/>
<wire x1="142.24" y1="189.23" x2="142.24" y2="186.69" width="0.1524" layer="91"/>
<junction x="142.24" y="186.69"/>
</segment>
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="GND"/>
<wire x1="302.26" y1="180.34" x2="298.45" y2="180.34" width="0.1524" layer="91"/>
<wire x1="298.45" y1="180.34" x2="298.45" y2="176.53" width="0.1524" layer="91"/>
<wire x1="298.45" y1="176.53" x2="262.89" y2="176.53" width="0.1524" layer="91"/>
<wire x1="262.89" y1="176.53" x2="262.89" y2="175.26" width="0.1524" layer="91"/>
<pinref part="4543_HOUR_B" gate="A" pin="VSS"/>
<wire x1="266.7" y1="182.88" x2="262.89" y2="182.88" width="0.1524" layer="91"/>
<wire x1="262.89" y1="182.88" x2="262.89" y2="176.53" width="0.1524" layer="91"/>
<junction x="262.89" y="176.53"/>
<pinref part="4543_HOUR_B" gate="A" pin="BI"/>
<wire x1="266.7" y1="185.42" x2="262.89" y2="185.42" width="0.1524" layer="91"/>
<wire x1="262.89" y1="185.42" x2="262.89" y2="182.88" width="0.1524" layer="91"/>
<junction x="262.89" y="182.88"/>
<pinref part="4543_HOUR_B" gate="A" pin="PH"/>
<wire x1="266.7" y1="187.96" x2="262.89" y2="187.96" width="0.1524" layer="91"/>
<wire x1="262.89" y1="187.96" x2="262.89" y2="185.42" width="0.1524" layer="91"/>
<junction x="262.89" y="185.42"/>
<label x="262.89" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="2003_MIN_A" gate="A" pin="GND"/>
<wire x1="421.64" y1="180.34" x2="417.83" y2="180.34" width="0.1524" layer="91"/>
<wire x1="417.83" y1="180.34" x2="417.83" y2="176.53" width="0.1524" layer="91"/>
<wire x1="417.83" y1="176.53" x2="382.27" y2="176.53" width="0.1524" layer="91"/>
<wire x1="382.27" y1="176.53" x2="382.27" y2="175.26" width="0.1524" layer="91"/>
<pinref part="4543_MIN_A" gate="A" pin="VSS"/>
<wire x1="386.08" y1="182.88" x2="382.27" y2="182.88" width="0.1524" layer="91"/>
<wire x1="382.27" y1="182.88" x2="382.27" y2="176.53" width="0.1524" layer="91"/>
<junction x="382.27" y="176.53"/>
<pinref part="4543_MIN_A" gate="A" pin="BI"/>
<wire x1="386.08" y1="185.42" x2="382.27" y2="185.42" width="0.1524" layer="91"/>
<wire x1="382.27" y1="185.42" x2="382.27" y2="182.88" width="0.1524" layer="91"/>
<junction x="382.27" y="182.88"/>
<pinref part="4543_MIN_A" gate="A" pin="PH"/>
<wire x1="386.08" y1="187.96" x2="382.27" y2="187.96" width="0.1524" layer="91"/>
<wire x1="382.27" y1="187.96" x2="382.27" y2="185.42" width="0.1524" layer="91"/>
<junction x="382.27" y="185.42"/>
<label x="382.27" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="2003_MIN_B" gate="A" pin="GND"/>
<wire x1="542.29" y1="180.34" x2="538.48" y2="180.34" width="0.1524" layer="91"/>
<wire x1="538.48" y1="180.34" x2="538.48" y2="176.53" width="0.1524" layer="91"/>
<wire x1="538.48" y1="176.53" x2="502.92" y2="176.53" width="0.1524" layer="91"/>
<wire x1="502.92" y1="176.53" x2="502.92" y2="175.26" width="0.1524" layer="91"/>
<pinref part="4543_MIN_B" gate="A" pin="VSS"/>
<wire x1="506.73" y1="182.88" x2="502.92" y2="182.88" width="0.1524" layer="91"/>
<wire x1="502.92" y1="182.88" x2="502.92" y2="176.53" width="0.1524" layer="91"/>
<junction x="502.92" y="176.53"/>
<pinref part="4543_MIN_B" gate="A" pin="BI"/>
<wire x1="506.73" y1="185.42" x2="502.92" y2="185.42" width="0.1524" layer="91"/>
<wire x1="502.92" y1="185.42" x2="502.92" y2="182.88" width="0.1524" layer="91"/>
<junction x="502.92" y="182.88"/>
<pinref part="4543_MIN_B" gate="A" pin="PH"/>
<wire x1="506.73" y1="187.96" x2="502.92" y2="187.96" width="0.1524" layer="91"/>
<wire x1="502.92" y1="187.96" x2="502.92" y2="185.42" width="0.1524" layer="91"/>
<junction x="502.92" y="185.42"/>
<label x="502.92" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="2003_SEC_A" gate="A" pin="GND"/>
<wire x1="665.48" y1="180.34" x2="661.67" y2="180.34" width="0.1524" layer="91"/>
<wire x1="661.67" y1="180.34" x2="661.67" y2="176.53" width="0.1524" layer="91"/>
<wire x1="661.67" y1="176.53" x2="626.11" y2="176.53" width="0.1524" layer="91"/>
<wire x1="626.11" y1="176.53" x2="626.11" y2="175.26" width="0.1524" layer="91"/>
<pinref part="4543_SEC_A" gate="A" pin="VSS"/>
<wire x1="629.92" y1="182.88" x2="626.11" y2="182.88" width="0.1524" layer="91"/>
<wire x1="626.11" y1="182.88" x2="626.11" y2="176.53" width="0.1524" layer="91"/>
<junction x="626.11" y="176.53"/>
<pinref part="4543_SEC_A" gate="A" pin="BI"/>
<wire x1="629.92" y1="185.42" x2="626.11" y2="185.42" width="0.1524" layer="91"/>
<wire x1="626.11" y1="185.42" x2="626.11" y2="182.88" width="0.1524" layer="91"/>
<junction x="626.11" y="182.88"/>
<pinref part="4543_SEC_A" gate="A" pin="PH"/>
<wire x1="629.92" y1="187.96" x2="626.11" y2="187.96" width="0.1524" layer="91"/>
<wire x1="626.11" y1="187.96" x2="626.11" y2="185.42" width="0.1524" layer="91"/>
<junction x="626.11" y="185.42"/>
<label x="626.11" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="2003_SEC_B" gate="A" pin="GND"/>
<wire x1="787.4" y1="180.34" x2="783.59" y2="180.34" width="0.1524" layer="91"/>
<wire x1="783.59" y1="180.34" x2="783.59" y2="176.53" width="0.1524" layer="91"/>
<wire x1="783.59" y1="176.53" x2="748.03" y2="176.53" width="0.1524" layer="91"/>
<wire x1="748.03" y1="176.53" x2="748.03" y2="175.26" width="0.1524" layer="91"/>
<pinref part="4543_SEC_B" gate="A" pin="VSS"/>
<wire x1="751.84" y1="182.88" x2="748.03" y2="182.88" width="0.1524" layer="91"/>
<wire x1="748.03" y1="182.88" x2="748.03" y2="176.53" width="0.1524" layer="91"/>
<junction x="748.03" y="176.53"/>
<pinref part="4543_SEC_B" gate="A" pin="BI"/>
<wire x1="751.84" y1="185.42" x2="748.03" y2="185.42" width="0.1524" layer="91"/>
<wire x1="748.03" y1="185.42" x2="748.03" y2="182.88" width="0.1524" layer="91"/>
<junction x="748.03" y="182.88"/>
<pinref part="4543_SEC_B" gate="A" pin="PH"/>
<wire x1="751.84" y1="187.96" x2="748.03" y2="187.96" width="0.1524" layer="91"/>
<wire x1="748.03" y1="187.96" x2="748.03" y2="185.42" width="0.1524" layer="91"/>
<junction x="748.03" y="185.42"/>
<label x="748.03" y="175.26" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="4543_DAY_A" gate="A" pin="BI"/>
<wire x1="128.27" y1="88.9" x2="118.11" y2="88.9" width="0.1524" layer="91"/>
<wire x1="118.11" y1="88.9" x2="118.11" y2="86.36" width="0.1524" layer="91"/>
<label x="118.11" y="82.55" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_DAY_A" gate="A" pin="VSS"/>
<wire x1="118.11" y1="86.36" x2="118.11" y2="82.55" width="0.1524" layer="91"/>
<wire x1="128.27" y1="86.36" x2="118.11" y2="86.36" width="0.1524" layer="91"/>
<junction x="118.11" y="86.36"/>
</segment>
<segment>
<pinref part="4543_DAY_B" gate="A" pin="BI"/>
<wire x1="203.2" y1="88.9" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<wire x1="193.04" y1="88.9" x2="193.04" y2="86.36" width="0.1524" layer="91"/>
<label x="193.04" y="82.55" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_DAY_B" gate="A" pin="VSS"/>
<wire x1="193.04" y1="86.36" x2="193.04" y2="82.55" width="0.1524" layer="91"/>
<wire x1="203.2" y1="86.36" x2="193.04" y2="86.36" width="0.1524" layer="91"/>
<junction x="193.04" y="86.36"/>
</segment>
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="BI"/>
<wire x1="279.4" y1="88.9" x2="269.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="269.24" y1="88.9" x2="269.24" y2="86.36" width="0.1524" layer="91"/>
<label x="269.24" y="82.55" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_MONTH_A" gate="A" pin="VSS"/>
<wire x1="269.24" y1="86.36" x2="269.24" y2="82.55" width="0.1524" layer="91"/>
<wire x1="279.4" y1="86.36" x2="269.24" y2="86.36" width="0.1524" layer="91"/>
<junction x="269.24" y="86.36"/>
</segment>
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="BI"/>
<wire x1="359.41" y1="87.63" x2="349.25" y2="87.63" width="0.1524" layer="91"/>
<wire x1="349.25" y1="87.63" x2="349.25" y2="85.09" width="0.1524" layer="91"/>
<label x="349.25" y="81.28" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_MONTH_B" gate="A" pin="VSS"/>
<wire x1="349.25" y1="85.09" x2="349.25" y2="81.28" width="0.1524" layer="91"/>
<wire x1="359.41" y1="85.09" x2="349.25" y2="85.09" width="0.1524" layer="91"/>
<junction x="349.25" y="85.09"/>
</segment>
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="BI"/>
<wire x1="439.42" y1="87.63" x2="429.26" y2="87.63" width="0.1524" layer="91"/>
<wire x1="429.26" y1="87.63" x2="429.26" y2="85.09" width="0.1524" layer="91"/>
<label x="429.26" y="81.28" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_YEAR_A" gate="A" pin="VSS"/>
<wire x1="429.26" y1="85.09" x2="429.26" y2="81.28" width="0.1524" layer="91"/>
<wire x1="439.42" y1="85.09" x2="429.26" y2="85.09" width="0.1524" layer="91"/>
<junction x="429.26" y="85.09"/>
</segment>
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="BI"/>
<wire x1="516.89" y1="87.63" x2="506.73" y2="87.63" width="0.1524" layer="91"/>
<wire x1="506.73" y1="87.63" x2="506.73" y2="85.09" width="0.1524" layer="91"/>
<label x="506.73" y="81.28" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_YEAR_B" gate="A" pin="VSS"/>
<wire x1="506.73" y1="85.09" x2="506.73" y2="81.28" width="0.1524" layer="91"/>
<wire x1="516.89" y1="85.09" x2="506.73" y2="85.09" width="0.1524" layer="91"/>
<junction x="506.73" y="85.09"/>
</segment>
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="BI"/>
<wire x1="598.17" y1="87.63" x2="588.01" y2="87.63" width="0.1524" layer="91"/>
<wire x1="588.01" y1="87.63" x2="588.01" y2="85.09" width="0.1524" layer="91"/>
<label x="588.01" y="81.28" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_YEAR_C" gate="A" pin="VSS"/>
<wire x1="588.01" y1="85.09" x2="588.01" y2="81.28" width="0.1524" layer="91"/>
<wire x1="598.17" y1="85.09" x2="588.01" y2="85.09" width="0.1524" layer="91"/>
<junction x="588.01" y="85.09"/>
</segment>
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="BI"/>
<wire x1="675.64" y1="87.63" x2="665.48" y2="87.63" width="0.1524" layer="91"/>
<wire x1="665.48" y1="87.63" x2="665.48" y2="85.09" width="0.1524" layer="91"/>
<label x="665.48" y="81.28" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_YEAR_D" gate="A" pin="VSS"/>
<wire x1="665.48" y1="85.09" x2="665.48" y2="81.28" width="0.1524" layer="91"/>
<wire x1="675.64" y1="85.09" x2="665.48" y2="85.09" width="0.1524" layer="91"/>
<junction x="665.48" y="85.09"/>
</segment>
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="BI"/>
<wire x1="353.06" y1="-12.7" x2="342.9" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="342.9" y1="-12.7" x2="342.9" y2="-15.24" width="0.1524" layer="91"/>
<label x="342.9" y="-19.05" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_TEMP_A" gate="A" pin="VSS"/>
<wire x1="342.9" y1="-15.24" x2="342.9" y2="-19.05" width="0.1524" layer="91"/>
<wire x1="353.06" y1="-15.24" x2="342.9" y2="-15.24" width="0.1524" layer="91"/>
<junction x="342.9" y="-15.24"/>
</segment>
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="BI"/>
<wire x1="431.8" y1="-12.7" x2="421.64" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="421.64" y1="-12.7" x2="421.64" y2="-15.24" width="0.1524" layer="91"/>
<label x="421.64" y="-19.05" size="1.778" layer="95" xref="yes"/>
<pinref part="4543_TEMP_B" gate="A" pin="VSS"/>
<wire x1="421.64" y1="-15.24" x2="421.64" y2="-19.05" width="0.1524" layer="91"/>
<wire x1="431.8" y1="-15.24" x2="421.64" y2="-15.24" width="0.1524" layer="91"/>
<junction x="421.64" y="-15.24"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="VSS"/>
<wire x1="571.5" y1="12.7" x2="567.69" y2="12.7" width="0.1524" layer="91"/>
<label x="567.69" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="INH"/>
<wire x1="596.9" y1="17.78" x2="600.71" y2="17.78" width="0.1524" layer="91"/>
<label x="600.71" y="17.78" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="D"/>
<wire x1="596.9" y1="12.7" x2="600.71" y2="12.7" width="0.1524" layer="91"/>
<label x="600.71" y="12.7" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="THU" gate="G$1" pin="C"/>
<wire x1="530.86" y1="0" x2="530.86" y2="-3.81" width="0.1524" layer="91"/>
<label x="530.86" y="-15.24" size="1.778" layer="95" xref="yes"/>
<pinref part="WED" gate="G$1" pin="C"/>
<wire x1="530.86" y1="-3.81" x2="530.86" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="523.24" y1="0" x2="523.24" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="523.24" y1="-3.81" x2="530.86" y2="-3.81" width="0.1524" layer="91"/>
<junction x="530.86" y="-3.81"/>
<pinref part="FRI" gate="G$1" pin="C"/>
<wire x1="538.48" y1="0" x2="538.48" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="538.48" y1="-3.81" x2="530.86" y2="-3.81" width="0.1524" layer="91"/>
<pinref part="TUE" gate="G$1" pin="C"/>
<wire x1="515.62" y1="0" x2="515.62" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="515.62" y1="-3.81" x2="523.24" y2="-3.81" width="0.1524" layer="91"/>
<junction x="523.24" y="-3.81"/>
<pinref part="MON" gate="G$1" pin="C"/>
<wire x1="508" y1="0" x2="508" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="508" y1="-3.81" x2="515.62" y2="-3.81" width="0.1524" layer="91"/>
<junction x="515.62" y="-3.81"/>
<pinref part="SAT" gate="G$1" pin="C"/>
<wire x1="546.1" y1="0" x2="546.1" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="546.1" y1="-3.81" x2="538.48" y2="-3.81" width="0.1524" layer="91"/>
<junction x="538.48" y="-3.81"/>
<pinref part="SUN" gate="G$1" pin="C"/>
<wire x1="553.72" y1="0" x2="553.72" y2="-3.81" width="0.1524" layer="91"/>
<wire x1="553.72" y1="-3.81" x2="546.1" y2="-3.81" width="0.1524" layer="91"/>
<junction x="546.1" y="-3.81"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="853.44" y1="152.4" x2="855.98" y2="152.4" width="0.1524" layer="91"/>
<label x="855.98" y="152.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="COLON_B" gate="G$1" pin="C"/>
<wire x1="363.22" y1="264.16" x2="363.22" y2="259.08" width="0.1524" layer="91"/>
<label x="363.22" y="259.08" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="COLON_D" gate="G$1" pin="C"/>
<wire x1="434.34" y1="261.62" x2="434.34" y2="255.27" width="0.1524" layer="91"/>
<label x="434.34" y="255.27" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="1"/>
<wire x1="824.23" y1="120.65" x2="814.07" y2="120.65" width="0.1524" layer="91"/>
<label x="814.07" y="120.65" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="17"/>
<wire x1="824.23" y1="100.33" x2="814.07" y2="100.33" width="0.1524" layer="91"/>
<label x="814.07" y="100.33" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="VDD"/>
<wire x1="171.45" y1="201.93" x2="171.45" y2="207.01" width="0.1524" layer="91"/>
<label x="171.45" y="207.01" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="VDD"/>
<wire x1="292.1" y1="200.66" x2="292.1" y2="205.74" width="0.1524" layer="91"/>
<label x="292.1" y="205.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="4543_MIN_A" gate="A" pin="VDD"/>
<wire x1="411.48" y1="200.66" x2="411.48" y2="205.74" width="0.1524" layer="91"/>
<label x="411.48" y="205.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="4543_MIN_B" gate="A" pin="VDD"/>
<wire x1="532.13" y1="200.66" x2="532.13" y2="205.74" width="0.1524" layer="91"/>
<label x="532.13" y="205.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="4543_SEC_A" gate="A" pin="VDD"/>
<wire x1="655.32" y1="200.66" x2="655.32" y2="205.74" width="0.1524" layer="91"/>
<label x="655.32" y="205.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="4543_SEC_B" gate="A" pin="VDD"/>
<wire x1="777.24" y1="200.66" x2="777.24" y2="205.74" width="0.1524" layer="91"/>
<label x="777.24" y="205.74" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="118.11" y1="91.44" x2="114.3" y2="91.44" width="0.1524" layer="91"/>
<label x="114.3" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-35" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_DAY_A" gate="A" pin="VDD"/>
<wire x1="153.67" y1="104.14" x2="154.94" y2="104.14" width="0.1524" layer="91"/>
<label x="156.21" y="104.14" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_DAY_A" gate="G$1" pin="VDD1"/>
<wire x1="154.94" y1="104.14" x2="156.21" y2="104.14" width="0.1524" layer="91"/>
<wire x1="149.86" y1="129.54" x2="154.94" y2="129.54" width="0.1524" layer="91"/>
<wire x1="154.94" y1="129.54" x2="154.94" y2="104.14" width="0.1524" layer="91"/>
<junction x="154.94" y="104.14"/>
</segment>
<segment>
<wire x1="193.04" y1="91.44" x2="189.23" y2="91.44" width="0.1524" layer="91"/>
<label x="189.23" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-41" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_DAY_B" gate="A" pin="VDD"/>
<wire x1="228.6" y1="104.14" x2="229.87" y2="104.14" width="0.1524" layer="91"/>
<label x="231.14" y="104.14" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_DAY_B" gate="G$1" pin="VDD1"/>
<wire x1="229.87" y1="104.14" x2="231.14" y2="104.14" width="0.1524" layer="91"/>
<wire x1="224.79" y1="129.54" x2="229.87" y2="129.54" width="0.1524" layer="91"/>
<wire x1="229.87" y1="129.54" x2="229.87" y2="104.14" width="0.1524" layer="91"/>
<junction x="229.87" y="104.14"/>
</segment>
<segment>
<wire x1="269.24" y1="91.44" x2="265.43" y2="91.44" width="0.1524" layer="91"/>
<label x="265.43" y="91.44" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-47" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="VDD"/>
<wire x1="304.8" y1="104.14" x2="306.07" y2="104.14" width="0.1524" layer="91"/>
<label x="307.34" y="104.14" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_MONTH_A" gate="G$1" pin="VDD1"/>
<wire x1="306.07" y1="104.14" x2="307.34" y2="104.14" width="0.1524" layer="91"/>
<wire x1="300.99" y1="129.54" x2="306.07" y2="129.54" width="0.1524" layer="91"/>
<wire x1="306.07" y1="129.54" x2="306.07" y2="104.14" width="0.1524" layer="91"/>
<junction x="306.07" y="104.14"/>
</segment>
<segment>
<wire x1="349.25" y1="90.17" x2="345.44" y2="90.17" width="0.1524" layer="91"/>
<label x="345.44" y="90.17" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-53" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="VDD"/>
<wire x1="384.81" y1="102.87" x2="386.08" y2="102.87" width="0.1524" layer="91"/>
<label x="387.35" y="102.87" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_MONTH_B" gate="G$1" pin="VDD1"/>
<wire x1="386.08" y1="102.87" x2="387.35" y2="102.87" width="0.1524" layer="91"/>
<wire x1="381" y1="128.27" x2="386.08" y2="128.27" width="0.1524" layer="91"/>
<wire x1="386.08" y1="128.27" x2="386.08" y2="102.87" width="0.1524" layer="91"/>
<junction x="386.08" y="102.87"/>
</segment>
<segment>
<wire x1="429.26" y1="90.17" x2="425.45" y2="90.17" width="0.1524" layer="91"/>
<label x="425.45" y="90.17" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-59" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="VDD"/>
<wire x1="464.82" y1="102.87" x2="466.09" y2="102.87" width="0.1524" layer="91"/>
<label x="467.36" y="102.87" size="1.778" layer="95" xref="yes"/>
<wire x1="466.09" y1="102.87" x2="467.36" y2="102.87" width="0.1524" layer="91"/>
<junction x="466.09" y="102.87"/>
<wire x1="466.09" y1="128.27" x2="466.09" y2="102.87" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_A" gate="G$1" pin="VDD1"/>
<wire x1="466.09" y1="128.27" x2="461.01" y2="128.27" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="506.73" y1="90.17" x2="502.92" y2="90.17" width="0.1524" layer="91"/>
<label x="502.92" y="90.17" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-65" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="VDD"/>
<wire x1="542.29" y1="102.87" x2="543.56" y2="102.87" width="0.1524" layer="91"/>
<label x="544.83" y="102.87" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_YEAR_B" gate="G$1" pin="VDD1"/>
<wire x1="543.56" y1="102.87" x2="544.83" y2="102.87" width="0.1524" layer="91"/>
<wire x1="538.48" y1="128.27" x2="543.56" y2="128.27" width="0.1524" layer="91"/>
<wire x1="543.56" y1="128.27" x2="543.56" y2="102.87" width="0.1524" layer="91"/>
<junction x="543.56" y="102.87"/>
</segment>
<segment>
<wire x1="588.01" y1="90.17" x2="584.2" y2="90.17" width="0.1524" layer="91"/>
<label x="584.2" y="90.17" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-71" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="VDD"/>
<wire x1="623.57" y1="102.87" x2="624.84" y2="102.87" width="0.1524" layer="91"/>
<label x="626.11" y="102.87" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_YEAR_C" gate="G$1" pin="VDD1"/>
<wire x1="624.84" y1="102.87" x2="626.11" y2="102.87" width="0.1524" layer="91"/>
<wire x1="619.76" y1="128.27" x2="624.84" y2="128.27" width="0.1524" layer="91"/>
<wire x1="624.84" y1="128.27" x2="624.84" y2="102.87" width="0.1524" layer="91"/>
<junction x="624.84" y="102.87"/>
</segment>
<segment>
<wire x1="665.48" y1="90.17" x2="661.67" y2="90.17" width="0.1524" layer="91"/>
<label x="661.67" y="90.17" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-77" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="VDD"/>
<wire x1="701.04" y1="102.87" x2="702.31" y2="102.87" width="0.1524" layer="91"/>
<label x="703.58" y="102.87" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_YEAR_D" gate="G$1" pin="VDD1"/>
<wire x1="702.31" y1="102.87" x2="703.58" y2="102.87" width="0.1524" layer="91"/>
<wire x1="697.23" y1="128.27" x2="702.31" y2="128.27" width="0.1524" layer="91"/>
<wire x1="702.31" y1="128.27" x2="702.31" y2="102.87" width="0.1524" layer="91"/>
<junction x="702.31" y="102.87"/>
</segment>
<segment>
<wire x1="342.9" y1="-10.16" x2="339.09" y2="-10.16" width="0.1524" layer="91"/>
<label x="339.09" y="-10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-83" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="VDD"/>
<wire x1="378.46" y1="2.54" x2="379.73" y2="2.54" width="0.1524" layer="91"/>
<label x="381" y="2.54" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_TEMP_A" gate="G$1" pin="VDD1"/>
<wire x1="379.73" y1="2.54" x2="381" y2="2.54" width="0.1524" layer="91"/>
<wire x1="374.65" y1="27.94" x2="379.73" y2="27.94" width="0.1524" layer="91"/>
<wire x1="379.73" y1="27.94" x2="379.73" y2="2.54" width="0.1524" layer="91"/>
<junction x="379.73" y="2.54"/>
</segment>
<segment>
<wire x1="421.64" y1="-10.16" x2="417.83" y2="-10.16" width="0.1524" layer="91"/>
<label x="417.83" y="-10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-89" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="VDD"/>
<wire x1="457.2" y1="2.54" x2="458.47" y2="2.54" width="0.1524" layer="91"/>
<label x="459.74" y="2.54" size="1.778" layer="95" xref="yes"/>
<pinref part="LCD_TEMP_B" gate="G$1" pin="VDD1"/>
<wire x1="458.47" y1="2.54" x2="459.74" y2="2.54" width="0.1524" layer="91"/>
<wire x1="453.39" y1="27.94" x2="458.47" y2="27.94" width="0.1524" layer="91"/>
<wire x1="458.47" y1="27.94" x2="458.47" y2="2.54" width="0.1524" layer="91"/>
<junction x="458.47" y="2.54"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="VDD"/>
<wire x1="596.9" y1="40.64" x2="600.71" y2="40.64" width="0.1524" layer="91"/>
<label x="600.71" y="40.64" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="I2C-SDA" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="3"/>
<wire x1="824.23" y1="118.11" x2="814.07" y2="118.11" width="0.1524" layer="91"/>
<label x="814.07" y="118.11" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="I2C-SCL" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="5"/>
<wire x1="824.23" y1="115.57" x2="814.07" y2="115.57" width="0.1524" layer="91"/>
<label x="814.07" y="115.57" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DEMUX_IO" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="40"/>
<wire x1="831.85" y1="72.39" x2="842.01" y2="72.39" width="0.1524" layer="91"/>
<label x="842.01" y="72.39" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="O/I"/>
<wire x1="571.5" y1="40.64" x2="565.15" y2="40.64" width="0.1524" layer="91"/>
<label x="565.15" y="40.64" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SEC_B_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="26"/>
<wire x1="831.85" y1="90.17" x2="842.01" y2="90.17" width="0.1524" layer="91"/>
<label x="842.01" y="90.17" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="737.87" y1="200.66" x2="735.33" y2="200.66" width="0.1524" layer="91"/>
<label x="735.33" y="200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="SEC_A_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="24"/>
<wire x1="831.85" y1="92.71" x2="842.01" y2="92.71" width="0.1524" layer="91"/>
<label x="842.01" y="92.71" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="615.95" y1="200.66" x2="613.41" y2="200.66" width="0.1524" layer="91"/>
<label x="613.41" y="200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-21" gate="G$1" pin="1"/>
</segment>
</net>
<net name="MIN_B_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="22"/>
<wire x1="831.85" y1="95.25" x2="842.01" y2="95.25" width="0.1524" layer="91"/>
<label x="842.01" y="95.25" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="492.76" y1="200.66" x2="490.22" y2="200.66" width="0.1524" layer="91"/>
<label x="490.22" y="200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="MIN_A_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="18"/>
<wire x1="831.85" y1="100.33" x2="842.01" y2="100.33" width="0.1524" layer="91"/>
<label x="842.01" y="100.33" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="372.11" y1="200.66" x2="369.57" y2="200.66" width="0.1524" layer="91"/>
<label x="369.57" y="200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="HOUR_B_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="16"/>
<wire x1="831.85" y1="102.87" x2="842.01" y2="102.87" width="0.1524" layer="91"/>
<label x="842.01" y="102.87" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="252.73" y1="200.66" x2="250.19" y2="200.66" width="0.1524" layer="91"/>
<label x="250.19" y="200.66" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="HOUR_A_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="12"/>
<wire x1="831.85" y1="107.95" x2="842.01" y2="107.95" width="0.1524" layer="91"/>
<label x="842.01" y="107.95" size="1.778" layer="95" xref="yes"/>
<label x="842.01" y="107.95" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="132.08" y1="201.93" x2="129.54" y2="201.93" width="0.1524" layer="91"/>
<label x="129.54" y="201.93" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="4"/>
<wire x1="831.85" y1="118.11" x2="842.01" y2="118.11" width="0.1524" layer="91"/>
<label x="842.01" y="118.11" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="2"/>
<wire x1="831.85" y1="120.65" x2="842.01" y2="120.65" width="0.1524" layer="91"/>
<label x="842.01" y="120.65" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="DAY_B_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="21"/>
<wire x1="824.23" y1="95.25" x2="814.07" y2="95.25" width="0.1524" layer="91"/>
<label x="814.07" y="95.25" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="193.04" y1="104.14" x2="189.23" y2="104.14" width="0.1524" layer="91"/>
<label x="189.23" y="104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-36" gate="G$1" pin="1"/>
</segment>
</net>
<net name="MONTH_A_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="23"/>
<wire x1="824.23" y1="92.71" x2="814.07" y2="92.71" width="0.1524" layer="91"/>
<label x="814.07" y="92.71" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="269.24" y1="104.14" x2="265.43" y2="104.14" width="0.1524" layer="91"/>
<label x="265.43" y="104.14" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-42" gate="G$1" pin="1"/>
</segment>
</net>
<net name="MONTH_B_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="29"/>
<wire x1="824.23" y1="85.09" x2="814.07" y2="85.09" width="0.1524" layer="91"/>
<label x="814.07" y="85.09" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="349.25" y1="102.87" x2="345.44" y2="102.87" width="0.1524" layer="91"/>
<label x="345.44" y="102.87" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-48" gate="G$1" pin="1"/>
</segment>
</net>
<net name="YEAR_A_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="31"/>
<wire x1="824.23" y1="82.55" x2="814.07" y2="82.55" width="0.1524" layer="91"/>
<label x="814.07" y="82.55" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="429.26" y1="102.87" x2="425.45" y2="102.87" width="0.1524" layer="91"/>
<label x="425.45" y="102.87" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-54" gate="G$1" pin="1"/>
</segment>
</net>
<net name="YEAR_B_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="33"/>
<wire x1="824.23" y1="80.01" x2="814.07" y2="80.01" width="0.1524" layer="91"/>
<label x="814.07" y="80.01" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="506.73" y1="102.87" x2="502.92" y2="102.87" width="0.1524" layer="91"/>
<label x="502.92" y="102.87" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-60" gate="G$1" pin="1"/>
</segment>
</net>
<net name="YEAR_C_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="35"/>
<wire x1="824.23" y1="77.47" x2="814.07" y2="77.47" width="0.1524" layer="91"/>
<label x="814.07" y="77.47" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="588.01" y1="102.87" x2="584.2" y2="102.87" width="0.1524" layer="91"/>
<label x="584.2" y="102.87" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-66" gate="G$1" pin="1"/>
</segment>
</net>
<net name="YEAR_D_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="37"/>
<wire x1="824.23" y1="74.93" x2="814.07" y2="74.93" width="0.1524" layer="91"/>
<label x="814.07" y="74.93" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="665.48" y1="102.87" x2="661.67" y2="102.87" width="0.1524" layer="91"/>
<label x="661.67" y="102.87" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-72" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TEMP_A_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="8"/>
<wire x1="831.85" y1="113.03" x2="842.01" y2="113.03" width="0.1524" layer="91"/>
<label x="842.01" y="113.03" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="342.9" y1="2.54" x2="339.09" y2="2.54" width="0.1524" layer="91"/>
<label x="339.09" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-78" gate="G$1" pin="1"/>
</segment>
</net>
<net name="TEMP_B_LD" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="10"/>
<wire x1="831.85" y1="110.49" x2="842.01" y2="110.49" width="0.1524" layer="91"/>
<label x="842.01" y="110.49" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="421.64" y1="2.54" x2="417.83" y2="2.54" width="0.1524" layer="91"/>
<label x="417.83" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="1K-84" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="LD"/>
<wire x1="146.05" y1="201.93" x2="142.24" y2="201.93" width="0.1524" layer="91"/>
<pinref part="1K-1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="IB"/>
<wire x1="146.05" y1="199.39" x2="142.24" y2="199.39" width="0.1524" layer="91"/>
<pinref part="1K-2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="IC"/>
<wire x1="146.05" y1="196.85" x2="142.24" y2="196.85" width="0.1524" layer="91"/>
<pinref part="1K-3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="ID"/>
<wire x1="146.05" y1="194.31" x2="142.24" y2="194.31" width="0.1524" layer="91"/>
<pinref part="1K-4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="IA"/>
<wire x1="146.05" y1="191.77" x2="142.24" y2="191.77" width="0.1524" layer="91"/>
<pinref part="1K-5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="217.17" y1="199.39" x2="217.17" y2="231.14" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_A" gate="G$1" pin="F"/>
<wire x1="217.17" y1="231.14" x2="165.1" y2="231.14" width="0.1524" layer="91"/>
<pinref part="470-1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="LCD_HOUR_A" gate="G$1" pin="G"/>
<wire x1="165.1" y1="228.6" x2="218.44" y2="228.6" width="0.1524" layer="91"/>
<wire x1="218.44" y1="228.6" x2="218.44" y2="196.85" width="0.1524" layer="91"/>
<wire x1="218.44" y1="196.85" x2="217.17" y2="196.85" width="0.1524" layer="91"/>
<pinref part="470-2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="217.17" y1="194.31" x2="219.71" y2="194.31" width="0.1524" layer="91"/>
<wire x1="219.71" y1="194.31" x2="219.71" y2="217.17" width="0.1524" layer="91"/>
<wire x1="219.71" y1="217.17" x2="148.59" y2="217.17" width="0.1524" layer="91"/>
<wire x1="148.59" y1="217.17" x2="148.59" y2="220.98" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_A" gate="G$1" pin="E"/>
<wire x1="148.59" y1="220.98" x2="149.86" y2="220.98" width="0.1524" layer="91"/>
<pinref part="470-3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="LCD_HOUR_A" gate="G$1" pin="D"/>
<wire x1="149.86" y1="223.52" x2="147.32" y2="223.52" width="0.1524" layer="91"/>
<wire x1="147.32" y1="223.52" x2="147.32" y2="215.9" width="0.1524" layer="91"/>
<wire x1="147.32" y1="215.9" x2="220.98" y2="215.9" width="0.1524" layer="91"/>
<wire x1="220.98" y1="215.9" x2="220.98" y2="191.77" width="0.1524" layer="91"/>
<wire x1="220.98" y1="191.77" x2="217.17" y2="191.77" width="0.1524" layer="91"/>
<pinref part="470-4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="217.17" y1="189.23" x2="222.25" y2="189.23" width="0.1524" layer="91"/>
<wire x1="222.25" y1="189.23" x2="222.25" y2="214.63" width="0.1524" layer="91"/>
<wire x1="222.25" y1="214.63" x2="146.05" y2="214.63" width="0.1524" layer="91"/>
<wire x1="146.05" y1="214.63" x2="146.05" y2="226.06" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_A" gate="G$1" pin="C"/>
<wire x1="146.05" y1="226.06" x2="149.86" y2="226.06" width="0.1524" layer="91"/>
<pinref part="470-5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="LCD_HOUR_A" gate="G$1" pin="B"/>
<wire x1="149.86" y1="228.6" x2="144.78" y2="228.6" width="0.1524" layer="91"/>
<wire x1="144.78" y1="228.6" x2="144.78" y2="213.36" width="0.1524" layer="91"/>
<wire x1="144.78" y1="213.36" x2="223.52" y2="213.36" width="0.1524" layer="91"/>
<wire x1="223.52" y1="213.36" x2="223.52" y2="186.69" width="0.1524" layer="91"/>
<wire x1="223.52" y1="186.69" x2="217.17" y2="186.69" width="0.1524" layer="91"/>
<pinref part="470-6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="217.17" y1="184.15" x2="224.79" y2="184.15" width="0.1524" layer="91"/>
<wire x1="224.79" y1="184.15" x2="224.79" y2="212.09" width="0.1524" layer="91"/>
<wire x1="224.79" y1="212.09" x2="143.51" y2="212.09" width="0.1524" layer="91"/>
<wire x1="143.51" y1="212.09" x2="143.51" y2="231.14" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_A" gate="G$1" pin="A"/>
<wire x1="143.51" y1="231.14" x2="149.86" y2="231.14" width="0.1524" layer="91"/>
<pinref part="470-7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="LD"/>
<wire x1="266.7" y1="200.66" x2="262.89" y2="200.66" width="0.1524" layer="91"/>
<pinref part="1K-6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="IB"/>
<wire x1="266.7" y1="198.12" x2="262.89" y2="198.12" width="0.1524" layer="91"/>
<pinref part="1K-7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="IC"/>
<wire x1="266.7" y1="195.58" x2="262.89" y2="195.58" width="0.1524" layer="91"/>
<pinref part="1K-8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="ID"/>
<wire x1="266.7" y1="193.04" x2="262.89" y2="193.04" width="0.1524" layer="91"/>
<pinref part="1K-9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="IA"/>
<wire x1="266.7" y1="190.5" x2="262.89" y2="190.5" width="0.1524" layer="91"/>
<pinref part="1K-10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<wire x1="337.82" y1="198.12" x2="337.82" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_B" gate="G$1" pin="F"/>
<wire x1="337.82" y1="229.87" x2="285.75" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$66" class="0">
<segment>
<pinref part="LCD_HOUR_B" gate="G$1" pin="G"/>
<wire x1="285.75" y1="227.33" x2="339.09" y2="227.33" width="0.1524" layer="91"/>
<wire x1="339.09" y1="227.33" x2="339.09" y2="195.58" width="0.1524" layer="91"/>
<wire x1="339.09" y1="195.58" x2="337.82" y2="195.58" width="0.1524" layer="91"/>
<pinref part="470-9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$67" class="0">
<segment>
<wire x1="337.82" y1="193.04" x2="340.36" y2="193.04" width="0.1524" layer="91"/>
<wire x1="340.36" y1="193.04" x2="340.36" y2="215.9" width="0.1524" layer="91"/>
<wire x1="340.36" y1="215.9" x2="269.24" y2="215.9" width="0.1524" layer="91"/>
<wire x1="269.24" y1="215.9" x2="269.24" y2="219.71" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_B" gate="G$1" pin="E"/>
<wire x1="269.24" y1="219.71" x2="270.51" y2="219.71" width="0.1524" layer="91"/>
<pinref part="470-10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$68" class="0">
<segment>
<pinref part="LCD_HOUR_B" gate="G$1" pin="D"/>
<wire x1="270.51" y1="222.25" x2="267.97" y2="222.25" width="0.1524" layer="91"/>
<wire x1="267.97" y1="222.25" x2="267.97" y2="214.63" width="0.1524" layer="91"/>
<wire x1="267.97" y1="214.63" x2="341.63" y2="214.63" width="0.1524" layer="91"/>
<wire x1="341.63" y1="214.63" x2="341.63" y2="190.5" width="0.1524" layer="91"/>
<wire x1="341.63" y1="190.5" x2="337.82" y2="190.5" width="0.1524" layer="91"/>
<pinref part="470-11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<wire x1="337.82" y1="187.96" x2="342.9" y2="187.96" width="0.1524" layer="91"/>
<wire x1="342.9" y1="187.96" x2="342.9" y2="213.36" width="0.1524" layer="91"/>
<wire x1="342.9" y1="213.36" x2="266.7" y2="213.36" width="0.1524" layer="91"/>
<wire x1="266.7" y1="213.36" x2="266.7" y2="224.79" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_B" gate="G$1" pin="C"/>
<wire x1="266.7" y1="224.79" x2="270.51" y2="224.79" width="0.1524" layer="91"/>
<pinref part="470-12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="LCD_HOUR_B" gate="G$1" pin="B"/>
<wire x1="270.51" y1="227.33" x2="265.43" y2="227.33" width="0.1524" layer="91"/>
<wire x1="265.43" y1="227.33" x2="265.43" y2="212.09" width="0.1524" layer="91"/>
<wire x1="265.43" y1="212.09" x2="344.17" y2="212.09" width="0.1524" layer="91"/>
<wire x1="344.17" y1="212.09" x2="344.17" y2="185.42" width="0.1524" layer="91"/>
<wire x1="344.17" y1="185.42" x2="337.82" y2="185.42" width="0.1524" layer="91"/>
<pinref part="470-13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="337.82" y1="182.88" x2="345.44" y2="182.88" width="0.1524" layer="91"/>
<wire x1="345.44" y1="182.88" x2="345.44" y2="210.82" width="0.1524" layer="91"/>
<wire x1="345.44" y1="210.82" x2="264.16" y2="210.82" width="0.1524" layer="91"/>
<wire x1="264.16" y1="210.82" x2="264.16" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_B" gate="G$1" pin="A"/>
<wire x1="264.16" y1="229.87" x2="270.51" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="12V" class="0">
<segment>
<pinref part="SUPPLY1" gate="G$1" pin="VDD"/>
<wire x1="120.65" y1="231.14" x2="120.65" y2="226.06" width="0.1524" layer="91"/>
<label x="120.65" y="226.06" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="120.65" y1="209.55" x2="229.87" y2="209.55" width="0.1524" layer="91"/>
<wire x1="229.87" y1="209.55" x2="229.87" y2="181.61" width="0.1524" layer="91"/>
<pinref part="2003_HOUR_A" gate="A" pin="CD+"/>
<wire x1="229.87" y1="181.61" x2="207.01" y2="181.61" width="0.1524" layer="91"/>
<wire x1="180.34" y1="223.52" x2="229.87" y2="223.52" width="0.1524" layer="91"/>
<wire x1="229.87" y1="223.52" x2="229.87" y2="209.55" width="0.1524" layer="91"/>
<junction x="229.87" y="209.55"/>
<label x="120.65" y="209.55" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="180.34" y1="223.52" x2="180.34" y2="220.98" width="0.1524" layer="91"/>
<pinref part="LCD_HOUR_A" gate="G$1" pin="VDD2"/>
<wire x1="180.34" y1="220.98" x2="165.1" y2="220.98" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="241.3" y1="208.28" x2="350.52" y2="208.28" width="0.1524" layer="91"/>
<wire x1="350.52" y1="208.28" x2="350.52" y2="180.34" width="0.1524" layer="91"/>
<pinref part="2003_HOUR_B" gate="A" pin="CD+"/>
<wire x1="350.52" y1="180.34" x2="327.66" y2="180.34" width="0.1524" layer="91"/>
<label x="241.3" y="208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="350.52" y1="208.28" x2="350.52" y2="219.71" width="0.1524" layer="91"/>
<wire x1="350.52" y1="219.71" x2="294.64" y2="219.71" width="0.1524" layer="91"/>
<junction x="350.52" y="208.28"/>
<pinref part="LCD_HOUR_B" gate="G$1" pin="VDD1"/>
<wire x1="285.75" y1="222.25" x2="294.64" y2="222.25" width="0.1524" layer="91"/>
<wire x1="294.64" y1="222.25" x2="294.64" y2="219.71" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="360.68" y1="208.28" x2="469.9" y2="208.28" width="0.1524" layer="91"/>
<wire x1="469.9" y1="208.28" x2="469.9" y2="180.34" width="0.1524" layer="91"/>
<pinref part="2003_MIN_A" gate="A" pin="CD+"/>
<wire x1="469.9" y1="180.34" x2="447.04" y2="180.34" width="0.1524" layer="91"/>
<label x="360.68" y="208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="469.9" y1="208.28" x2="469.9" y2="219.71" width="0.1524" layer="91"/>
<wire x1="469.9" y1="219.71" x2="416.56" y2="219.71" width="0.1524" layer="91"/>
<junction x="469.9" y="208.28"/>
<pinref part="LCD_MIN_A" gate="G$1" pin="VDD1"/>
<wire x1="405.13" y1="222.25" x2="416.56" y2="222.25" width="0.1524" layer="91"/>
<wire x1="416.56" y1="222.25" x2="416.56" y2="219.71" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="481.33" y1="208.28" x2="590.55" y2="208.28" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_B" gate="G$1" pin="VDD1"/>
<wire x1="525.78" y1="222.25" x2="590.55" y2="222.25" width="0.1524" layer="91"/>
<wire x1="590.55" y1="222.25" x2="590.55" y2="208.28" width="0.1524" layer="91"/>
<label x="481.33" y="208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="2003_MIN_B" gate="A" pin="CD+"/>
<wire x1="590.55" y1="208.28" x2="590.55" y2="180.34" width="0.1524" layer="91"/>
<wire x1="590.55" y1="180.34" x2="567.69" y2="180.34" width="0.1524" layer="91"/>
<junction x="590.55" y="208.28"/>
</segment>
<segment>
<wire x1="604.52" y1="208.28" x2="713.74" y2="208.28" width="0.1524" layer="91"/>
<wire x1="713.74" y1="208.28" x2="713.74" y2="180.34" width="0.1524" layer="91"/>
<pinref part="2003_SEC_A" gate="A" pin="CD+"/>
<wire x1="713.74" y1="180.34" x2="690.88" y2="180.34" width="0.1524" layer="91"/>
<label x="604.52" y="208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="LCD_SEC_A" gate="G$1" pin="VDD1"/>
<wire x1="648.97" y1="222.25" x2="713.74" y2="222.25" width="0.1524" layer="91"/>
<wire x1="713.74" y1="222.25" x2="713.74" y2="208.28" width="0.1524" layer="91"/>
<junction x="713.74" y="208.28"/>
</segment>
<segment>
<wire x1="726.44" y1="208.28" x2="835.66" y2="208.28" width="0.1524" layer="91"/>
<wire x1="835.66" y1="208.28" x2="835.66" y2="180.34" width="0.1524" layer="91"/>
<pinref part="2003_SEC_B" gate="A" pin="CD+"/>
<wire x1="835.66" y1="180.34" x2="812.8" y2="180.34" width="0.1524" layer="91"/>
<label x="726.44" y="208.28" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="LCD_SEC_B" gate="G$1" pin="VDD2"/>
<wire x1="835.66" y1="208.28" x2="835.66" y2="219.71" width="0.1524" layer="91"/>
<wire x1="835.66" y1="219.71" x2="770.89" y2="219.71" width="0.1524" layer="91"/>
<junction x="835.66" y="208.28"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<wire x1="853.44" y1="147.32" x2="855.98" y2="147.32" width="0.1524" layer="91"/>
<label x="855.98" y="147.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="470-43" gate="G$1" pin="2"/>
<wire x1="368.3" y1="311.15" x2="368.3" y2="314.96" width="0.1524" layer="91"/>
<label x="368.3" y="314.96" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="470-44" gate="G$1" pin="2"/>
<wire x1="441.96" y1="311.15" x2="441.96" y2="314.96" width="0.1524" layer="91"/>
<label x="441.96" y="314.96" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="LD"/>
<wire x1="386.08" y1="200.66" x2="382.27" y2="200.66" width="0.1524" layer="91"/>
<pinref part="1K-11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="IB"/>
<wire x1="386.08" y1="198.12" x2="382.27" y2="198.12" width="0.1524" layer="91"/>
<pinref part="1K-12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="IC"/>
<wire x1="386.08" y1="195.58" x2="382.27" y2="195.58" width="0.1524" layer="91"/>
<pinref part="1K-13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="ID"/>
<wire x1="386.08" y1="193.04" x2="382.27" y2="193.04" width="0.1524" layer="91"/>
<pinref part="1K-14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$89" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="IA"/>
<wire x1="386.08" y1="190.5" x2="382.27" y2="190.5" width="0.1524" layer="91"/>
<pinref part="1K-15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$97" class="0">
<segment>
<wire x1="457.2" y1="198.12" x2="457.2" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_A" gate="G$1" pin="F"/>
<wire x1="457.2" y1="229.87" x2="405.13" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="LCD_MIN_A" gate="G$1" pin="G"/>
<wire x1="405.13" y1="227.33" x2="458.47" y2="227.33" width="0.1524" layer="91"/>
<wire x1="458.47" y1="227.33" x2="458.47" y2="195.58" width="0.1524" layer="91"/>
<wire x1="458.47" y1="195.58" x2="457.2" y2="195.58" width="0.1524" layer="91"/>
<pinref part="470-16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<wire x1="457.2" y1="193.04" x2="459.74" y2="193.04" width="0.1524" layer="91"/>
<wire x1="459.74" y1="193.04" x2="459.74" y2="215.9" width="0.1524" layer="91"/>
<wire x1="459.74" y1="215.9" x2="388.62" y2="215.9" width="0.1524" layer="91"/>
<wire x1="388.62" y1="215.9" x2="388.62" y2="219.71" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_A" gate="G$1" pin="E"/>
<wire x1="388.62" y1="219.71" x2="389.89" y2="219.71" width="0.1524" layer="91"/>
<pinref part="470-17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="LCD_MIN_A" gate="G$1" pin="D"/>
<wire x1="389.89" y1="222.25" x2="387.35" y2="222.25" width="0.1524" layer="91"/>
<wire x1="387.35" y1="222.25" x2="387.35" y2="214.63" width="0.1524" layer="91"/>
<wire x1="387.35" y1="214.63" x2="461.01" y2="214.63" width="0.1524" layer="91"/>
<wire x1="461.01" y1="214.63" x2="461.01" y2="190.5" width="0.1524" layer="91"/>
<wire x1="461.01" y1="190.5" x2="457.2" y2="190.5" width="0.1524" layer="91"/>
<pinref part="470-18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<wire x1="457.2" y1="187.96" x2="462.28" y2="187.96" width="0.1524" layer="91"/>
<wire x1="462.28" y1="187.96" x2="462.28" y2="213.36" width="0.1524" layer="91"/>
<wire x1="462.28" y1="213.36" x2="386.08" y2="213.36" width="0.1524" layer="91"/>
<wire x1="386.08" y1="213.36" x2="386.08" y2="224.79" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_A" gate="G$1" pin="C"/>
<wire x1="386.08" y1="224.79" x2="389.89" y2="224.79" width="0.1524" layer="91"/>
<pinref part="470-19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="LCD_MIN_A" gate="G$1" pin="B"/>
<wire x1="389.89" y1="227.33" x2="384.81" y2="227.33" width="0.1524" layer="91"/>
<wire x1="384.81" y1="227.33" x2="384.81" y2="212.09" width="0.1524" layer="91"/>
<wire x1="384.81" y1="212.09" x2="463.55" y2="212.09" width="0.1524" layer="91"/>
<wire x1="463.55" y1="212.09" x2="463.55" y2="185.42" width="0.1524" layer="91"/>
<wire x1="463.55" y1="185.42" x2="457.2" y2="185.42" width="0.1524" layer="91"/>
<pinref part="470-20" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$103" class="0">
<segment>
<wire x1="457.2" y1="182.88" x2="464.82" y2="182.88" width="0.1524" layer="91"/>
<wire x1="464.82" y1="182.88" x2="464.82" y2="210.82" width="0.1524" layer="91"/>
<wire x1="464.82" y1="210.82" x2="383.54" y2="210.82" width="0.1524" layer="91"/>
<wire x1="383.54" y1="210.82" x2="383.54" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_A" gate="G$1" pin="A"/>
<wire x1="383.54" y1="229.87" x2="389.89" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="LD"/>
<wire x1="506.73" y1="200.66" x2="502.92" y2="200.66" width="0.1524" layer="91"/>
<pinref part="1K-16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="IB"/>
<wire x1="506.73" y1="198.12" x2="502.92" y2="198.12" width="0.1524" layer="91"/>
<pinref part="1K-17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="IC"/>
<wire x1="506.73" y1="195.58" x2="502.92" y2="195.58" width="0.1524" layer="91"/>
<pinref part="1K-18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$121" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="ID"/>
<wire x1="506.73" y1="193.04" x2="502.92" y2="193.04" width="0.1524" layer="91"/>
<pinref part="1K-19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$122" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="IA"/>
<wire x1="506.73" y1="190.5" x2="502.92" y2="190.5" width="0.1524" layer="91"/>
<pinref part="1K-20" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$130" class="0">
<segment>
<wire x1="577.85" y1="198.12" x2="577.85" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_B" gate="G$1" pin="F"/>
<wire x1="577.85" y1="229.87" x2="525.78" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$131" class="0">
<segment>
<pinref part="LCD_MIN_B" gate="G$1" pin="G"/>
<wire x1="525.78" y1="227.33" x2="579.12" y2="227.33" width="0.1524" layer="91"/>
<wire x1="579.12" y1="227.33" x2="579.12" y2="195.58" width="0.1524" layer="91"/>
<wire x1="579.12" y1="195.58" x2="577.85" y2="195.58" width="0.1524" layer="91"/>
<pinref part="470-23" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<wire x1="577.85" y1="193.04" x2="580.39" y2="193.04" width="0.1524" layer="91"/>
<wire x1="580.39" y1="193.04" x2="580.39" y2="215.9" width="0.1524" layer="91"/>
<wire x1="580.39" y1="215.9" x2="509.27" y2="215.9" width="0.1524" layer="91"/>
<wire x1="509.27" y1="215.9" x2="509.27" y2="219.71" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_B" gate="G$1" pin="E"/>
<wire x1="509.27" y1="219.71" x2="510.54" y2="219.71" width="0.1524" layer="91"/>
<pinref part="470-24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<pinref part="LCD_MIN_B" gate="G$1" pin="D"/>
<wire x1="510.54" y1="222.25" x2="508" y2="222.25" width="0.1524" layer="91"/>
<wire x1="508" y1="222.25" x2="508" y2="214.63" width="0.1524" layer="91"/>
<wire x1="508" y1="214.63" x2="581.66" y2="214.63" width="0.1524" layer="91"/>
<wire x1="581.66" y1="214.63" x2="581.66" y2="190.5" width="0.1524" layer="91"/>
<wire x1="581.66" y1="190.5" x2="577.85" y2="190.5" width="0.1524" layer="91"/>
<pinref part="470-25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<wire x1="577.85" y1="187.96" x2="582.93" y2="187.96" width="0.1524" layer="91"/>
<wire x1="582.93" y1="187.96" x2="582.93" y2="213.36" width="0.1524" layer="91"/>
<wire x1="582.93" y1="213.36" x2="506.73" y2="213.36" width="0.1524" layer="91"/>
<wire x1="506.73" y1="213.36" x2="506.73" y2="224.79" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_B" gate="G$1" pin="C"/>
<wire x1="506.73" y1="224.79" x2="510.54" y2="224.79" width="0.1524" layer="91"/>
<pinref part="470-26" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<pinref part="LCD_MIN_B" gate="G$1" pin="B"/>
<wire x1="510.54" y1="227.33" x2="505.46" y2="227.33" width="0.1524" layer="91"/>
<wire x1="505.46" y1="227.33" x2="505.46" y2="212.09" width="0.1524" layer="91"/>
<wire x1="505.46" y1="212.09" x2="584.2" y2="212.09" width="0.1524" layer="91"/>
<wire x1="584.2" y1="212.09" x2="584.2" y2="185.42" width="0.1524" layer="91"/>
<wire x1="584.2" y1="185.42" x2="577.85" y2="185.42" width="0.1524" layer="91"/>
<pinref part="470-27" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$136" class="0">
<segment>
<wire x1="577.85" y1="182.88" x2="585.47" y2="182.88" width="0.1524" layer="91"/>
<wire x1="585.47" y1="182.88" x2="585.47" y2="210.82" width="0.1524" layer="91"/>
<wire x1="585.47" y1="210.82" x2="504.19" y2="210.82" width="0.1524" layer="91"/>
<wire x1="504.19" y1="210.82" x2="504.19" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_MIN_B" gate="G$1" pin="A"/>
<wire x1="504.19" y1="229.87" x2="510.54" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-28" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$151" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="LD"/>
<wire x1="629.92" y1="200.66" x2="626.11" y2="200.66" width="0.1524" layer="91"/>
<pinref part="1K-21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$152" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="IB"/>
<wire x1="629.92" y1="198.12" x2="626.11" y2="198.12" width="0.1524" layer="91"/>
<pinref part="1K-22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$153" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="IC"/>
<wire x1="629.92" y1="195.58" x2="626.11" y2="195.58" width="0.1524" layer="91"/>
<pinref part="1K-23" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$154" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="ID"/>
<wire x1="629.92" y1="193.04" x2="626.11" y2="193.04" width="0.1524" layer="91"/>
<pinref part="1K-24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$155" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="IA"/>
<wire x1="629.92" y1="190.5" x2="626.11" y2="190.5" width="0.1524" layer="91"/>
<pinref part="1K-25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$163" class="0">
<segment>
<wire x1="701.04" y1="198.12" x2="701.04" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_SEC_A" gate="G$1" pin="F"/>
<wire x1="701.04" y1="229.87" x2="648.97" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-29" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$164" class="0">
<segment>
<pinref part="LCD_SEC_A" gate="G$1" pin="G"/>
<wire x1="648.97" y1="227.33" x2="702.31" y2="227.33" width="0.1524" layer="91"/>
<wire x1="702.31" y1="227.33" x2="702.31" y2="195.58" width="0.1524" layer="91"/>
<wire x1="702.31" y1="195.58" x2="701.04" y2="195.58" width="0.1524" layer="91"/>
<pinref part="470-30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$165" class="0">
<segment>
<wire x1="701.04" y1="193.04" x2="703.58" y2="193.04" width="0.1524" layer="91"/>
<wire x1="703.58" y1="193.04" x2="703.58" y2="215.9" width="0.1524" layer="91"/>
<wire x1="703.58" y1="215.9" x2="632.46" y2="215.9" width="0.1524" layer="91"/>
<wire x1="632.46" y1="215.9" x2="632.46" y2="219.71" width="0.1524" layer="91"/>
<pinref part="LCD_SEC_A" gate="G$1" pin="E"/>
<wire x1="632.46" y1="219.71" x2="633.73" y2="219.71" width="0.1524" layer="91"/>
<pinref part="470-31" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$166" class="0">
<segment>
<pinref part="LCD_SEC_A" gate="G$1" pin="D"/>
<wire x1="633.73" y1="222.25" x2="631.19" y2="222.25" width="0.1524" layer="91"/>
<wire x1="631.19" y1="222.25" x2="631.19" y2="214.63" width="0.1524" layer="91"/>
<wire x1="631.19" y1="214.63" x2="704.85" y2="214.63" width="0.1524" layer="91"/>
<wire x1="704.85" y1="214.63" x2="704.85" y2="190.5" width="0.1524" layer="91"/>
<wire x1="704.85" y1="190.5" x2="701.04" y2="190.5" width="0.1524" layer="91"/>
<pinref part="470-32" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$167" class="0">
<segment>
<wire x1="701.04" y1="187.96" x2="706.12" y2="187.96" width="0.1524" layer="91"/>
<wire x1="706.12" y1="187.96" x2="706.12" y2="213.36" width="0.1524" layer="91"/>
<wire x1="706.12" y1="213.36" x2="629.92" y2="213.36" width="0.1524" layer="91"/>
<wire x1="629.92" y1="213.36" x2="629.92" y2="224.79" width="0.1524" layer="91"/>
<pinref part="LCD_SEC_A" gate="G$1" pin="C"/>
<wire x1="629.92" y1="224.79" x2="633.73" y2="224.79" width="0.1524" layer="91"/>
<pinref part="470-33" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$168" class="0">
<segment>
<pinref part="LCD_SEC_A" gate="G$1" pin="B"/>
<wire x1="633.73" y1="227.33" x2="628.65" y2="227.33" width="0.1524" layer="91"/>
<wire x1="628.65" y1="227.33" x2="628.65" y2="212.09" width="0.1524" layer="91"/>
<wire x1="628.65" y1="212.09" x2="707.39" y2="212.09" width="0.1524" layer="91"/>
<wire x1="707.39" y1="212.09" x2="707.39" y2="185.42" width="0.1524" layer="91"/>
<wire x1="707.39" y1="185.42" x2="701.04" y2="185.42" width="0.1524" layer="91"/>
<pinref part="470-34" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$169" class="0">
<segment>
<wire x1="701.04" y1="182.88" x2="708.66" y2="182.88" width="0.1524" layer="91"/>
<wire x1="708.66" y1="182.88" x2="708.66" y2="210.82" width="0.1524" layer="91"/>
<wire x1="708.66" y1="210.82" x2="627.38" y2="210.82" width="0.1524" layer="91"/>
<wire x1="627.38" y1="210.82" x2="627.38" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_SEC_A" gate="G$1" pin="A"/>
<wire x1="627.38" y1="229.87" x2="633.73" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$184" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="LD"/>
<wire x1="751.84" y1="200.66" x2="748.03" y2="200.66" width="0.1524" layer="91"/>
<pinref part="1K-26" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$185" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="IB"/>
<wire x1="751.84" y1="198.12" x2="748.03" y2="198.12" width="0.1524" layer="91"/>
<pinref part="1K-27" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$186" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="IC"/>
<wire x1="751.84" y1="195.58" x2="748.03" y2="195.58" width="0.1524" layer="91"/>
<pinref part="1K-28" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$187" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="ID"/>
<wire x1="751.84" y1="193.04" x2="748.03" y2="193.04" width="0.1524" layer="91"/>
<pinref part="1K-29" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$188" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="IA"/>
<wire x1="751.84" y1="190.5" x2="748.03" y2="190.5" width="0.1524" layer="91"/>
<pinref part="1K-30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$196" class="0">
<segment>
<wire x1="822.96" y1="198.12" x2="822.96" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_SEC_B" gate="G$1" pin="F"/>
<wire x1="822.96" y1="229.87" x2="770.89" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-36" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$197" class="0">
<segment>
<pinref part="LCD_SEC_B" gate="G$1" pin="G"/>
<wire x1="770.89" y1="227.33" x2="824.23" y2="227.33" width="0.1524" layer="91"/>
<wire x1="824.23" y1="227.33" x2="824.23" y2="195.58" width="0.1524" layer="91"/>
<wire x1="824.23" y1="195.58" x2="822.96" y2="195.58" width="0.1524" layer="91"/>
<pinref part="470-37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$198" class="0">
<segment>
<wire x1="822.96" y1="193.04" x2="825.5" y2="193.04" width="0.1524" layer="91"/>
<wire x1="825.5" y1="193.04" x2="825.5" y2="215.9" width="0.1524" layer="91"/>
<wire x1="825.5" y1="215.9" x2="754.38" y2="215.9" width="0.1524" layer="91"/>
<wire x1="754.38" y1="215.9" x2="754.38" y2="219.71" width="0.1524" layer="91"/>
<pinref part="LCD_SEC_B" gate="G$1" pin="E"/>
<wire x1="754.38" y1="219.71" x2="755.65" y2="219.71" width="0.1524" layer="91"/>
<pinref part="470-38" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$199" class="0">
<segment>
<pinref part="LCD_SEC_B" gate="G$1" pin="D"/>
<wire x1="755.65" y1="222.25" x2="753.11" y2="222.25" width="0.1524" layer="91"/>
<wire x1="753.11" y1="222.25" x2="753.11" y2="214.63" width="0.1524" layer="91"/>
<wire x1="753.11" y1="214.63" x2="826.77" y2="214.63" width="0.1524" layer="91"/>
<wire x1="826.77" y1="214.63" x2="826.77" y2="190.5" width="0.1524" layer="91"/>
<wire x1="826.77" y1="190.5" x2="822.96" y2="190.5" width="0.1524" layer="91"/>
<pinref part="470-39" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$200" class="0">
<segment>
<wire x1="822.96" y1="187.96" x2="828.04" y2="187.96" width="0.1524" layer="91"/>
<wire x1="828.04" y1="187.96" x2="828.04" y2="213.36" width="0.1524" layer="91"/>
<wire x1="828.04" y1="213.36" x2="751.84" y2="213.36" width="0.1524" layer="91"/>
<wire x1="751.84" y1="213.36" x2="751.84" y2="224.79" width="0.1524" layer="91"/>
<pinref part="LCD_SEC_B" gate="G$1" pin="C"/>
<wire x1="751.84" y1="224.79" x2="755.65" y2="224.79" width="0.1524" layer="91"/>
<pinref part="470-40" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$201" class="0">
<segment>
<pinref part="LCD_SEC_B" gate="G$1" pin="B"/>
<wire x1="755.65" y1="227.33" x2="750.57" y2="227.33" width="0.1524" layer="91"/>
<wire x1="750.57" y1="227.33" x2="750.57" y2="212.09" width="0.1524" layer="91"/>
<wire x1="750.57" y1="212.09" x2="829.31" y2="212.09" width="0.1524" layer="91"/>
<wire x1="829.31" y1="212.09" x2="829.31" y2="185.42" width="0.1524" layer="91"/>
<wire x1="829.31" y1="185.42" x2="822.96" y2="185.42" width="0.1524" layer="91"/>
<pinref part="470-41" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$202" class="0">
<segment>
<wire x1="822.96" y1="182.88" x2="830.58" y2="182.88" width="0.1524" layer="91"/>
<wire x1="830.58" y1="182.88" x2="830.58" y2="210.82" width="0.1524" layer="91"/>
<wire x1="830.58" y1="210.82" x2="749.3" y2="210.82" width="0.1524" layer="91"/>
<wire x1="749.3" y1="210.82" x2="749.3" y2="229.87" width="0.1524" layer="91"/>
<pinref part="LCD_SEC_B" gate="G$1" pin="A"/>
<wire x1="749.3" y1="229.87" x2="755.65" y2="229.87" width="0.1524" layer="91"/>
<pinref part="470-42" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$216" class="0">
<segment>
<wire x1="163.83" y1="101.6" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="165.1" y1="101.6" x2="165.1" y2="137.16" width="0.1524" layer="91"/>
<pinref part="LCD_DAY_A" gate="G$1" pin="F"/>
<wire x1="165.1" y1="137.16" x2="149.86" y2="137.16" width="0.1524" layer="91"/>
<pinref part="100-1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$217" class="0">
<segment>
<pinref part="LCD_DAY_A" gate="G$1" pin="G"/>
<wire x1="149.86" y1="134.62" x2="166.37" y2="134.62" width="0.1524" layer="91"/>
<wire x1="166.37" y1="134.62" x2="166.37" y2="99.06" width="0.1524" layer="91"/>
<wire x1="166.37" y1="99.06" x2="163.83" y2="99.06" width="0.1524" layer="91"/>
<pinref part="100-2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$218" class="0">
<segment>
<wire x1="163.83" y1="96.52" x2="167.64" y2="96.52" width="0.1524" layer="91"/>
<wire x1="167.64" y1="96.52" x2="167.64" y2="123.19" width="0.1524" layer="91"/>
<wire x1="167.64" y1="123.19" x2="133.35" y2="123.19" width="0.1524" layer="91"/>
<wire x1="133.35" y1="123.19" x2="133.35" y2="127" width="0.1524" layer="91"/>
<pinref part="LCD_DAY_A" gate="G$1" pin="E"/>
<wire x1="133.35" y1="127" x2="134.62" y2="127" width="0.1524" layer="91"/>
<pinref part="100-3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$219" class="0">
<segment>
<pinref part="LCD_DAY_A" gate="G$1" pin="D"/>
<wire x1="134.62" y1="129.54" x2="132.08" y2="129.54" width="0.1524" layer="91"/>
<wire x1="132.08" y1="129.54" x2="132.08" y2="121.92" width="0.1524" layer="91"/>
<wire x1="132.08" y1="121.92" x2="168.91" y2="121.92" width="0.1524" layer="91"/>
<wire x1="168.91" y1="121.92" x2="168.91" y2="93.98" width="0.1524" layer="91"/>
<wire x1="168.91" y1="93.98" x2="163.83" y2="93.98" width="0.1524" layer="91"/>
<pinref part="100-4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$220" class="0">
<segment>
<wire x1="163.83" y1="91.44" x2="170.18" y2="91.44" width="0.1524" layer="91"/>
<wire x1="170.18" y1="91.44" x2="170.18" y2="120.65" width="0.1524" layer="91"/>
<wire x1="170.18" y1="120.65" x2="130.81" y2="120.65" width="0.1524" layer="91"/>
<wire x1="130.81" y1="120.65" x2="130.81" y2="132.08" width="0.1524" layer="91"/>
<pinref part="LCD_DAY_A" gate="G$1" pin="C"/>
<wire x1="130.81" y1="132.08" x2="134.62" y2="132.08" width="0.1524" layer="91"/>
<pinref part="100-5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$221" class="0">
<segment>
<pinref part="LCD_DAY_A" gate="G$1" pin="B"/>
<wire x1="134.62" y1="134.62" x2="129.54" y2="134.62" width="0.1524" layer="91"/>
<wire x1="129.54" y1="134.62" x2="129.54" y2="119.38" width="0.1524" layer="91"/>
<wire x1="129.54" y1="119.38" x2="171.45" y2="119.38" width="0.1524" layer="91"/>
<wire x1="171.45" y1="119.38" x2="171.45" y2="88.9" width="0.1524" layer="91"/>
<wire x1="171.45" y1="88.9" x2="163.83" y2="88.9" width="0.1524" layer="91"/>
<pinref part="100-6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$222" class="0">
<segment>
<wire x1="163.83" y1="86.36" x2="172.72" y2="86.36" width="0.1524" layer="91"/>
<wire x1="172.72" y1="86.36" x2="172.72" y2="118.11" width="0.1524" layer="91"/>
<wire x1="172.72" y1="118.11" x2="128.27" y2="118.11" width="0.1524" layer="91"/>
<wire x1="128.27" y1="118.11" x2="128.27" y2="137.16" width="0.1524" layer="91"/>
<pinref part="LCD_DAY_A" gate="G$1" pin="A"/>
<wire x1="128.27" y1="137.16" x2="134.62" y2="137.16" width="0.1524" layer="91"/>
<pinref part="100-7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$236" class="0">
<segment>
<wire x1="238.76" y1="101.6" x2="240.03" y2="101.6" width="0.1524" layer="91"/>
<wire x1="240.03" y1="101.6" x2="240.03" y2="137.16" width="0.1524" layer="91"/>
<pinref part="LCD_DAY_B" gate="G$1" pin="F"/>
<wire x1="240.03" y1="137.16" x2="224.79" y2="137.16" width="0.1524" layer="91"/>
<pinref part="100-8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$237" class="0">
<segment>
<pinref part="LCD_DAY_B" gate="G$1" pin="G"/>
<wire x1="224.79" y1="134.62" x2="241.3" y2="134.62" width="0.1524" layer="91"/>
<wire x1="241.3" y1="134.62" x2="241.3" y2="99.06" width="0.1524" layer="91"/>
<wire x1="241.3" y1="99.06" x2="238.76" y2="99.06" width="0.1524" layer="91"/>
<pinref part="100-9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$238" class="0">
<segment>
<wire x1="238.76" y1="96.52" x2="242.57" y2="96.52" width="0.1524" layer="91"/>
<wire x1="242.57" y1="96.52" x2="242.57" y2="123.19" width="0.1524" layer="91"/>
<wire x1="242.57" y1="123.19" x2="208.28" y2="123.19" width="0.1524" layer="91"/>
<wire x1="208.28" y1="123.19" x2="208.28" y2="127" width="0.1524" layer="91"/>
<pinref part="LCD_DAY_B" gate="G$1" pin="E"/>
<wire x1="208.28" y1="127" x2="209.55" y2="127" width="0.1524" layer="91"/>
<pinref part="100-10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$239" class="0">
<segment>
<pinref part="LCD_DAY_B" gate="G$1" pin="D"/>
<wire x1="209.55" y1="129.54" x2="207.01" y2="129.54" width="0.1524" layer="91"/>
<wire x1="207.01" y1="129.54" x2="207.01" y2="121.92" width="0.1524" layer="91"/>
<wire x1="207.01" y1="121.92" x2="243.84" y2="121.92" width="0.1524" layer="91"/>
<wire x1="243.84" y1="121.92" x2="243.84" y2="93.98" width="0.1524" layer="91"/>
<wire x1="243.84" y1="93.98" x2="238.76" y2="93.98" width="0.1524" layer="91"/>
<pinref part="100-11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$240" class="0">
<segment>
<wire x1="238.76" y1="91.44" x2="245.11" y2="91.44" width="0.1524" layer="91"/>
<wire x1="245.11" y1="91.44" x2="245.11" y2="120.65" width="0.1524" layer="91"/>
<wire x1="245.11" y1="120.65" x2="205.74" y2="120.65" width="0.1524" layer="91"/>
<wire x1="205.74" y1="120.65" x2="205.74" y2="132.08" width="0.1524" layer="91"/>
<pinref part="LCD_DAY_B" gate="G$1" pin="C"/>
<wire x1="205.74" y1="132.08" x2="209.55" y2="132.08" width="0.1524" layer="91"/>
<pinref part="100-12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$241" class="0">
<segment>
<pinref part="LCD_DAY_B" gate="G$1" pin="B"/>
<wire x1="209.55" y1="134.62" x2="204.47" y2="134.62" width="0.1524" layer="91"/>
<wire x1="204.47" y1="134.62" x2="204.47" y2="119.38" width="0.1524" layer="91"/>
<wire x1="204.47" y1="119.38" x2="246.38" y2="119.38" width="0.1524" layer="91"/>
<wire x1="246.38" y1="119.38" x2="246.38" y2="88.9" width="0.1524" layer="91"/>
<wire x1="246.38" y1="88.9" x2="238.76" y2="88.9" width="0.1524" layer="91"/>
<pinref part="100-13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$242" class="0">
<segment>
<wire x1="238.76" y1="86.36" x2="247.65" y2="86.36" width="0.1524" layer="91"/>
<wire x1="247.65" y1="86.36" x2="247.65" y2="118.11" width="0.1524" layer="91"/>
<wire x1="247.65" y1="118.11" x2="203.2" y2="118.11" width="0.1524" layer="91"/>
<wire x1="203.2" y1="118.11" x2="203.2" y2="137.16" width="0.1524" layer="91"/>
<pinref part="LCD_DAY_B" gate="G$1" pin="A"/>
<wire x1="203.2" y1="137.16" x2="209.55" y2="137.16" width="0.1524" layer="91"/>
<pinref part="100-14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$256" class="0">
<segment>
<wire x1="314.96" y1="101.6" x2="316.23" y2="101.6" width="0.1524" layer="91"/>
<wire x1="316.23" y1="101.6" x2="316.23" y2="137.16" width="0.1524" layer="91"/>
<pinref part="LCD_MONTH_A" gate="G$1" pin="F"/>
<wire x1="316.23" y1="137.16" x2="300.99" y2="137.16" width="0.1524" layer="91"/>
<pinref part="100-15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$257" class="0">
<segment>
<pinref part="LCD_MONTH_A" gate="G$1" pin="G"/>
<wire x1="300.99" y1="134.62" x2="317.5" y2="134.62" width="0.1524" layer="91"/>
<wire x1="317.5" y1="134.62" x2="317.5" y2="99.06" width="0.1524" layer="91"/>
<wire x1="317.5" y1="99.06" x2="314.96" y2="99.06" width="0.1524" layer="91"/>
<pinref part="100-16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$258" class="0">
<segment>
<wire x1="314.96" y1="96.52" x2="318.77" y2="96.52" width="0.1524" layer="91"/>
<wire x1="318.77" y1="96.52" x2="318.77" y2="123.19" width="0.1524" layer="91"/>
<wire x1="318.77" y1="123.19" x2="284.48" y2="123.19" width="0.1524" layer="91"/>
<wire x1="284.48" y1="123.19" x2="284.48" y2="127" width="0.1524" layer="91"/>
<pinref part="LCD_MONTH_A" gate="G$1" pin="E"/>
<wire x1="284.48" y1="127" x2="285.75" y2="127" width="0.1524" layer="91"/>
<pinref part="100-17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$259" class="0">
<segment>
<pinref part="LCD_MONTH_A" gate="G$1" pin="D"/>
<wire x1="285.75" y1="129.54" x2="283.21" y2="129.54" width="0.1524" layer="91"/>
<wire x1="283.21" y1="129.54" x2="283.21" y2="121.92" width="0.1524" layer="91"/>
<wire x1="283.21" y1="121.92" x2="320.04" y2="121.92" width="0.1524" layer="91"/>
<wire x1="320.04" y1="121.92" x2="320.04" y2="93.98" width="0.1524" layer="91"/>
<wire x1="320.04" y1="93.98" x2="314.96" y2="93.98" width="0.1524" layer="91"/>
<pinref part="100-18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$260" class="0">
<segment>
<wire x1="314.96" y1="91.44" x2="321.31" y2="91.44" width="0.1524" layer="91"/>
<wire x1="321.31" y1="91.44" x2="321.31" y2="120.65" width="0.1524" layer="91"/>
<wire x1="321.31" y1="120.65" x2="281.94" y2="120.65" width="0.1524" layer="91"/>
<wire x1="281.94" y1="120.65" x2="281.94" y2="132.08" width="0.1524" layer="91"/>
<pinref part="LCD_MONTH_A" gate="G$1" pin="C"/>
<wire x1="281.94" y1="132.08" x2="285.75" y2="132.08" width="0.1524" layer="91"/>
<pinref part="100-19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$261" class="0">
<segment>
<pinref part="LCD_MONTH_A" gate="G$1" pin="B"/>
<wire x1="285.75" y1="134.62" x2="280.67" y2="134.62" width="0.1524" layer="91"/>
<wire x1="280.67" y1="134.62" x2="280.67" y2="119.38" width="0.1524" layer="91"/>
<wire x1="280.67" y1="119.38" x2="322.58" y2="119.38" width="0.1524" layer="91"/>
<wire x1="322.58" y1="119.38" x2="322.58" y2="88.9" width="0.1524" layer="91"/>
<wire x1="322.58" y1="88.9" x2="314.96" y2="88.9" width="0.1524" layer="91"/>
<pinref part="100-20" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$262" class="0">
<segment>
<wire x1="314.96" y1="86.36" x2="323.85" y2="86.36" width="0.1524" layer="91"/>
<wire x1="323.85" y1="86.36" x2="323.85" y2="118.11" width="0.1524" layer="91"/>
<wire x1="323.85" y1="118.11" x2="279.4" y2="118.11" width="0.1524" layer="91"/>
<wire x1="279.4" y1="118.11" x2="279.4" y2="137.16" width="0.1524" layer="91"/>
<pinref part="LCD_MONTH_A" gate="G$1" pin="A"/>
<wire x1="279.4" y1="137.16" x2="285.75" y2="137.16" width="0.1524" layer="91"/>
<pinref part="100-21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$276" class="0">
<segment>
<wire x1="394.97" y1="100.33" x2="396.24" y2="100.33" width="0.1524" layer="91"/>
<wire x1="396.24" y1="100.33" x2="396.24" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_MONTH_B" gate="G$1" pin="F"/>
<wire x1="396.24" y1="135.89" x2="381" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$277" class="0">
<segment>
<pinref part="LCD_MONTH_B" gate="G$1" pin="G"/>
<wire x1="381" y1="133.35" x2="397.51" y2="133.35" width="0.1524" layer="91"/>
<wire x1="397.51" y1="133.35" x2="397.51" y2="97.79" width="0.1524" layer="91"/>
<wire x1="397.51" y1="97.79" x2="394.97" y2="97.79" width="0.1524" layer="91"/>
<pinref part="100-23" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$278" class="0">
<segment>
<wire x1="394.97" y1="95.25" x2="398.78" y2="95.25" width="0.1524" layer="91"/>
<wire x1="398.78" y1="95.25" x2="398.78" y2="121.92" width="0.1524" layer="91"/>
<wire x1="398.78" y1="121.92" x2="364.49" y2="121.92" width="0.1524" layer="91"/>
<wire x1="364.49" y1="121.92" x2="364.49" y2="125.73" width="0.1524" layer="91"/>
<pinref part="LCD_MONTH_B" gate="G$1" pin="E"/>
<wire x1="364.49" y1="125.73" x2="365.76" y2="125.73" width="0.1524" layer="91"/>
<pinref part="100-24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$279" class="0">
<segment>
<pinref part="LCD_MONTH_B" gate="G$1" pin="D"/>
<wire x1="365.76" y1="128.27" x2="363.22" y2="128.27" width="0.1524" layer="91"/>
<wire x1="363.22" y1="128.27" x2="363.22" y2="120.65" width="0.1524" layer="91"/>
<wire x1="363.22" y1="120.65" x2="400.05" y2="120.65" width="0.1524" layer="91"/>
<wire x1="400.05" y1="120.65" x2="400.05" y2="92.71" width="0.1524" layer="91"/>
<wire x1="400.05" y1="92.71" x2="394.97" y2="92.71" width="0.1524" layer="91"/>
<pinref part="100-25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$280" class="0">
<segment>
<wire x1="394.97" y1="90.17" x2="401.32" y2="90.17" width="0.1524" layer="91"/>
<wire x1="401.32" y1="90.17" x2="401.32" y2="119.38" width="0.1524" layer="91"/>
<wire x1="401.32" y1="119.38" x2="361.95" y2="119.38" width="0.1524" layer="91"/>
<wire x1="361.95" y1="119.38" x2="361.95" y2="130.81" width="0.1524" layer="91"/>
<pinref part="LCD_MONTH_B" gate="G$1" pin="C"/>
<wire x1="361.95" y1="130.81" x2="365.76" y2="130.81" width="0.1524" layer="91"/>
<pinref part="100-26" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$281" class="0">
<segment>
<pinref part="LCD_MONTH_B" gate="G$1" pin="B"/>
<wire x1="365.76" y1="133.35" x2="360.68" y2="133.35" width="0.1524" layer="91"/>
<wire x1="360.68" y1="133.35" x2="360.68" y2="118.11" width="0.1524" layer="91"/>
<wire x1="360.68" y1="118.11" x2="402.59" y2="118.11" width="0.1524" layer="91"/>
<wire x1="402.59" y1="118.11" x2="402.59" y2="87.63" width="0.1524" layer="91"/>
<wire x1="402.59" y1="87.63" x2="394.97" y2="87.63" width="0.1524" layer="91"/>
<pinref part="100-27" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$282" class="0">
<segment>
<wire x1="394.97" y1="85.09" x2="403.86" y2="85.09" width="0.1524" layer="91"/>
<wire x1="403.86" y1="85.09" x2="403.86" y2="116.84" width="0.1524" layer="91"/>
<wire x1="403.86" y1="116.84" x2="359.41" y2="116.84" width="0.1524" layer="91"/>
<wire x1="359.41" y1="116.84" x2="359.41" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_MONTH_B" gate="G$1" pin="A"/>
<wire x1="359.41" y1="135.89" x2="365.76" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-28" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$296" class="0">
<segment>
<wire x1="474.98" y1="100.33" x2="476.25" y2="100.33" width="0.1524" layer="91"/>
<wire x1="476.25" y1="100.33" x2="476.25" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_A" gate="G$1" pin="F"/>
<wire x1="476.25" y1="135.89" x2="461.01" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-29" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$297" class="0">
<segment>
<pinref part="LCD_YEAR_A" gate="G$1" pin="G"/>
<wire x1="461.01" y1="133.35" x2="477.52" y2="133.35" width="0.1524" layer="91"/>
<wire x1="477.52" y1="133.35" x2="477.52" y2="97.79" width="0.1524" layer="91"/>
<wire x1="477.52" y1="97.79" x2="474.98" y2="97.79" width="0.1524" layer="91"/>
<pinref part="100-30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$298" class="0">
<segment>
<wire x1="474.98" y1="95.25" x2="478.79" y2="95.25" width="0.1524" layer="91"/>
<wire x1="478.79" y1="95.25" x2="478.79" y2="121.92" width="0.1524" layer="91"/>
<wire x1="478.79" y1="121.92" x2="444.5" y2="121.92" width="0.1524" layer="91"/>
<wire x1="444.5" y1="121.92" x2="444.5" y2="125.73" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_A" gate="G$1" pin="E"/>
<wire x1="444.5" y1="125.73" x2="445.77" y2="125.73" width="0.1524" layer="91"/>
<pinref part="100-31" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$299" class="0">
<segment>
<pinref part="LCD_YEAR_A" gate="G$1" pin="D"/>
<wire x1="445.77" y1="128.27" x2="443.23" y2="128.27" width="0.1524" layer="91"/>
<wire x1="443.23" y1="128.27" x2="443.23" y2="120.65" width="0.1524" layer="91"/>
<wire x1="443.23" y1="120.65" x2="480.06" y2="120.65" width="0.1524" layer="91"/>
<wire x1="480.06" y1="120.65" x2="480.06" y2="92.71" width="0.1524" layer="91"/>
<wire x1="480.06" y1="92.71" x2="474.98" y2="92.71" width="0.1524" layer="91"/>
<pinref part="100-32" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$300" class="0">
<segment>
<wire x1="474.98" y1="90.17" x2="481.33" y2="90.17" width="0.1524" layer="91"/>
<wire x1="481.33" y1="90.17" x2="481.33" y2="119.38" width="0.1524" layer="91"/>
<wire x1="481.33" y1="119.38" x2="441.96" y2="119.38" width="0.1524" layer="91"/>
<wire x1="441.96" y1="119.38" x2="441.96" y2="130.81" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_A" gate="G$1" pin="C"/>
<wire x1="441.96" y1="130.81" x2="445.77" y2="130.81" width="0.1524" layer="91"/>
<pinref part="100-33" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$301" class="0">
<segment>
<pinref part="LCD_YEAR_A" gate="G$1" pin="B"/>
<wire x1="445.77" y1="133.35" x2="440.69" y2="133.35" width="0.1524" layer="91"/>
<wire x1="440.69" y1="133.35" x2="440.69" y2="118.11" width="0.1524" layer="91"/>
<wire x1="440.69" y1="118.11" x2="482.6" y2="118.11" width="0.1524" layer="91"/>
<wire x1="482.6" y1="118.11" x2="482.6" y2="87.63" width="0.1524" layer="91"/>
<wire x1="482.6" y1="87.63" x2="474.98" y2="87.63" width="0.1524" layer="91"/>
<pinref part="100-34" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$302" class="0">
<segment>
<wire x1="474.98" y1="85.09" x2="483.87" y2="85.09" width="0.1524" layer="91"/>
<wire x1="483.87" y1="85.09" x2="483.87" y2="116.84" width="0.1524" layer="91"/>
<wire x1="483.87" y1="116.84" x2="439.42" y2="116.84" width="0.1524" layer="91"/>
<wire x1="439.42" y1="116.84" x2="439.42" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_A" gate="G$1" pin="A"/>
<wire x1="439.42" y1="135.89" x2="445.77" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$316" class="0">
<segment>
<wire x1="552.45" y1="100.33" x2="553.72" y2="100.33" width="0.1524" layer="91"/>
<wire x1="553.72" y1="100.33" x2="553.72" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_B" gate="G$1" pin="F"/>
<wire x1="553.72" y1="135.89" x2="538.48" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-36" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$317" class="0">
<segment>
<pinref part="LCD_YEAR_B" gate="G$1" pin="G"/>
<wire x1="538.48" y1="133.35" x2="554.99" y2="133.35" width="0.1524" layer="91"/>
<wire x1="554.99" y1="133.35" x2="554.99" y2="97.79" width="0.1524" layer="91"/>
<wire x1="554.99" y1="97.79" x2="552.45" y2="97.79" width="0.1524" layer="91"/>
<pinref part="100-37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$318" class="0">
<segment>
<wire x1="552.45" y1="95.25" x2="556.26" y2="95.25" width="0.1524" layer="91"/>
<wire x1="556.26" y1="95.25" x2="556.26" y2="121.92" width="0.1524" layer="91"/>
<wire x1="556.26" y1="121.92" x2="521.97" y2="121.92" width="0.1524" layer="91"/>
<wire x1="521.97" y1="121.92" x2="521.97" y2="125.73" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_B" gate="G$1" pin="E"/>
<wire x1="521.97" y1="125.73" x2="523.24" y2="125.73" width="0.1524" layer="91"/>
<pinref part="100-38" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$319" class="0">
<segment>
<pinref part="LCD_YEAR_B" gate="G$1" pin="D"/>
<wire x1="523.24" y1="128.27" x2="520.7" y2="128.27" width="0.1524" layer="91"/>
<wire x1="520.7" y1="128.27" x2="520.7" y2="120.65" width="0.1524" layer="91"/>
<wire x1="520.7" y1="120.65" x2="557.53" y2="120.65" width="0.1524" layer="91"/>
<wire x1="557.53" y1="120.65" x2="557.53" y2="92.71" width="0.1524" layer="91"/>
<wire x1="557.53" y1="92.71" x2="552.45" y2="92.71" width="0.1524" layer="91"/>
<pinref part="100-39" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$320" class="0">
<segment>
<wire x1="552.45" y1="90.17" x2="558.8" y2="90.17" width="0.1524" layer="91"/>
<wire x1="558.8" y1="90.17" x2="558.8" y2="119.38" width="0.1524" layer="91"/>
<wire x1="558.8" y1="119.38" x2="519.43" y2="119.38" width="0.1524" layer="91"/>
<wire x1="519.43" y1="119.38" x2="519.43" y2="130.81" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_B" gate="G$1" pin="C"/>
<wire x1="519.43" y1="130.81" x2="523.24" y2="130.81" width="0.1524" layer="91"/>
<pinref part="100-40" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$321" class="0">
<segment>
<pinref part="LCD_YEAR_B" gate="G$1" pin="B"/>
<wire x1="523.24" y1="133.35" x2="518.16" y2="133.35" width="0.1524" layer="91"/>
<wire x1="518.16" y1="133.35" x2="518.16" y2="118.11" width="0.1524" layer="91"/>
<wire x1="518.16" y1="118.11" x2="560.07" y2="118.11" width="0.1524" layer="91"/>
<wire x1="560.07" y1="118.11" x2="560.07" y2="87.63" width="0.1524" layer="91"/>
<wire x1="560.07" y1="87.63" x2="552.45" y2="87.63" width="0.1524" layer="91"/>
<pinref part="100-41" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$322" class="0">
<segment>
<wire x1="561.34" y1="85.09" x2="561.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="561.34" y1="116.84" x2="516.89" y2="116.84" width="0.1524" layer="91"/>
<wire x1="516.89" y1="116.84" x2="516.89" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_B" gate="G$1" pin="A"/>
<wire x1="516.89" y1="135.89" x2="523.24" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-77" gate="G$1" pin="2"/>
<wire x1="552.45" y1="85.09" x2="561.34" y2="85.09" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$336" class="0">
<segment>
<wire x1="633.73" y1="100.33" x2="635" y2="100.33" width="0.1524" layer="91"/>
<wire x1="635" y1="100.33" x2="635" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_C" gate="G$1" pin="F"/>
<wire x1="635" y1="135.89" x2="619.76" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-42" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$337" class="0">
<segment>
<pinref part="LCD_YEAR_C" gate="G$1" pin="G"/>
<wire x1="619.76" y1="133.35" x2="636.27" y2="133.35" width="0.1524" layer="91"/>
<wire x1="636.27" y1="133.35" x2="636.27" y2="97.79" width="0.1524" layer="91"/>
<wire x1="636.27" y1="97.79" x2="633.73" y2="97.79" width="0.1524" layer="91"/>
<pinref part="100-43" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$338" class="0">
<segment>
<wire x1="633.73" y1="95.25" x2="637.54" y2="95.25" width="0.1524" layer="91"/>
<wire x1="637.54" y1="95.25" x2="637.54" y2="121.92" width="0.1524" layer="91"/>
<wire x1="637.54" y1="121.92" x2="603.25" y2="121.92" width="0.1524" layer="91"/>
<wire x1="603.25" y1="121.92" x2="603.25" y2="125.73" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_C" gate="G$1" pin="E"/>
<wire x1="603.25" y1="125.73" x2="604.52" y2="125.73" width="0.1524" layer="91"/>
<pinref part="100-44" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$339" class="0">
<segment>
<pinref part="LCD_YEAR_C" gate="G$1" pin="D"/>
<wire x1="604.52" y1="128.27" x2="601.98" y2="128.27" width="0.1524" layer="91"/>
<wire x1="601.98" y1="128.27" x2="601.98" y2="120.65" width="0.1524" layer="91"/>
<wire x1="601.98" y1="120.65" x2="638.81" y2="120.65" width="0.1524" layer="91"/>
<wire x1="638.81" y1="120.65" x2="638.81" y2="92.71" width="0.1524" layer="91"/>
<wire x1="638.81" y1="92.71" x2="633.73" y2="92.71" width="0.1524" layer="91"/>
<pinref part="100-45" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$340" class="0">
<segment>
<wire x1="633.73" y1="90.17" x2="640.08" y2="90.17" width="0.1524" layer="91"/>
<wire x1="640.08" y1="90.17" x2="640.08" y2="119.38" width="0.1524" layer="91"/>
<wire x1="640.08" y1="119.38" x2="600.71" y2="119.38" width="0.1524" layer="91"/>
<wire x1="600.71" y1="119.38" x2="600.71" y2="130.81" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_C" gate="G$1" pin="C"/>
<wire x1="600.71" y1="130.81" x2="604.52" y2="130.81" width="0.1524" layer="91"/>
<pinref part="100-46" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$341" class="0">
<segment>
<pinref part="LCD_YEAR_C" gate="G$1" pin="B"/>
<wire x1="604.52" y1="133.35" x2="599.44" y2="133.35" width="0.1524" layer="91"/>
<wire x1="599.44" y1="133.35" x2="599.44" y2="118.11" width="0.1524" layer="91"/>
<wire x1="599.44" y1="118.11" x2="641.35" y2="118.11" width="0.1524" layer="91"/>
<wire x1="641.35" y1="118.11" x2="641.35" y2="87.63" width="0.1524" layer="91"/>
<wire x1="641.35" y1="87.63" x2="633.73" y2="87.63" width="0.1524" layer="91"/>
<pinref part="100-47" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$342" class="0">
<segment>
<wire x1="633.73" y1="85.09" x2="642.62" y2="85.09" width="0.1524" layer="91"/>
<wire x1="642.62" y1="85.09" x2="642.62" y2="116.84" width="0.1524" layer="91"/>
<wire x1="642.62" y1="116.84" x2="598.17" y2="116.84" width="0.1524" layer="91"/>
<wire x1="598.17" y1="116.84" x2="598.17" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_C" gate="G$1" pin="A"/>
<wire x1="598.17" y1="135.89" x2="604.52" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-48" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$356" class="0">
<segment>
<wire x1="711.2" y1="100.33" x2="712.47" y2="100.33" width="0.1524" layer="91"/>
<wire x1="712.47" y1="100.33" x2="712.47" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_D" gate="G$1" pin="F"/>
<wire x1="712.47" y1="135.89" x2="697.23" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-49" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$357" class="0">
<segment>
<pinref part="LCD_YEAR_D" gate="G$1" pin="G"/>
<wire x1="697.23" y1="133.35" x2="713.74" y2="133.35" width="0.1524" layer="91"/>
<wire x1="713.74" y1="133.35" x2="713.74" y2="97.79" width="0.1524" layer="91"/>
<wire x1="713.74" y1="97.79" x2="711.2" y2="97.79" width="0.1524" layer="91"/>
<pinref part="100-50" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$358" class="0">
<segment>
<wire x1="711.2" y1="95.25" x2="715.01" y2="95.25" width="0.1524" layer="91"/>
<wire x1="715.01" y1="95.25" x2="715.01" y2="121.92" width="0.1524" layer="91"/>
<wire x1="715.01" y1="121.92" x2="680.72" y2="121.92" width="0.1524" layer="91"/>
<wire x1="680.72" y1="121.92" x2="680.72" y2="125.73" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_D" gate="G$1" pin="E"/>
<wire x1="680.72" y1="125.73" x2="681.99" y2="125.73" width="0.1524" layer="91"/>
<pinref part="100-51" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$359" class="0">
<segment>
<pinref part="LCD_YEAR_D" gate="G$1" pin="D"/>
<wire x1="681.99" y1="128.27" x2="679.45" y2="128.27" width="0.1524" layer="91"/>
<wire x1="679.45" y1="128.27" x2="679.45" y2="120.65" width="0.1524" layer="91"/>
<wire x1="679.45" y1="120.65" x2="716.28" y2="120.65" width="0.1524" layer="91"/>
<wire x1="716.28" y1="120.65" x2="716.28" y2="92.71" width="0.1524" layer="91"/>
<wire x1="716.28" y1="92.71" x2="711.2" y2="92.71" width="0.1524" layer="91"/>
<pinref part="100-52" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$360" class="0">
<segment>
<wire x1="711.2" y1="90.17" x2="717.55" y2="90.17" width="0.1524" layer="91"/>
<wire x1="717.55" y1="90.17" x2="717.55" y2="119.38" width="0.1524" layer="91"/>
<wire x1="717.55" y1="119.38" x2="678.18" y2="119.38" width="0.1524" layer="91"/>
<wire x1="678.18" y1="119.38" x2="678.18" y2="130.81" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_D" gate="G$1" pin="C"/>
<wire x1="678.18" y1="130.81" x2="681.99" y2="130.81" width="0.1524" layer="91"/>
<pinref part="100-53" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$361" class="0">
<segment>
<pinref part="LCD_YEAR_D" gate="G$1" pin="B"/>
<wire x1="681.99" y1="133.35" x2="676.91" y2="133.35" width="0.1524" layer="91"/>
<wire x1="676.91" y1="133.35" x2="676.91" y2="118.11" width="0.1524" layer="91"/>
<wire x1="676.91" y1="118.11" x2="718.82" y2="118.11" width="0.1524" layer="91"/>
<wire x1="718.82" y1="118.11" x2="718.82" y2="87.63" width="0.1524" layer="91"/>
<wire x1="718.82" y1="87.63" x2="711.2" y2="87.63" width="0.1524" layer="91"/>
<pinref part="100-54" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$362" class="0">
<segment>
<wire x1="711.2" y1="85.09" x2="720.09" y2="85.09" width="0.1524" layer="91"/>
<wire x1="720.09" y1="85.09" x2="720.09" y2="116.84" width="0.1524" layer="91"/>
<wire x1="720.09" y1="116.84" x2="675.64" y2="116.84" width="0.1524" layer="91"/>
<wire x1="675.64" y1="116.84" x2="675.64" y2="135.89" width="0.1524" layer="91"/>
<pinref part="LCD_YEAR_D" gate="G$1" pin="A"/>
<wire x1="675.64" y1="135.89" x2="681.99" y2="135.89" width="0.1524" layer="91"/>
<pinref part="100-55" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$376" class="0">
<segment>
<wire x1="388.62" y1="0" x2="389.89" y2="0" width="0.1524" layer="91"/>
<wire x1="389.89" y1="0" x2="389.89" y2="35.56" width="0.1524" layer="91"/>
<pinref part="LCD_TEMP_A" gate="G$1" pin="F"/>
<wire x1="389.89" y1="35.56" x2="374.65" y2="35.56" width="0.1524" layer="91"/>
<pinref part="100-56" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$377" class="0">
<segment>
<pinref part="LCD_TEMP_A" gate="G$1" pin="G"/>
<wire x1="374.65" y1="33.02" x2="391.16" y2="33.02" width="0.1524" layer="91"/>
<wire x1="391.16" y1="33.02" x2="391.16" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="391.16" y1="-2.54" x2="388.62" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="100-57" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$378" class="0">
<segment>
<wire x1="388.62" y1="-5.08" x2="392.43" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="392.43" y1="-5.08" x2="392.43" y2="21.59" width="0.1524" layer="91"/>
<wire x1="392.43" y1="21.59" x2="358.14" y2="21.59" width="0.1524" layer="91"/>
<wire x1="358.14" y1="21.59" x2="358.14" y2="25.4" width="0.1524" layer="91"/>
<pinref part="LCD_TEMP_A" gate="G$1" pin="E"/>
<wire x1="358.14" y1="25.4" x2="359.41" y2="25.4" width="0.1524" layer="91"/>
<pinref part="100-58" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$379" class="0">
<segment>
<pinref part="LCD_TEMP_A" gate="G$1" pin="D"/>
<wire x1="359.41" y1="27.94" x2="356.87" y2="27.94" width="0.1524" layer="91"/>
<wire x1="356.87" y1="27.94" x2="356.87" y2="20.32" width="0.1524" layer="91"/>
<wire x1="356.87" y1="20.32" x2="393.7" y2="20.32" width="0.1524" layer="91"/>
<wire x1="393.7" y1="20.32" x2="393.7" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="393.7" y1="-7.62" x2="388.62" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="100-59" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$380" class="0">
<segment>
<wire x1="388.62" y1="-10.16" x2="394.97" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="394.97" y1="-10.16" x2="394.97" y2="19.05" width="0.1524" layer="91"/>
<wire x1="394.97" y1="19.05" x2="355.6" y2="19.05" width="0.1524" layer="91"/>
<wire x1="355.6" y1="19.05" x2="355.6" y2="30.48" width="0.1524" layer="91"/>
<pinref part="LCD_TEMP_A" gate="G$1" pin="C"/>
<wire x1="355.6" y1="30.48" x2="359.41" y2="30.48" width="0.1524" layer="91"/>
<pinref part="100-60" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$381" class="0">
<segment>
<pinref part="LCD_TEMP_A" gate="G$1" pin="B"/>
<wire x1="359.41" y1="33.02" x2="354.33" y2="33.02" width="0.1524" layer="91"/>
<wire x1="354.33" y1="33.02" x2="354.33" y2="17.78" width="0.1524" layer="91"/>
<wire x1="354.33" y1="17.78" x2="396.24" y2="17.78" width="0.1524" layer="91"/>
<wire x1="396.24" y1="17.78" x2="396.24" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="396.24" y1="-12.7" x2="388.62" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="100-61" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$382" class="0">
<segment>
<wire x1="388.62" y1="-15.24" x2="397.51" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="397.51" y1="-15.24" x2="397.51" y2="16.51" width="0.1524" layer="91"/>
<wire x1="397.51" y1="16.51" x2="353.06" y2="16.51" width="0.1524" layer="91"/>
<wire x1="353.06" y1="16.51" x2="353.06" y2="35.56" width="0.1524" layer="91"/>
<pinref part="LCD_TEMP_A" gate="G$1" pin="A"/>
<wire x1="353.06" y1="35.56" x2="359.41" y2="35.56" width="0.1524" layer="91"/>
<pinref part="100-62" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$396" class="0">
<segment>
<wire x1="467.36" y1="0" x2="468.63" y2="0" width="0.1524" layer="91"/>
<wire x1="468.63" y1="0" x2="468.63" y2="35.56" width="0.1524" layer="91"/>
<pinref part="LCD_TEMP_B" gate="G$1" pin="F"/>
<wire x1="468.63" y1="35.56" x2="453.39" y2="35.56" width="0.1524" layer="91"/>
<pinref part="100-63" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$397" class="0">
<segment>
<pinref part="LCD_TEMP_B" gate="G$1" pin="G"/>
<wire x1="453.39" y1="33.02" x2="469.9" y2="33.02" width="0.1524" layer="91"/>
<wire x1="469.9" y1="33.02" x2="469.9" y2="-2.54" width="0.1524" layer="91"/>
<wire x1="469.9" y1="-2.54" x2="467.36" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="100-64" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$398" class="0">
<segment>
<wire x1="467.36" y1="-5.08" x2="471.17" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="471.17" y1="-5.08" x2="471.17" y2="21.59" width="0.1524" layer="91"/>
<wire x1="471.17" y1="21.59" x2="436.88" y2="21.59" width="0.1524" layer="91"/>
<wire x1="436.88" y1="21.59" x2="436.88" y2="25.4" width="0.1524" layer="91"/>
<pinref part="LCD_TEMP_B" gate="G$1" pin="E"/>
<wire x1="436.88" y1="25.4" x2="438.15" y2="25.4" width="0.1524" layer="91"/>
<pinref part="100-65" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$399" class="0">
<segment>
<pinref part="LCD_TEMP_B" gate="G$1" pin="D"/>
<wire x1="438.15" y1="27.94" x2="435.61" y2="27.94" width="0.1524" layer="91"/>
<wire x1="435.61" y1="27.94" x2="435.61" y2="20.32" width="0.1524" layer="91"/>
<wire x1="435.61" y1="20.32" x2="472.44" y2="20.32" width="0.1524" layer="91"/>
<wire x1="472.44" y1="20.32" x2="472.44" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="472.44" y1="-7.62" x2="467.36" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="100-66" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$400" class="0">
<segment>
<wire x1="467.36" y1="-10.16" x2="473.71" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="473.71" y1="-10.16" x2="473.71" y2="19.05" width="0.1524" layer="91"/>
<wire x1="473.71" y1="19.05" x2="434.34" y2="19.05" width="0.1524" layer="91"/>
<wire x1="434.34" y1="19.05" x2="434.34" y2="30.48" width="0.1524" layer="91"/>
<pinref part="LCD_TEMP_B" gate="G$1" pin="C"/>
<wire x1="434.34" y1="30.48" x2="438.15" y2="30.48" width="0.1524" layer="91"/>
<pinref part="100-67" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$401" class="0">
<segment>
<pinref part="LCD_TEMP_B" gate="G$1" pin="B"/>
<wire x1="438.15" y1="33.02" x2="433.07" y2="33.02" width="0.1524" layer="91"/>
<wire x1="433.07" y1="33.02" x2="433.07" y2="17.78" width="0.1524" layer="91"/>
<wire x1="433.07" y1="17.78" x2="474.98" y2="17.78" width="0.1524" layer="91"/>
<wire x1="474.98" y1="17.78" x2="474.98" y2="-12.7" width="0.1524" layer="91"/>
<wire x1="474.98" y1="-12.7" x2="467.36" y2="-12.7" width="0.1524" layer="91"/>
<pinref part="100-68" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$402" class="0">
<segment>
<wire x1="467.36" y1="-15.24" x2="476.25" y2="-15.24" width="0.1524" layer="91"/>
<wire x1="476.25" y1="-15.24" x2="476.25" y2="16.51" width="0.1524" layer="91"/>
<wire x1="476.25" y1="16.51" x2="431.8" y2="16.51" width="0.1524" layer="91"/>
<wire x1="431.8" y1="16.51" x2="431.8" y2="35.56" width="0.1524" layer="91"/>
<pinref part="LCD_TEMP_B" gate="G$1" pin="A"/>
<wire x1="431.8" y1="35.56" x2="438.15" y2="35.56" width="0.1524" layer="91"/>
<pinref part="100-69" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$410" class="0">
<segment>
<pinref part="MON" gate="G$1" pin="A"/>
<wire x1="561.34" y1="22.86" x2="508" y2="22.86" width="0.1524" layer="91"/>
<wire x1="508" y1="22.86" x2="508" y2="7.62" width="0.1524" layer="91"/>
<pinref part="100-76" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$411" class="0">
<segment>
<pinref part="TUE" gate="G$1" pin="A"/>
<wire x1="561.34" y1="25.4" x2="515.62" y2="25.4" width="0.1524" layer="91"/>
<wire x1="515.62" y1="25.4" x2="515.62" y2="7.62" width="0.1524" layer="91"/>
<pinref part="100-75" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$412" class="0">
<segment>
<pinref part="WED" gate="G$1" pin="A"/>
<wire x1="561.34" y1="27.94" x2="523.24" y2="27.94" width="0.1524" layer="91"/>
<wire x1="523.24" y1="27.94" x2="523.24" y2="7.62" width="0.1524" layer="91"/>
<pinref part="100-74" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$413" class="0">
<segment>
<pinref part="THU" gate="G$1" pin="A"/>
<wire x1="561.34" y1="30.48" x2="530.86" y2="30.48" width="0.1524" layer="91"/>
<wire x1="530.86" y1="30.48" x2="530.86" y2="7.62" width="0.1524" layer="91"/>
<pinref part="100-73" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$414" class="0">
<segment>
<pinref part="FRI" gate="G$1" pin="A"/>
<wire x1="561.34" y1="33.02" x2="538.48" y2="33.02" width="0.1524" layer="91"/>
<wire x1="538.48" y1="33.02" x2="538.48" y2="7.62" width="0.1524" layer="91"/>
<pinref part="100-72" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$415" class="0">
<segment>
<pinref part="SAT" gate="G$1" pin="A"/>
<wire x1="561.34" y1="35.56" x2="546.1" y2="35.56" width="0.1524" layer="91"/>
<wire x1="546.1" y1="35.56" x2="546.1" y2="7.62" width="0.1524" layer="91"/>
<pinref part="100-71" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$416" class="0">
<segment>
<pinref part="SUN" gate="G$1" pin="A"/>
<wire x1="561.34" y1="38.1" x2="553.72" y2="38.1" width="0.1524" layer="91"/>
<wire x1="553.72" y1="38.1" x2="553.72" y2="7.62" width="0.1524" layer="91"/>
<pinref part="100-70" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DEMUX_A" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="32"/>
<wire x1="831.85" y1="82.55" x2="842.01" y2="82.55" width="0.1524" layer="91"/>
<label x="842.01" y="82.55" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="A"/>
<wire x1="571.5" y1="17.78" x2="567.69" y2="17.78" width="0.1524" layer="91"/>
<label x="567.69" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DEMUX_B" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="36"/>
<wire x1="831.85" y1="77.47" x2="842.01" y2="77.47" width="0.1524" layer="91"/>
<label x="842.01" y="77.47" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="B"/>
<wire x1="571.5" y1="15.24" x2="567.69" y2="15.24" width="0.1524" layer="91"/>
<label x="567.69" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="DEMUX_C" class="0">
<segment>
<pinref part="RASPBERRYPI" gate="A" pin="38"/>
<wire x1="831.85" y1="74.93" x2="842.01" y2="74.93" width="0.1524" layer="91"/>
<label x="842.01" y="74.93" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="IC1" gate="A" pin="C"/>
<wire x1="596.9" y1="15.24" x2="600.71" y2="15.24" width="0.1524" layer="91"/>
<label x="600.71" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="10K-1" gate="G$1" pin="2"/>
<pinref part="2003_HOUR_A" gate="A" pin="I1"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="F"/>
<pinref part="10K-1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="G"/>
<pinref part="10K-2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="I2"/>
<pinref part="10K-2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="E"/>
<pinref part="10K-3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="I3"/>
<pinref part="10K-3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="D"/>
<pinref part="10K-4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="I4"/>
<pinref part="10K-4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="C"/>
<pinref part="10K-5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="I5"/>
<pinref part="10K-5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="B"/>
<pinref part="10K-6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="I6"/>
<pinref part="10K-6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="4543_HOUR_A" gate="A" pin="A"/>
<pinref part="10K-7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="I7"/>
<pinref part="10K-7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O1"/>
<pinref part="470-1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O2"/>
<pinref part="470-2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O3"/>
<pinref part="470-3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O4"/>
<pinref part="470-4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O5"/>
<pinref part="470-5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O6"/>
<pinref part="470-6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="2003_HOUR_A" gate="A" pin="O7"/>
<pinref part="470-7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="F"/>
<pinref part="10K-8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="I1"/>
<pinref part="10K-8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="G"/>
<pinref part="10K-9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="I2"/>
<pinref part="10K-9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="D"/>
<pinref part="10K-11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="I4"/>
<pinref part="10K-11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="A"/>
<pinref part="10K-12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="I7"/>
<pinref part="10K-12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="B"/>
<pinref part="10K-13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="I6"/>
<pinref part="10K-13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="E"/>
<pinref part="10K-10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="I3"/>
<pinref part="10K-10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="4543_HOUR_B" gate="A" pin="C"/>
<pinref part="10K-14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="I5"/>
<pinref part="10K-14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O1"/>
<pinref part="470-8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O2"/>
<pinref part="470-9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O3"/>
<pinref part="470-10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O4"/>
<pinref part="470-11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O5"/>
<pinref part="470-12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O6"/>
<pinref part="470-13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="2003_HOUR_B" gate="A" pin="O7"/>
<pinref part="470-14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="F"/>
<pinref part="10K-15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="I1"/>
<pinref part="10K-15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="G"/>
<pinref part="10K-16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="I2"/>
<pinref part="10K-16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="E"/>
<pinref part="10K-17" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="I3"/>
<pinref part="10K-17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="D"/>
<pinref part="10K-18" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="I4"/>
<pinref part="10K-18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="C"/>
<pinref part="10K-19" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="I5"/>
<pinref part="10K-19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="B"/>
<pinref part="10K-20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$78" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="I6"/>
<pinref part="10K-20" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<pinref part="4543_MIN_A" gate="A" pin="A"/>
<pinref part="10K-21" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$80" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="I7"/>
<pinref part="10K-21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O1"/>
<pinref part="470-15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O2"/>
<pinref part="470-16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O3"/>
<pinref part="470-17" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O4"/>
<pinref part="470-18" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O5"/>
<pinref part="470-19" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O6"/>
<pinref part="470-20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="2003_MIN_A" gate="A" pin="O7"/>
<pinref part="470-21" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="F"/>
<pinref part="10K-22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="I1"/>
<pinref part="10K-22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="G"/>
<pinref part="10K-23" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="I2"/>
<pinref part="10K-23" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$104" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="E"/>
<pinref part="10K-24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$105" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="I3"/>
<pinref part="10K-24" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$106" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="D"/>
<pinref part="10K-25" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="I4"/>
<pinref part="10K-25" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="C"/>
<pinref part="10K-26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="I5"/>
<pinref part="10K-26" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="B"/>
<pinref part="10K-27" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="I6"/>
<pinref part="10K-27" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="4543_MIN_B" gate="A" pin="A"/>
<pinref part="10K-28" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="I7"/>
<pinref part="10K-28" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O1"/>
<pinref part="470-22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O2"/>
<pinref part="470-23" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O3"/>
<pinref part="470-24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O4"/>
<pinref part="470-25" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$123" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O5"/>
<pinref part="470-26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$124" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O6"/>
<pinref part="470-27" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$125" class="0">
<segment>
<pinref part="2003_MIN_B" gate="A" pin="O7"/>
<pinref part="470-28" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$126" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="F"/>
<pinref part="10K-29" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$127" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="I1"/>
<pinref part="10K-29" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$128" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="G"/>
<pinref part="10K-30" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$129" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="I2"/>
<pinref part="10K-30" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$138" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="I3"/>
<pinref part="10K-31" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="B"/>
<pinref part="10K-34" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$140" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="I6"/>
<pinref part="10K-34" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$141" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="A"/>
<pinref part="10K-35" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$142" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="I7"/>
<pinref part="10K-35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="D"/>
<pinref part="10K-32" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$144" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="I4"/>
<pinref part="10K-32" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$145" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="C"/>
<pinref part="10K-33" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$146" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="I5"/>
<pinref part="10K-33" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$147" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O2"/>
<pinref part="470-30" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$148" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O3"/>
<pinref part="470-31" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$149" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O4"/>
<pinref part="470-32" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$150" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O5"/>
<pinref part="470-33" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$156" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O6"/>
<pinref part="470-34" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$157" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O7"/>
<pinref part="470-35" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$158" class="0">
<segment>
<pinref part="2003_SEC_A" gate="A" pin="O1"/>
<pinref part="470-29" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$159" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="F"/>
<pinref part="10K-36" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$160" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="I1"/>
<pinref part="10K-36" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$161" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="G"/>
<pinref part="10K-37" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$162" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="I2"/>
<pinref part="10K-37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$170" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="E"/>
<pinref part="10K-38" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$171" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="I3"/>
<pinref part="10K-38" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$172" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="D"/>
<pinref part="10K-39" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$173" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="I4"/>
<pinref part="10K-39" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$174" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="C"/>
<pinref part="10K-40" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$175" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="I5"/>
<pinref part="10K-40" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$176" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="B"/>
<pinref part="10K-41" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$177" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="I6"/>
<pinref part="10K-41" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$178" class="0">
<segment>
<pinref part="4543_SEC_B" gate="A" pin="A"/>
<pinref part="10K-42" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$179" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="I7"/>
<pinref part="10K-42" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$180" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O1"/>
<pinref part="470-36" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$181" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O2"/>
<pinref part="470-37" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$182" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O3"/>
<pinref part="470-38" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$183" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O4"/>
<pinref part="470-39" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$189" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O5"/>
<pinref part="470-40" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$190" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O6"/>
<pinref part="470-41" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$191" class="0">
<segment>
<pinref part="2003_SEC_B" gate="A" pin="O7"/>
<pinref part="470-42" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$192" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="LD"/>
<pinref part="1K-91" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$193" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="F"/>
<pinref part="100-1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$194" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="IB"/>
<pinref part="1K-31" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$195" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="IC"/>
<pinref part="1K-32" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$203" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="ID"/>
<pinref part="1K-33" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$204" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="IA"/>
<pinref part="1K-34" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$205" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="PH"/>
<pinref part="1K-35" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$206" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="G"/>
<pinref part="100-2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$207" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="E"/>
<pinref part="100-3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$208" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="D"/>
<pinref part="100-4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$209" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="C"/>
<pinref part="100-5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$210" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="B"/>
<pinref part="100-6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$211" class="0">
<segment>
<pinref part="4543_DAY_A" gate="A" pin="A"/>
<pinref part="100-7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$212" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="F"/>
<pinref part="100-8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$213" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="LD"/>
<pinref part="1K-36" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$214" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="IB"/>
<pinref part="1K-37" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$215" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="IC"/>
<pinref part="1K-38" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$223" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="ID"/>
<pinref part="1K-39" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$224" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="IA"/>
<pinref part="1K-40" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$225" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="PH"/>
<pinref part="1K-41" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$226" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="G"/>
<pinref part="100-9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$227" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="D"/>
<pinref part="100-11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$228" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="C"/>
<pinref part="100-12" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$229" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="B"/>
<pinref part="100-13" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$230" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="A"/>
<pinref part="100-14" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$231" class="0">
<segment>
<pinref part="4543_DAY_B" gate="A" pin="E"/>
<pinref part="100-10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$232" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="LD"/>
<pinref part="1K-42" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$233" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="IC"/>
<pinref part="1K-44" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$234" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="ID"/>
<pinref part="1K-45" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$235" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="IA"/>
<pinref part="1K-46" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$243" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="PH"/>
<pinref part="1K-47" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$244" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="F"/>
<pinref part="100-15" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$245" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="G"/>
<pinref part="100-16" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$246" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="E"/>
<pinref part="100-17" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$247" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="D"/>
<pinref part="100-18" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$248" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="C"/>
<pinref part="100-19" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$249" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="B"/>
<pinref part="100-20" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$250" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="A"/>
<pinref part="100-21" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$251" class="0">
<segment>
<pinref part="4543_MONTH_A" gate="A" pin="IB"/>
<pinref part="1K-43" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$252" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="LD"/>
<pinref part="1K-48" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$253" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="F"/>
<pinref part="100-22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$254" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="IB"/>
<pinref part="1K-49" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$255" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="IC"/>
<pinref part="1K-50" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$263" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="ID"/>
<pinref part="1K-51" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$264" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="IA"/>
<pinref part="1K-52" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$265" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="PH"/>
<pinref part="1K-53" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$266" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="LD"/>
<pinref part="1K-54" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$267" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="G"/>
<pinref part="100-23" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$268" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="E"/>
<pinref part="100-24" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$269" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="D"/>
<pinref part="100-25" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$270" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="C"/>
<pinref part="100-26" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$271" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="B"/>
<pinref part="100-27" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$272" class="0">
<segment>
<pinref part="4543_MONTH_B" gate="A" pin="A"/>
<pinref part="100-28" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$273" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="F"/>
<pinref part="100-29" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$274" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="IB"/>
<pinref part="1K-55" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$275" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="IC"/>
<pinref part="1K-56" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$283" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="ID"/>
<pinref part="1K-57" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$284" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="IA"/>
<pinref part="1K-58" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$285" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="PH"/>
<pinref part="1K-59" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$286" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="G"/>
<pinref part="100-30" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$287" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="E"/>
<pinref part="100-31" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$288" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="D"/>
<pinref part="100-32" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$289" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="C"/>
<pinref part="100-33" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$290" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="B"/>
<pinref part="100-34" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$291" class="0">
<segment>
<pinref part="4543_YEAR_A" gate="A" pin="A"/>
<pinref part="100-35" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$292" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="LD"/>
<pinref part="1K-60" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$293" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="F"/>
<pinref part="100-36" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$294" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="IB"/>
<pinref part="1K-61" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$295" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="IC"/>
<pinref part="1K-62" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$303" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="ID"/>
<pinref part="1K-63" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$304" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="IA"/>
<pinref part="1K-64" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$305" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="PH"/>
<pinref part="1K-65" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$306" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="LD"/>
<pinref part="1K-66" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$307" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="G"/>
<pinref part="100-37" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$308" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="E"/>
<pinref part="100-38" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$309" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="D"/>
<pinref part="100-39" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$310" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="C"/>
<pinref part="100-40" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$311" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="B"/>
<pinref part="100-41" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$312" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="F"/>
<pinref part="100-42" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$313" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="IB"/>
<pinref part="1K-67" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$314" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="IC"/>
<pinref part="1K-68" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$315" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="ID"/>
<pinref part="1K-69" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$323" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="IA"/>
<pinref part="1K-70" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$324" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="PH"/>
<pinref part="1K-71" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$325" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="LD"/>
<pinref part="1K-72" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$326" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="G"/>
<pinref part="100-43" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$327" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="E"/>
<pinref part="100-44" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$328" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="D"/>
<pinref part="100-45" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$329" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="C"/>
<pinref part="100-46" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$330" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="B"/>
<pinref part="100-47" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$331" class="0">
<segment>
<pinref part="4543_YEAR_C" gate="A" pin="A"/>
<pinref part="100-48" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$332" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="IB"/>
<pinref part="1K-73" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$333" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="IC"/>
<pinref part="1K-74" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$334" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="ID"/>
<pinref part="1K-75" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$335" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="IA"/>
<pinref part="1K-76" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$343" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="PH"/>
<pinref part="1K-77" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$344" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="F"/>
<pinref part="100-49" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$345" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="E"/>
<pinref part="100-51" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$346" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="D"/>
<pinref part="100-52" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$347" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="C"/>
<pinref part="100-53" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$348" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="B"/>
<pinref part="100-54" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$349" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="A"/>
<pinref part="100-55" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$350" class="0">
<segment>
<pinref part="4543_YEAR_D" gate="A" pin="G"/>
<pinref part="100-50" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$351" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="LD"/>
<pinref part="1K-78" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$352" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="F"/>
<pinref part="100-56" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$353" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="IB"/>
<pinref part="1K-79" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$354" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="IC"/>
<pinref part="1K-80" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$355" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="ID"/>
<pinref part="1K-81" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$363" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="IA"/>
<pinref part="1K-82" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$364" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="PH"/>
<pinref part="1K-83" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$365" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="G"/>
<pinref part="100-57" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$366" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="E"/>
<pinref part="100-58" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$367" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="D"/>
<pinref part="100-59" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$368" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="C"/>
<pinref part="100-60" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$369" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="B"/>
<pinref part="100-61" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$370" class="0">
<segment>
<pinref part="4543_TEMP_A" gate="A" pin="A"/>
<pinref part="100-62" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$371" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="LD"/>
<pinref part="1K-84" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$372" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="F"/>
<pinref part="100-63" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$373" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="G"/>
<pinref part="100-64" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$374" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="E"/>
<pinref part="100-65" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$375" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="D"/>
<pinref part="100-66" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$383" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="C"/>
<pinref part="100-67" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$384" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="B"/>
<pinref part="100-68" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$385" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="A"/>
<pinref part="100-69" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$386" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="IB"/>
<pinref part="1K-85" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$387" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="IC"/>
<pinref part="1K-86" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$388" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="ID"/>
<pinref part="1K-87" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$389" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="IA"/>
<pinref part="1K-88" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$390" class="0">
<segment>
<pinref part="4543_TEMP_B" gate="A" pin="PH"/>
<pinref part="1K-89" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$391" class="0">
<segment>
<pinref part="IC1" gate="A" pin="X7"/>
<pinref part="100-70" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$392" class="0">
<segment>
<pinref part="IC1" gate="A" pin="X6"/>
<pinref part="100-71" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$393" class="0">
<segment>
<pinref part="IC1" gate="A" pin="X5"/>
<pinref part="100-72" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$394" class="0">
<segment>
<pinref part="IC1" gate="A" pin="X4"/>
<pinref part="100-73" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$395" class="0">
<segment>
<pinref part="IC1" gate="A" pin="X3"/>
<pinref part="100-74" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$403" class="0">
<segment>
<pinref part="IC1" gate="A" pin="X2"/>
<pinref part="100-75" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$404" class="0">
<segment>
<pinref part="IC1" gate="A" pin="X1"/>
<pinref part="100-76" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$137" class="0">
<segment>
<pinref part="4543_SEC_A" gate="A" pin="E"/>
<pinref part="10K-31" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$408" class="0">
<segment>
<pinref part="4543_YEAR_B" gate="A" pin="A"/>
<pinref part="100-77" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$429" class="0">
<segment>
<pinref part="470-43" gate="G$1" pin="1"/>
<pinref part="COLON_A" gate="G$1" pin="A"/>
<wire x1="368.3" y1="300.99" x2="368.3" y2="294.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$430" class="0">
<segment>
<pinref part="COLON_A" gate="G$1" pin="C"/>
<wire x1="368.3" y1="287.02" x2="368.3" y2="280.67" width="0.1524" layer="91"/>
<pinref part="COLON_B" gate="G$1" pin="A"/>
<wire x1="368.3" y1="280.67" x2="363.22" y2="280.67" width="0.1524" layer="91"/>
<wire x1="363.22" y1="280.67" x2="363.22" y2="271.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$431" class="0">
<segment>
<pinref part="470-44" gate="G$1" pin="1"/>
<pinref part="COLON_C" gate="G$1" pin="A"/>
<wire x1="441.96" y1="300.99" x2="441.96" y2="294.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$432" class="0">
<segment>
<pinref part="COLON_C" gate="G$1" pin="C"/>
<wire x1="441.96" y1="287.02" x2="441.96" y2="279.4" width="0.1524" layer="91"/>
<pinref part="COLON_D" gate="G$1" pin="A"/>
<wire x1="441.96" y1="279.4" x2="434.34" y2="279.4" width="0.1524" layer="91"/>
<wire x1="434.34" y1="279.4" x2="434.34" y2="269.24" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
