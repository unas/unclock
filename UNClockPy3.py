#!/usr/bin/python3

import time
from datetime import datetime
import _strptime
import RPi.GPIO as GPIO
import sys
import logging
import socket
import json
import threading

class FuncThread(threading.Thread):
	def __init__(self, target, *args):
		self._target = target
		self._args = args
		threading.Thread.__init__(self)
 
	def run(self):
		self._target(*self._args)

logging.basicConfig(filename='UNClock.log', level=logging.INFO,format='%(asctime)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')

class NightTime:
	minTime = ["6:00", "6:00", "6:00", "6:00", "6:00", "6:00", "6:00"]
	maxTime = ["21:00", "21:00", "21:00", "21:00", "22:30", "22:30", "21:00"]

class LD:
	HourALD = 18
	HourBLD = 23
	MinALD = 24
	MinBLD = 25
	SecALD = 8
	SecBLD = 7
	DayALD = 10
	DayBLD = 9
	MonthALD = 11
	MonthBLD = 5
	YearALD = 6
	YearBLD = 13
	YearCLD = 19
	YearDLD = 26
	TemperatureALD = 14
	TemperatureBLD = 15

class Bit:
	A = 4
	B = 17
	C = 27
	D = 22

class weekday:
	A = 12
	B = 16
	C = 20
	IO = 21

class Area:
	second = [LD.SecALD, LD.SecBLD]
	minute = [LD.MinALD, LD.MinBLD]
	hour = [LD.HourALD, LD.HourBLD]
	day = [LD.DayALD, LD.DayBLD]
	month = [LD.MonthALD, LD.MonthBLD]
	year = [LD.YearALD, LD.YearBLD, LD.YearCLD, LD.YearDLD]
	temperature = [LD.TemperatureALD, LD.TemperatureBLD]
	weekday = [weekday.C, weekday.B, weekday.A]

def Init():
	logging.info("Initializing")
	
	GPIO.setmode(GPIO.BCM)
	allBinaryPins = [Bit.A, Bit.B, Bit.C, Bit.D]
	allDigitLDs = [LD.HourALD, LD.HourBLD, LD.MinALD, LD.MinBLD, LD.SecALD, LD.SecBLD, LD.DayALD, LD.DayBLD, LD.MonthALD, LD.MonthBLD, LD.YearALD, LD.YearBLD, LD.YearCLD, LD.YearDLD, LD.TemperatureALD, LD.TemperatureBLD]
	
	# Setting all LDs to True, so that we can put all digits to zero
	for i in allDigitLDs:
		GPIO.setup(i, GPIO.OUT)
		GPIO.output(i, True)
	
	# All bits to zero
	for i in allBinaryPins:
		GPIO.setup(i, GPIO.OUT)
		GPIO.output(i, False)
	
	# Turning all LDs off
	for i in allDigitLDs:
		GPIO.output(i, False)
	
	# Setting weekday IO on and turning off all leds
	GPIO.setup(weekday.IO, GPIO.OUT)
	GPIO.setup(weekday.A, GPIO.OUT)
	GPIO.setup(weekday.B, GPIO.OUT)
	GPIO.setup(weekday.C, GPIO.OUT)
	GPIO.output(weekday.IO, True)
	GPIO.output(weekday.A, False)
	GPIO.output(weekday.B, False)
	GPIO.output(weekday.C, False)

# Converts decimal number to binary number
def IntToBinArray(value):
	returnValue = [0, 0, 0, 0]
	if (value > 9):
		return returnValue
	
	binaryStr = str(bin(value)[2:])
	# If the binaryStr is shorter than 3 characters, then adding zeros to the beginning 111 => 0111
	if (len(binaryStr) < len(returnValue)):
		zeroAmount = len(returnValue) - len(binaryStr)
		for i in range(zeroAmount):
			binaryStr = '0' + binaryStr
	
	for i in range(len(binaryStr)):
		returnValue[i] = int(binaryStr[i])
	
	return returnValue

# Set's a specific LED's number
def setLEDValue(LD, number):
	# Converting number to binary array
	binArray = IntToBinArray(number)
	if (len(binArray) != 4): # Has to be 4-bit value
		logging.error("Error, number is not 4-bit")
		return 0
	
	GPIO.output(Bit.D, binArray[0]) # D
	GPIO.output(Bit.C, binArray[1]) # C
	GPIO.output(Bit.B, binArray[2]) # B
	GPIO.output(Bit.A, binArray[3]) # A
	
	# Turning LD on
	GPIO.output(LD, True)
	# Turning LD back off
	GPIO.output(LD, False)

def turnLEDOff(AreaId):
	# Setting bin 1111, which will turn ledscreen off
	GPIO.output(Bit.D, 1) # D
	GPIO.output(Bit.C, 1) # C
	GPIO.output(Bit.B, 1) # B
	GPIO.output(Bit.A, 1) # A
	for i in range(len(AreaId)):	
		# Turning LD on
		GPIO.output(AreaId[i], True)
		# Turning LD back off
		GPIO.output(AreaId[i], False)

# Sets a value (string) to a number area
def setAreaValue(AreaID, value):
	valueArray = [0, 0, 0, 0]
	# Converts value to int and put every digit to array
	valueLength = len(str(value))
	if (valueLength > 4):
		valueLength = 4
	for i in range(valueLength):
		index = (i + 1) * -1
		valueArray[index] = int((str(value))[index])
	
	# Setting every value separately to bits, turning their LD on for a moment to let the value be shown
	for i in range(len(AreaID)):
		index = (i + 1) * -1
		setLEDValue(AreaID[index], valueArray[index])

def setWeekday(override = None):
	
	GPIO.output(weekday.IO, True)
	# On the board, first day is sunday, but python returns monday as zero and sunday as 6
	weekdayNumber = datetime.now().weekday()
	
	# If an override value is given, then setting that day value
	if (override != None):
		# Checking that the value is an integer
		try:
			weekdayNumber = int(override)
		except ValueError:
			return
	
	# Converting mo-su (0-6) to demux value 1-7(su-sa)
	weekdayNumber += 2
	if (weekdayNumber > 7):
		weekdayNumber = 1
	
	binArray = IntToBinArray(weekdayNumber)
	# Got a 4-bit array, but we will only use the 3 last bits
	for i in range(3, 0, -1):
		GPIO.output(Area.weekday[i - 1], binArray[i])

def turnWeekdayOff():
	GPIO.output(weekday.IO, False)

def setTemperature():
	TCP_IP = '10.0.2.15'
	TCP_PORT = 5015
	BUFFER_SIZE = 10240
	MESSAGE = "getTemperature"
	
	while True:
		currentTemp = 0
		try:
			s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			s.connect((TCP_IP, TCP_PORT))
			s.send(MESSAGE.encode('utf-8'))
			data = s.recv(BUFFER_SIZE)
			
			if (data != "Unknown request!"):
				jsonData = json.loads(data)
				if jsonData['success']:
					#print('Temperature: ' + str(round(jsonData['temperatureC'], 2)) + "C")
					currentTemp = int(round(jsonData['temperatureC'], 0))
				else:
					logging.info("Could not get temperature")
				
				if (isNightTime()):
					turnLEDOff(Area.temperature)
				else:
					setAreaValue(Area.temperature, currentTemp)
			
			s.close()
		except:
			logging.info("Could not get temperature")
		finally:
			if (isNightTime()):
				turnLEDOff(Area.temperature)
			else:
				setAreaValue(Area.temperature, currentTemp)
		
		time.sleep(60.0)

def isNightTime():
	weekday = datetime.now().weekday()
	now = datetime.strptime(str(datetime.now().hour) + ":" + str(datetime.now().minute), "%H:%M")
	minTime = datetime.strptime(NightTime.minTime[weekday], "%H:%M")
	maxTime = datetime.strptime(NightTime.maxTime[weekday], "%H:%M")
	
	return now < minTime or now >= maxTime

def firstMinuteAfterNightTime():
	weekday = datetime.now().weekday()
	now = datetime.strptime(str(datetime.now().hour) + ":" + str(datetime.now().minute), "%H:%M")
	minTime = datetime.strptime(NightTime.minTime[weekday], "%H:%M")
	
	return now == minTime;

def main(argv):
	success = True
	try:
		logging.info("Starting UNClock")
		# Checking if debug is on
		debug = False
		for i in range(0, len(argv)):
			if (argv[i] == '-d' or argv[i] == '--debug'):
				debug = True
		
		# Initializing pins
		Init()
		
		logging.info("Starting main loop")
		
		# Setting date and time
		hour = datetime.now().hour
		minute = datetime.now().minute
		second = datetime.now().second
		day = datetime.now().day
		month = datetime.now().month
		year = datetime.now().year
		weekday = datetime.now().weekday()
		
		effectSleep = 1.0
		time.sleep(effectSleep)
		setAreaValue(Area.day, day)
		time.sleep(effectSleep)
		setAreaValue(Area.month, month)
		time.sleep(effectSleep)
		setAreaValue(Area.year, year)
		time.sleep(effectSleep)
		setWeekday()
		time.sleep(effectSleep)
		setAreaValue(Area.hour, hour)
		time.sleep(effectSleep)
		setAreaValue(Area.minute, minute)
		time.sleep(effectSleep)
		setAreaValue(Area.second, second)
		
		# Temperature updater
		#temperatureThread = FuncThread(setTemperature)
		temperatureThread = threading.Thread(target = setTemperature)
		# When threads are daemons, they will be killed with ctrl-c, not just the main program
		temperatureThread.daemon = True
		temperatureThread.start()
		
		# Starting main loop
		while True:
			hour = datetime.now().hour
			minute = datetime.now().minute
			second = datetime.now().second
			day = datetime.now().day
			month = datetime.now().month
			year = datetime.now().year
			weekday = datetime.now().weekday()
			
			# If it's nighttime, then turning all lights off
			if (isNightTime()):
				turnLEDOff(Area.hour)
				turnLEDOff(Area.minute)
				turnLEDOff(Area.second)
				turnLEDOff(Area.day)
				turnLEDOff(Area.month)
				turnLEDOff(Area.year)
				turnWeekdayOff()
			elif (firstMinuteAfterNightTime() or hour == 23 and minute == 59 and second > 55 or hour == 0 and minute == 0 and second < 5):
				# Updating date and time
				setAreaValue(Area.hour, hour)
				setAreaValue(Area.minute, minute)
				setAreaValue(Area.second, second)
				setAreaValue(Area.day, day)
				setAreaValue(Area.month, month)
				setAreaValue(Area.year, year)
				setWeekday()
			else:
				# Updating time
				setAreaValue(Area.hour, hour)
				setAreaValue(Area.minute, minute)
				setAreaValue(Area.second, second)
			
			# Sleeping until next second
			sleepTime = 1 - float(datetime.now().microsecond) / 1000000
			if (debug):
				logging.info("Sleeping: " + str(sleepTime))
			time.sleep(sleepTime)
			if (debug):
				logging.info("Waking up from sleep: " + str(datetime.now()))
			
			# Checking that the second actually changed
			newSecond = datetime.now().second
			while (newSecond == second):
				# The second hasn't changed, we problably woke up too soon, sleeping some more
				logging.info("Woke up too soon, second hadn't changed")
				extraSleepTime = 1 - float(datetime.now().microsecond) / 1000000
				if (extraSleepTime == 0):
					exptraSleepTime = 0.1
				time.sleep(extraSleepTime)
				newSecond = datetime.now().second
			
		
	except Exception as e:
		logging.error("Error in main loop: " + str(e))
		success = False
	finally:
		logging.info("UNClock shutting down")
		logging.info("Cleaning GPIO")
		GPIO.cleanup()
		
		# Returning value. If exiting with an error, then the main program will be restarted
		return success

if __name__ == "__main__": # Checking if this is a the main program, not an imported module
	while True:
		returnValue = main(sys.argv)
		if (returnValue == True): # Exiting only if main didn't stop due to an error
			logging.info("Clean exit, shutting program down...")
			break
		else: # Main crashed, waiting 5 second and restarting
			logging.error("Error, restarting in 5 seconds")
			time.sleep(5)
